<?php

namespace Nitra\IntegraBundle\Lib\JobPreview;

use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters;

/**
 * Превью прайса сервис
 */
class JobPreview implements JobPreviewInterface
{
    /**
     * @var IntegraParameters $integraParameters
     */
    protected $integraParameters;

    /**
     * @var array массвив буквенных столюцов в файле excell
     */
    protected $excellColumnLetters = array(
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    );

    /**
     * Конструктор
     *
     * @param IntegraParameters $integraParameters
     */
    public function __construct(IntegraParameters $integraParameters)
    {
        // установить зависимости
        $this->integraParameters = $integraParameters;
    }

    /**
     * {@inheritdoc}
     */
    public function getIntegraParameters()
    {
        return $this->integraParameters;
    }

    /**
     * {@inheritdoc}
     */
    public function getExcellColumnLetters()
    {
        return $this->excellColumnLetters;
    }

    /**
     * {@inheritdoc}
     */
    public function preview(\Nitra\IntegraBundle\Entity\Model\JobInterface $job, $filePath = false, $maxRows = 10)
    {
        // если используется не стандартный загрузчик
        if (!$job->getIsStandart()) {
            // превью только для стандартных прайсов
            return null;
        }

        // путь к файлу прайса не указан
        if (!$filePath) {
            // путь к диреториии с прайсами
            $excelDir = $this->getIntegraParameters()->get('jobs[excel_dir]', null, true);
            // получить путь к файлу прайса
            $filePath = $excelDir . $job->getNameFilePrice();
        }

        // проверить файл прайса
        if (!is_file($filePath) || !file_exists($filePath)) {
            // нет файла прайса
            return null;
        }

        // получить массив буквенных столюцов в файле excell
        $excellColumnLetters = $this->getExcellColumnLetters();

        // массив столбцов из которых формруется
        // название продукта
        $columnProductName = preg_split('/[\s,]/', $job->getColumnName(), null, PREG_SPLIT_NO_EMPTY);

        // получить Reader
        $objReader   = \PHPExcel_IOFactory::createReaderForFile($filePath);
        $objPHPExcel = $objReader->load($filePath);

        // получить закладку файла прайса
        $worksheet  = $objPHPExcel->getSheet();

        $rowCounter = 0;        // счетчик добавленных строк
        $priceData  = array();  // результирующий массив парсинга файла прайса
        try {
            // преобразовать в массив
            $priceArray = $worksheet->toArray(null, true, null, true);
        } catch (\PHPExcel_Calculation_Exception $e) {
            $matches = array();
            preg_match('/!([A-Z]+\d+) ->/', $e->getMessage(), $matches);
            $coordinate = $matches[1];
            $this->getIntegraParameters()
                ->get('container')
                ->get('session')
                ->getFlashBag()
                ->add('error', 'Не возможно обработать прайс, в формуле "' . $coordinate . '" ошибки');
            return null;
        }

        foreach ($priceArray as $rowNum => $rowData) {
            // отступ кол-во строк сверху
            if ($rowNum < $job->getStartRow()) {
                // перейти к след строке
                continue;
            }

            // получить из файла прайса значение ячейки "название"
            $columnName = '';
            foreach ($columnProductName as $columnNum) {
                if (isset($excellColumnLetters[($columnNum-1)]) &&
                    isset($rowData[$excellColumnLetters[($columnNum-1)]])
                ) {
                    // запомнить столбец результируюшего массива из строки прайса
                    $columnName.= $rowData[$excellColumnLetters[($columnNum-1)]] .' ';
                }
            }

            // обрезать пробелы по краям
            $columnName = trim($columnName);

            // получить из файла прайса значение ячейки "наличие"
            $columnAvailiable = '';
            if ($job->getColumnAvailiable() &&
                isset($excellColumnLetters[($job->getColumnAvailiable()-1)]) &&
                isset($rowData[$excellColumnLetters[($job->getColumnAvailiable()-1)]])
            ) {
                // запомнить столбец результируюшего массива из строки прайса
                $columnAvailiable = $rowData[$excellColumnLetters[($job->getColumnAvailiable()-1)]];
            }

            // получить из файла прайса значение ячейки "цена1"
            $columnPriceIn1 = '';
            if ($job->getColumnPriceIn1() &&
                isset($excellColumnLetters[($job->getColumnPriceIn1()-1)]) &&
                isset($rowData[$excellColumnLetters[($job->getColumnPriceIn1()-1)]])
            ) {
                // запомнить столбец результируюшего массива из строки прайса
                $columnPriceIn1 = $rowData[$excellColumnLetters[($job->getColumnPriceIn1()-1)]];
            }

            // получить из файла прайса значение ячейки "цена2"
            $columnPriceIn2 = '';
            if ($job->getColumnPriceIn2() &&
                isset($excellColumnLetters[($job->getColumnPriceIn2()-1)]) &&
                isset($rowData[$excellColumnLetters[($job->getColumnPriceIn2()-1)]])
            ) {
                // запомнить столбец результируюшего массива из строки прайса
                $columnPriceIn2 = $rowData[$excellColumnLetters[($job->getColumnPriceIn2()-1)]];
            }

            // получить из файла прайса значение ячейки "цена3"
            $columnPriceIn3 = '';
            if ($job->getColumnPriceIn3() &&
                isset($excellColumnLetters[($job->getColumnPriceIn3()-1)]) &&
                isset($rowData[$excellColumnLetters[($job->getColumnPriceIn3()-1)]])
            ) {
                // запомнить столбец результируюшего массива из строки прайса
                $columnPriceIn3 = $rowData[$excellColumnLetters[($job->getColumnPriceIn3()-1)]];
            }

            // получить из файла прайса значение ячейки "цена4"
            $columnPriceIn4 = '';
            if ($job->getColumnPriceIn4() &&
                isset($excellColumnLetters[($job->getColumnPriceIn4()-1)]) &&
                isset($rowData[$excellColumnLetters[($job->getColumnPriceIn4()-1)]])
            ) {
                // запомнить столбец результируюшего массива из строки прайса
                $columnPriceIn4 = $rowData[$excellColumnLetters[($job->getColumnPriceIn4()-1)]];
            }

            // проверить правильность строки прайса
            // должно удовлетворять минимуму условий
            // если название продукта пустое или не уазана цена
            if (!$columnName || !is_numeric($columnPriceIn1)) {
                // перейти к другой строке
                continue;
            }

            // увеличить счетчик строк
            ++ $rowCounter;

            // строка результируюшего массива
            $rowResult = array(
                'columnName'       => $columnName,
                'columnAvailiable' => $columnAvailiable,
                'columnPriceIn1'   => $columnPriceIn1,
                'columnPriceIn2'   => $columnPriceIn2,
                'columnPriceIn3'   => $columnPriceIn3,
                'columnPriceIn4'   => $columnPriceIn4,
            );

            // наполнить результирующий массив
            $priceData[$rowNum] = $rowResult;

            // проверить получение максимального кол-ва строк
            if ($maxRows && $rowCounter >= $maxRows) {
                // прервать выполнение
                break;
            }
        }

        // массив данных предосмотра файла прайса
        $jobPreview = array(
            'isError'                => false,
            'columnPriceIn2_isError' => false,
            'columnPriceIn3_isError' => false,
            'columnPriceIn4_isError' => false,
            // массив сообщений ошибок
            'errors'                 => array(),
            // массив новеров строк с ошибками
            'errorsLines'            => array(),
            'priceData'              => $priceData,
        );

        // проверить наличие строк прайса
        if (!$priceData) {
            $jobPreview['errors'][] = array(
                'type'    => 'error',
                'message' => 'В файле прайса не найдено ни одного продукта.',
            );
        }

        // проверить каждую строку прайса
        foreach ($priceData as $rowNum => $price) {
            // если указана цена 2
            // в файле прайса
            if ($price['columnPriceIn2']) {
                // если столбец цена не число
                // или меньше нуля (отрицательная цена)
                if (!is_numeric($price['columnPriceIn2']) || $price['columnPriceIn2'] < 0) {
                    // запомнить номер строки с ошибкой
                    $jobPreview['errorsLines'][] = $rowNum;

                    // добавить сообщение в ощий массив ошибок
                    // столбец цена нет ошибки не в одной из предыдущих строк продуктов
                    if ($jobPreview['columnPriceIn2_isError'] === false) {
                        // обновить флаг ошибки цены столбца
                        $jobPreview['columnPriceIn2_isError'] = true;
                        // запомнить сообщение об ошибке
                        $jobPreview['errors'][] = array(
                            'type' => 'error',
                            'message' => 'Строка прайса '.$rowNum.' - цена 2 указана не верно.',
                        );
                    }
                }
            }

            // если указана цена 3
            // в файле прайса
            if ($price['columnPriceIn3']) {
                // если столбец цена не число
                // или меньше нуля (отрицательная цена)
                if (!is_numeric($price['columnPriceIn3']) || $price['columnPriceIn3'] < 0) {
                    // запомнить номер строки с ошибкой
                    $jobPreview['errorsLines'][] = $rowNum;

                    // добавить сообщение в ощий массив ошибок
                    // столбец цена нет ошибки не в одной из предыдущих строк продуктов
                    if ($jobPreview['columnPriceIn3_isError'] === false) {
                        // обновить флаг ошибки цены столбца
                        $jobPreview['columnPriceIn3_isError'] = true;
                        // запомнить сообщение об ошибке
                        $jobPreview['errors'][] = array(
                            'type' => 'error',
                            'message' => 'Строка прайса '.$rowNum.' - цена 3 указана не верно.',
                        );
                    }
                }
            }

            // если указана цена 4
            // в файле прайса
            if ($price['columnPriceIn4']) {
                // если столбец цена не число
                // или меньше нуля (отрицательная цена)
                if (!is_numeric($price['columnPriceIn4']) || $price['columnPriceIn4'] < 0) {
                    // запомнить номер строки с ошибкой
                    $jobPreview['errorsLines'][] = $rowNum;

                    // добавить сообщение в ощий массив ошибок
                    // столбец цена нет ошибки не в одной из предыдущих строк продуктов
                    if ($jobPreview['columnPriceIn4_isError'] === false) {
                        // обновить флаг ошибки цены столбца
                        $jobPreview['columnPriceIn4_isError'] = true;
                        // запомнить сообщение об ошибке
                        $jobPreview['errors'][] = array(
                            'type' => 'error',
                            'message' => 'Строка прайса '.$rowNum.' - цена 4 указана не верно.',
                        );
                    }
                }
            }
        }

        // переключить общий флаг ошибок
        if ($jobPreview['errors'] || // есть сообщения об ошибке
            $jobPreview['columnPriceIn2_isError'] || // если есть ошибка в цене 2
            $jobPreview['columnPriceIn3_isError'] || // если есть ошибка в цене 3
            $jobPreview['columnPriceIn4_isError']    // если есть ошибка в цене 4
        ) {
            // есть ошибка
            $jobPreview['isError'] = true;
            $jobPreview['errorsLines'] = array_unique($jobPreview['errorsLines']);
        }

        // вернуть результируюший массив парсинга фалйа прайса
        return $jobPreview;
    }
}