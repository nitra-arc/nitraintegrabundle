<?php

namespace Nitra\IntegraBundle\Lib\JobPreview;

/**
 * Превью прайса интерфейс
 */
interface JobPreviewInterface
{
    /**
     * Получить параметры интегры
     *
     * @return \Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters
     */
    public function getIntegraParameters();

    /**
     * Получить массив буквенных столюцов в файле excell
     *
     * @return array
     */
    public function getExcellColumnLetters();

    /**
     * Получить данные превью файла прайса
     *
     * @param \Nitra\IntegraBundle\Entity\Model\JobInterface    $job        прайс шаблон настроек обработчика файла прайса
     * @param boolean|string                                    $filePath   путь к прайс файлу
     * @param integer                                           $maxRows    количество проверяемых строк в файле прайса
     * если $maxRows = 0 то в файле будут проверяться все строки
     *
     * @return array|null  - <b>array</b> данные результата парсинга файла $filePath
     * <b>null</b> нет данных для отображения файла прайса
     */
    public function preview(\Nitra\IntegraBundle\Entity\Model\JobInterface $job, $filePath = false, $maxRows=10);
}