<?php

namespace Nitra\IntegraBundle\Lib\IntegraParameters;

interface IntegraParametersInterface
{
    /**
     * получить Document используемый интегрой
     * @param  string $name - ключь-название документа
     * @return string Document
     */
    public function getDocument($name);

    /**
     * получить Entity используемый интегрой
     * @param  string $name - ключь-название Entity
     * @return string Entity
     */
    public function getEntity($name);
}