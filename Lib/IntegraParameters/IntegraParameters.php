<?php

namespace Nitra\IntegraBundle\Lib\IntegraParameters;

/**
 * Параметры интегры
 */
class IntegraParameters extends IntegraParametersAware
{
    /**
     * {@inheritdoc}
     */
    public function get($path, $default = null, $deep = false)
    {
        // получить EntityManager
        if ($path == 'em') {
            return $this->container->get('doctrine.orm.entity_manager');
        }

        // получить DocumentManager
        if ($path == 'dm') {
            return $this->container->get('doctrine_mongodb.odm.document_manager');
        }

        // получить Container
        if ($path == 'container') {
            return $this->container;
        }

        // получить родительский параметр
        return parent::get($path, $default, $deep);
    }

    /**
     * Получить Document используемый интегрой
     *
     * @param  string $name Ключь-название документа
     *
     * @return string Document
     *
     * @throws \Exception
     */
    public function getDocument($name)
    {
        // получить значение параметра
        $value = parent::get('document[' . $name . ']', null, true);

        // проверить значение параметра
        // если запрашиваемый параметр не найден
        if (!$value) {
            throw new \Exception('Document интегры: "' . $name . '" не найден.');
        }

        // вернуть Document
        return $value;
    }

    /**
     * Получить Entity используемый интегрой
     *
     * @param  string $name Ключь-название Entity
     *
     * @return string Entity
     *
     * @throws \Exception
     */
    public function getEntity($name)
    {
        // получить значение параметра
        $value = parent::get('entity[' . $name . ']', null, true);

        // проверить значение параметра
        // если запрашиваемый параметр не найден
        if (!$value) {
            throw new \Exception('Entity интегры: "' . $name . '" не найден.');
        }

        // вернуть Entity
        return $value;
    }
}