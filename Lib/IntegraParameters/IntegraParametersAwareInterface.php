<?php

namespace Nitra\IntegraBundle\Lib\IntegraParameters;

/**
 * Установить параметры интегры
 */
interface IntegraParametersAwareInterface
{
    /**
     * Установить параметры интегры
     * @param IntegraParameters|null $integraParameters
     */
    public function setIntegraParameters(IntegraParameters $integraParameters);
}