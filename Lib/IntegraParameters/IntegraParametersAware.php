<?php

namespace Nitra\IntegraBundle\Lib\IntegraParameters;

use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

abstract class IntegraParametersAware extends ParameterBag implements ContainerAwareInterface, IntegraParametersInterface
{
    /**
     * @var ContainerInterface
     * @api
     */
    protected $container;

    /**
     * получить Document используемый интегрой
     * @param  string $name - ключь-название документа
     * @return string Document
     */
    abstract public function getDocument($name);

    /**
     * получить Entity используемый интегрой
     * @param  string $name - ключь-название Entity
     * @return string Entity
     */
    abstract public function getEntity($name);

    /**
     * Sets the Container associated with this Controller.
     * @param ContainerInterface $container A ContainerInterface instance
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}