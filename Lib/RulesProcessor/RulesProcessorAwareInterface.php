<?php

namespace Nitra\IntegraBundle\Lib\RulesProcessor;

interface RulesProcessorAwareInterface
{
    /**
     * Метод выполнения JS функции в монго
     * @param string $js
     * @return array
     */
    public function executeJs($js);

    /**
     * Получение функции для формирования цен входа
     * @param \Nitra\IntegraBundle\Document\Model\InRulesInterface $InRule
     * @return string JS формула для выполнения в Mongo
     */
    public function getInRuleJS(\Nitra\IntegraBundle\Document\Model\InRulesInterface $InRule);

    /**
     * Получение функции для формирования цен выхода
     * @param \Nitra\IntegraBundle\Document\Model\OutRulesInterface $OutRule
     * @param bool $acceptAll
     * @param bool $isLast
     * @return string JS формула для выполнения в Mongo
     */
    public function getOutRuleJS(\Nitra\IntegraBundle\Document\Model\OutRulesInterface $OutRule, $acceptAll = false, $isLast = false);
}