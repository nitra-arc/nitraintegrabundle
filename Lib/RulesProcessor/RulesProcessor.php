<?php

namespace Nitra\IntegraBundle\Lib\RulesProcessor;

/**
 * Обработчик правил прайсов
 */
class RulesProcessor extends RulesProcessorAware
{
    /**
     * Метод выполнения JS функции в монго
     *
     * @param string $js
     *
     * @return array
     */
    public function executeJs($js)
    {
        // get default database name
        $dbName = $this->getDocumentManager()
            ->getConfiguration()
            ->getDefaultDB();

        // get mongodb instance
        $mongodb = $this->getDocumentManager()
            ->getConnection()
            ->selectDatabase($dbName);

        \MongoCursor::$timeout = -1;

        return $mongodb->command(array(
            'eval'      => $js,
            'nolock'    => true,
        ));
    }

    /**
     * Получение функции для формирования цен входа
     *
     * @param \Nitra\IntegraBundle\Document\Model\InRulesInterface $InRule
     *
     * @return string JS формула для выполнения в Mongo
     */
    public function getInRuleJS(\Nitra\IntegraBundle\Document\Model\InRulesInterface $InRule)
    {
        $brandId     = $InRule->getBrand() ? $InRule->getBrand()->getId() : null;
        // для категории правила получить все подкатегории
        $categoryIds = $this->getCategoriesIds($InRule->getCategory());
        $warehouse   = $this->getEntityManager()->find($this->getEntity('warehouse'), $InRule->getWarehouseId());
        $supplier    = $warehouse ? $warehouse->getSupplier() : null;
        $formula     = $this->formulaConditionReplacer($InRule->getFormula());
        $condition   = $this->formulaConditionReplacer($InRule->getCondition());
        if ($condition == "") {
            $condition = "true";
        }

        // ввернуть шаблон
        return $this->getTwig()->render($this->getInRuleTemplate(), array_merge($this->getSupplierCurrency($supplier), array(
            'brandId'           => $brandId,
            'parametersValues'  => $this->formatParameterValuesFilter($InRule->getParameterValues()),
            'categoryIds'       => $categoryIds,
            'warehouse'         => $InRule->getWarehouseId(),
            'formula'           => $formula,
            'condition'         => $condition,
            'currencies'        => $this->getCurrencies(),
        )));
    }

    /**
     * Получение функции для формирования цен выхода
     *
     * @param \Nitra\IntegraBundle\Document\Model\OutRulesInterface $OutRule
     * @param bool                                                  $acceptAll
     * @param bool                                                  $isLast
     *
     * @return string JS формула для выполнения в Mongo
     */
    public function getOutRuleJS(\Nitra\IntegraBundle\Document\Model\OutRulesInterface $OutRule, $acceptAll = false, $isLast = false)
    {
        $brandId     = $OutRule->getBrand() ? $OutRule->getBrand()->getId() : null;
        // для категории правила получить все подкатегории
        $categoryIds = $this->getCategoriesIds($OutRule->getCategory());
        $formula     = $this->formulaConditionReplacer($OutRule->getFormula());
        $condition   = $this->formulaConditionReplacer($OutRule->getCondition()) ? : 'true';
        $supplier    = $this->getEntityManager()->find($this->getEntity('supplier'), $OutRule->getSupplierId());

        // ввернуть шаблон
        return $this->getTwig()->render($this->getOutRuleTemplate(), $this->getSupplierCurrency($supplier) + array(
            'brandId'            => $brandId,
            'categoryIds'        => $categoryIds,
            'warehousesId'       => $this->getOutRuleWarehousesIds($OutRule),
            'OutRule'            => $OutRule,
            'formula'            => $formula,
            'condition'          => $condition,
            'parameters'         => $this->getConfiguration(),
            'warehousesPriority' => $this->getWarehousesPriority(),
            'ownWarehouses'      => $this->getOwnWarehouses($acceptAll, $isLast),
            'isLast'             => $isLast,
            'currencies'         => $this->getCurrencies(),
            'parametersValues'   => $this->formatParameterValuesFilter($OutRule->getParameterValues()),
        ));
    }

    /**
     * @param \Nitra\IntegraBundle\Document\Model\CategoryInterface $category
     *
     * @return null|\MongoId[]
     */
    protected function getCategoryTreeIds($category)
    {
        $categoryIds = null;
        if ($category) {
            $regex = '/(' . preg_quote($category->getPath()) . ')/';
            $categoryIds = $this->getDocumentManager()
                ->createQueryBuilder($this->getDocument('category'))
                ->field('path')->equals(new \MongoRegex($regex))
                ->distinct('_id')
                ->getQuery()
                ->execute()->toArray();
        }

        return $categoryIds;
    }

    /**
     * @param \Nitra\IntegraBundle\Document\Model\ParameterAndValueInterface[] $parameterValues
     *
     * @return array
     */
    protected function formatParameterValuesFilter($parameterValues)
    {
        $result = array();
        foreach ($parameterValues as $parameterValue) {
            $result[$parameterValue->getParameter()->getId()] = $parameterValue->getParameterValue()->getId();
        }

        return $result;
    }

    /**
     * Метод замены плейсхолдеров формы на выражения
     *
     * @param string $fc
     *
     * @return string
     */
    protected function formulaConditionReplacer($fc)
    {
        $replacer = array(
            '{PI}'  => 's.priceIn',
            '{P1}'  => '(s.price1 * exchange)',
            '{P2}'  => '(s.price2 * exchange)',
            '{P3}'  => '(s.price3 * exchange)',
            '{P4}'  => '(s.price4 * exchange)',
        );

        return str_replace(array_keys($replacer), array_values($replacer), $fc);
    }

    /**
     * Получение кода и курса валюты поставщика
     *
     * @param \Nitra\MainBundle\Entity\Supplier|null $supplier
     *
     * @return array
     */
    protected function getSupplierCurrency($supplier)
    {
        $supplierCurrExch = $supplier
            ? ($supplier->getExchange()
                ? $supplier->getExchange()
                : ($supplier->getCurrency()
                    ? $supplier->getCurrency()->getExchange()
                    : 1.0))
            : 1.0;
        $supplierCurrCode = ($supplier && $supplier->getCurrency())
            ? $supplier->getCurrency()->getCode()
            : $this->getCurrencyCode();

        return array(
            'supplierCurrCode'  => $supplierCurrCode,
            'supplierCurrExch'  => $supplierCurrExch,
        );
    }

    /**
     * Получение массива валют (ключ - код валюты; значение - курс)
     *
     * @return array
     */
    protected function getCurrencies()
    {
        $currencies = $this->getEntityManager()->createQueryBuilder()
            ->select('c')
            ->from($this->getEntity('currency'), 'c')
            ->getQuery()->getArrayResult();

        // массив курсов
        $currencyExchange = array();
        foreach ($currencies as $currency) {
            $currencyExchange[$currency['code']] = $currency['exchange'];
        }

        // вернуть массив курсов валют
        return $currencyExchange;
    }

    /**
     * Получить массив с id складов поставщика
     *
     * @param \Nitra\IntegraBundle\Document\Model\OutRulesInterface $OutRule
     *
     * @return array
     */
    protected function getOutRuleWarehousesIds(\Nitra\IntegraBundle\Document\Model\OutRulesInterface $OutRule)
    {
        $warehousesIds  = array();
        if ($OutRule->getSupplierId()) {
            $query = $this->getEntityManager()->createQueryBuilder()
                ->from($this->getEntity('warehouse'), 'w')
                ->select("w.id")
                ->leftJoin($this->getEntity('supplier'), "s", "WITH", "s.id=w.supplier")
                ->where('s.id = ?1')
                ->setParameter(1, $OutRule->getSupplierId())
                ->getQuery();
            $results = $query->getArrayResult();

            foreach ($results as $warehouse) {
                $warehousesIds[] = $warehouse['id'];
            }
        }

        // вернуть результирующий массив
        return $warehousesIds;
    }

    /**
     * Получение id своих складов
     *
     * @param bool $acceptAll
     * @param bool $isLast
     *
     * @return array
     */
    protected function getOwnWarehouses($acceptAll, $isLast)
    {
        $ownWarehouses      = array();
        if ($acceptAll && $isLast) {
            $ids = $this->getEntityManager()->createQueryBuilder()
                ->from($this->getEntity('warehouse'), 'w')
                ->select("w.id")
                ->where("w.delivery IS NULL")
                ->andWhere("w.supplier IS NULL")
                ->getQuery()->getArrayResult();
            $ownWarehouses = array_map(function ($id) {
                return $id['id'];
            }, $ids);
        }

        // вернуть результирующий массив
        return $ownWarehouses;
    }

    /**
     * Получение массива своих складов и складов поставщиков
     * ключ - id склада; значение - приоритет
     *
     * @return array
     */
    protected function getWarehousesPriority()
    {
        $WarehousesPriority = $this->getEntityManager()->createQueryBuilder()
            ->from($this->getEntity('warehouse'), 'w')
            ->select("w.id, w.priority")
            ->where("w.priority IS NOT NULL")
            ->andWhere("w.delivery IS NULL")
            ->getQuery()->getArrayResult();
        $warehousesPriority = array();
        foreach ($WarehousesPriority as $warehouse) {
            $warehousesPriority[$warehouse['id']] = $warehouse['priority'];
        }

        // вернуть результирующий массив
        return $warehousesPriority;
    }

    /**
     * Get tree categories ids
     *
     * @param \Nitra\MainBundle\Document\Category $category
     *
     * @return null|array
     */
    protected function getCategoriesIds($category)
    {
        $categoryIds = null;
        if ($category) {
            $path = $category->getPath();
            $categoryIds = $this->getDocumentManager()
                ->createQueryBuilder($this->getDocument('category'))
                ->field('path')->equals(new \MongoRegex('/(' . preg_quote($path) . ')/'))
                ->distinct('_id')
                ->getQuery()
                ->execute()->toArray();
        }

        return $categoryIds;
    }
}