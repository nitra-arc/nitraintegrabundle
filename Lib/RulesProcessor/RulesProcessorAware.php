<?php

namespace Nitra\IntegraBundle\Lib\RulesProcessor;

use Doctrine\ORM\EntityManager;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\DependencyInjection\Container;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParametersInterface;

abstract class RulesProcessorAware implements RulesProcessorInterface, RulesProcessorAwareInterface, IntegraParametersInterface
{
    /**
     * @var \Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters
     */
    protected $integraParameters;

    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    protected $container;

    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var \Doctrine\ODM\MongoDB\DocumentManager
     */
    protected $dm;

    /**
     * @var string javascript шаблон входящих правил
     */
    protected $inRuleTemplate;

    /**
     * @var string javascript шаблон исходящих правил
     */
    protected $outRuleTemplate;

    /**
     * Метод выполнения JS функции в монго
     * @param string $js
     * @return array
     */
    abstract public function executeJs($js);

    /**
     * Получение функции для формирования цен входа
     * @param \Nitra\IntegraBundle\Document\Model\InRulesInterface $InRule
     * @return string JS формула для выполнения в Mongo
     */
    abstract public function getInRuleJS(\Nitra\IntegraBundle\Document\Model\InRulesInterface $InRule);

    /**
     * Получение функции для формирования цен выхода
     * @param \Nitra\IntegraBundle\Document\Model\OutRulesInterface $OutRule
     * @param bool $acceptAll
     * @param bool $isLast
     * @return string JS формула для выполнения в Mongo
     */
    abstract public function getOutRuleJS(\Nitra\IntegraBundle\Document\Model\OutRulesInterface $OutRule, $acceptAll = false, $isLast = false);

    /**
     * Метод замены плейсхолдеров формы на выражения
     * @param string $fc
     * @return string
     */
    abstract protected function formulaConditionReplacer($fc);

    /**
     * Получение кода и курса валюты поставщика
     * @param \Nitra\MainBundle\Entity\Supplier|null $supplier
     * @return array
     */
    abstract protected function getSupplierCurrency($supplier);

    /**
     * Получение массива валют (ключ - код валюты; значение - курс)
     * @return array
     */
    abstract protected function getCurrencies();

    /**
     * Получить массив с id складов поставщика
     * @param \Nitra\IntegraBundle\Document\OutRules $OutRule
     * @return array
     */
    abstract protected function getOutRuleWarehousesIds(\Nitra\IntegraBundle\Document\Model\OutRulesInterface $OutRule);

    /**
     * Получение id своих складов
     * @param bool $acceptAll
     * @param bool $isLast
     * @return array
     */
    abstract protected function getOwnWarehouses($acceptAll, $isLast);

    /**
     * Получение массива своих складов и складов поставщиков
     * ключ - id склада; значение - приоритет
     * @return array
     */
    abstract protected function getWarehousesPriority();

    /**
     * Конструктор
     * @param IntegraParameters $integraParameters  - параметры интугры
     * @param Container         $container          - контейнер
     * @param \Twig_Environment $twig               - шаблонизатор
     * @param EntityManager     $em                 - EntityManager
     * @param string            $inRuleTemplate     - шаблон обработки входящих цен
     * @param string            $outRuleTemplate    - шаблон обработки исходящих цен
     */
    public function __construct(
        IntegraParameters $integraParameters,
        Container         $container,
        \Twig_Environment $twig,
        EntityManager     $em,
        DocumentManager   $dm,
        $inRuleTemplate,
        $outRuleTemplate
    )
    {
        // установить зависимости
        $this->integraParameters = $integraParameters;
        $this->container         = $container;
        $this->twig              = $twig;
        $this->em                = $em;
        $this->dm                = $dm;
        $this->inRuleTemplate    = $inRuleTemplate;
        $this->outRuleTemplate   = $outRuleTemplate;
    }

    /**
     * {@inheritdoc}
     */
    public function getIntegraParameters()
    {
        return $this->integraParameters;
    }

    /**
     * {@inheritdoc}
     */
    public function getTwig()
    {
        return $this->twig;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityManager()
    {
        return $this->em;
    }

    /**
     * {@inheritdoc}
     */
    public function getDocumentManager()
    {
        return $this->dm;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfiguration()
    {
        return $this->getIntegraParameters()->get('rules');
    }

    /**
     * {@inheritdoc}
     */
    public function getInRuleTemplate()
    {
        return $this->inRuleTemplate;
    }

    /**
     * {@inheritdoc}
     */
    public function getOutRuleTemplate()
    {
        return $this->outRuleTemplate;
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrencyCode()
    {
        return $this->getIntegraParameters()->get('currencyCode');
    }

    /**
     * {@inheritdoc}
     */
    public function getDocument($name)
    {
        return $this->getIntegraParameters()->getDocument($name);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity($name)
    {
        return $this->getIntegraParameters()->getEntity($name);
    }

    /**
     * Перевод затраченого времени в читаебельный вид
     * @param double $sec
     * @return string
     */
    public function normalizeTime($sec)
    {
        $timesValues = array('сек.', 'мин.', 'час.', 'д.', 'лет',);

        $ret = '';

        $times     = array();
        $countZero = false;
        $periods   = array(60, 3600, 86400, 31536000);

        for ($i = 3; $i >= 0; $i --) {
            $period = floor($sec/$periods[$i]);
            if (($period > 0) || ($period == 0 && $countZero)) {
                $times[$i+1] = $period;
                $sec -= $period * $periods[$i];

                $countZero = true;
            }
        }
        $times[0] = $sec;

        for ($i = count($times) - 1; $i >= 0; $i --) {
            $ret .= (int) $times[$i] . ' ' . $timesValues[$i] . ' ';
        }

        // вернуть результирующую строку
        return $ret;
    }
}