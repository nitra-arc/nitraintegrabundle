<?php

namespace Nitra\IntegraBundle\Lib\RulesProcessor;

/**
 * прайсообработчик
 */
interface RulesProcessorInterface
{
    /**
     * получить параметры интегры
     * @return \Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters
     */
    public function getIntegraParameters();

    /**
     * получить twig
     * @return \Twig_Environment
     */
    public function getTwig();

    /**
     * Получить EntityManager
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager();

    /**
     * Получить DocumentManager
     * @return \Doctrine\ODM\MongoDB\DocumentManager
     */
    public function getDocumentManager();

    /**
     * Получить массив правил
     * @return array
     */
    public function getConfiguration();

    /**
     * Get template for input pricing function
     * @return string $template
     */
    public function getInRuleTemplate();

    /**
     * Get template for output pricing function
     * @return string $template
     */
    public function getOutRuleTemplate();

    /**
     * получить код валюты
     * @return string - код валюты
     */
    public function getCurrencyCode();
}