<?php

namespace Nitra\IntegraBundle\Controller\Job;

use Admingenerated\NitraIntegraBundle\BaseJobController\ListController as BaseListController;
use Nitra\IntegraBundle\Form\Type\Job\FiltersType;

class ListController extends BaseListController
{
    /**
     * Получить параметры интегры
     * @return \Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters
     */
    protected function getIntegraParameters()
    {
        return $this->get('nitra_integra.parameters');
    }

    /**
     * Получить форму фильтра
     *
     * @return FiltersType
     */
    protected function getFiltersType()
    {
        $type = new FiltersType();
        $type->setIntegraParameters($this->getIntegraParameters());

        return $type;
    }
}