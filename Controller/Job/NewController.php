<?php

namespace Nitra\IntegraBundle\Controller\Job;

use Admingenerated\NitraIntegraBundle\BaseJobController\NewController as BaseNewController;

class NewController extends BaseNewController
{
    /**
     * Получить параметры интегры
     * @return \Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters
     */
    protected function getIntegraParameters()
    {
        return $this->get('nitra_integra.parameters');
    }

    /**
     * Получить тип формы прайса
     * @return \Nitra\IntegraBundle\Form\Type\Job\EditType
     */
    protected function getNewType()
    {
        // получить форму родителем
        $type = parent::getNewType();
        // добавить параметры интегры
        $type->setIntegraParameters($this->getIntegraParameters());
        // вернуть форму
        return $type;
    }
}