<?php

namespace Nitra\IntegraBundle\Controller\Job;

use Admingenerated\NitraIntegraBundle\BaseJobController\ActionsController as BaseActionsController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Nitra\IntegraBundle\Form\Type\Job\JobLoadType;

class ActionsController extends BaseActionsController
{
    /**
     * @var \Nitra\IntegraBundle\Entity\Job парйс для которрого выполняется расчет
     */
    protected $job;

    /**
     * Получить параметры интегры
     *
     * @return \Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters
     */
    protected function getIntegraParameters()
    {
        return $this->get('nitra_integra.parameters');
    }

    /**
     * Получить превью прайса
     *
     * @return \Nitra\IntegraBundle\Lib\JobPreview\JobPreview
     */
    protected function getJobPreview()
    {
        return $this->get('nitra_integra.job.preview');
    }

    /** 
     * Получить обработчик парйсов
     *
     * @return \Nitra\IntegraBundle\Lib\RulesProcessor\RulesProcessor
     */
    protected function getRulesProcessor()
    {
        return $this->get('nitra_integra.rules.processor');
    }

    /**
     * Скачать последний загруженный файл прайса
     *
     * @param integer $pk идентификатор Job прайса
     *
     * @return BinaryFileResponse|RedirectResponse
     */
    public function attemptObjectDownload($pk)
    {
        // получить прайс
        $job = $this->getObject($pk);

        // получить путь к файлу прайса
        $pathFilePrice = $excelDir = $this->getIntegraParameters()->get('jobs[excel_dir]', null, true).$job->getNameFilePrice();

        // проверить файл прайса
        if (!$pathFilePrice || !is_file($pathFilePrice) || !file_exists($pathFilePrice)) {
            $this->get('session')->getFlashBag()->add('error', 'Извините, файл не найден, возможно он еще не был загружен'); 
            return new RedirectResponse($this->generateUrl("Nitra_IntegraBundle_Job_list"));
        }

        // стандартный загрузчик 
        if ($job->getIsStandart()) {
            // стандартный загрузчик 
            // имя файла для скачивания для 
            $downloadFileName = (string)$job->getWarehouse()->getSupplier()
                .' - '. (string)$job->getWarehouse()
                .' - '. $job->getDescription() 
                . '.' . $job->getFileExtention();
        } else {
            // индивидуальный загрузчик 
            // имя файла для скачивания 
            $downloadFileName = (string)$job->getWarehouse()->getSupplier() 
                .' - '. $job->getDescription()
                . '.' . $job->getFileExtention();
        }

        // создать ответ для скачивания файла
        $fileResponse = new BinaryFileResponse($pathFilePrice);
        $fileResponse->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $downloadFileName,
            iconv('UTF-8', 'ASCII//TRANSLIT', $downloadFileName)
        );

        // вернуть Response для скачивания файла
        return $fileResponse;
    }

    /**
     * Загрузить файл прайса
     *
     * @param integer $pk идентификатор Job прайса
     *
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function attemptObjectLoad($pk)
    {
        // Symfony\Component\HttpFoundation\Request
        $request = $this->get('request');

        // получить прайс
        $Job = $this->getObject($pk);

        // получить форму загрузки прайса
        $form = $this->createForm(new JobLoadType());

        // обработка формы
        if ($request->getMethod() == 'POST') {

            // заполнить форму 
            $form->submit($request);

            // валидация формы
            if ($form->isValid()) {

                // получить загруженные файлы
                $files = $request->files->get('job_load');

                // проверить если файл не был загружен
                if (empty($files['price'])) {
                    // добавить сообщение об ошибке, перенаправить пользователя на загрузку прайса
                    $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Файл не был загружен на сервер.')); 
                    return new RedirectResponse($this->generateUrl('Nitra_IntegraBundle_Job_object', array('pk' => $pk, 'action' => 'load')));
                }

                // проверить файл прайса
                if (!is_file($files['price']->getPathname()) || !file_exists($files['price']->getPathname())) {
                    // добавить сообщение об ошибке, перенаправить пользователя на загрузку прайса
                    $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Файл не был загружен на сервер.')); 
                    return new RedirectResponse($this->generateUrl('Nitra_IntegraBundle_Job_object', array('pk' => $pk, 'action' => 'load')));
                }

                // если используется стандартный загрузчик
                if ($Job->getIsStandart()) {
                    // получить превью файла прайса
                    $jobPreview = $this->getJobPreview()->preview($Job, $files['price']->getPathname());

                    // отображение ошибок
                    if ($jobPreview && isset($jobPreview['errors']) && $jobPreview['errors']) {
                        // добавить сообщение об ошибке
                        $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Загруженный прайс не соответствует шаблону.')); 
                        foreach($jobPreview['errors'] as $error) {
                            $this->get('session')->getFlashBag()->add($error['type'],  $error['message']);
                        }

                        // отобразить форму загрузки прайса
                        return $this->render('NitraIntegraBundle:JobActions:load.html.twig',
                            $this->getAdditionalRenderParameters($Job, 'load') + array(
                                'jobPreview' => $jobPreview,
                                'job' => $Job,
                                'form' => $form->createView(),
                                'title' => $this->get('translator')->trans(
                                    'Загрузить прайс "%jobDescription%" для постащика "%supplierName%".', 
                                    array('%jobDescription%' => $Job->getDescription(), '%supplierName%' => (string)$Job->getWarehouse()->getSupplier()), 
                                    'Admingenerator'),
                            ));
                    }
                }

                // директория загружаемых прайсов
                $excelDir = $this->getIntegraParameters()->get('jobs[excel_dir]', null, true);

                // расширение файла прайса
                $fileExt = trim(strrchr($files['price']->getClientOriginalName(), '.'), '.');

                // имя файла прайса
                $fileName = ($Job->getIsStandart())
                    // стандартный загрузчик
                    ? $Job->getName() .'_id'. $Job->getId() . '.' . $fileExt
                    // индивидуальный загрузчик
                    : $Job->getName() . '.' . $fileExt;

                // полный путь к файлу прайса
                $fullFileName = $excelDir . $fileName;

                // Создание папки Excel, если таковой нет
                if (!is_dir($excelDir)) {
                    mkdir($excelDir, 0755, true);
                }
                if (file_exists($fullFileName)){
                    //Сохраняем предыдущий файл прайса в историю
                    $date = date('Y_m_d');
                    if (!is_dir($excelDir . 'history')) {
                        mkdir($excelDir . 'history', 0755, true);
                    }
                    $historyDir = $excelDir . 'history/' . $date;
                    if (!is_dir($historyDir)) {
                        mkdir($historyDir, 0755, true);
                    }

                    // копия файла
                    $fullFileCopy = ($Job->getIsStandart())
                        ? $historyDir . '/' . $fileName . '_' . date('H_i') . '.' . $fileExt
                        : $historyDir . '/' . $Job->getName() . '_' . date('H_i') . '.' . $fileExt;

                    // создать копию прайса
                    copy($fullFileName, $fullFileCopy);
                    // удалить исходник прайса
                    unlink($fullFileName);
                }

                // Копирование нового файла прайса
                copy($files['price']->getPathname(), $fullFileName);

                // обновить парйс в БД
                $Job->setFileExtention($fileExt);
                $Job->setDownloadedAt(new \DateTime());
                $this->getIntegraParameters()->get('em')->flush();

                // запустить обработку прайса
                $this->jobExec($Job);

                // перенаправить пользователя на список прайсов
                return new RedirectResponse($this->generateUrl("Nitra_IntegraBundle_Job_list"));

            } else {
                // отображение ошибки валидации формы
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator')); 
            }

        }

        // отобразить форму загрузки прайса
        return $this->render('NitraIntegraBundle:JobActions:load.html.twig',
            $this->getAdditionalRenderParameters($Job, 'load') + array(
                'job' => $Job,
                'form' => $form->createView(),
                'title' => $this->get('translator')->trans(
                    'Загрузить прайс "%jobDescription%" для постащика "%supplierName%".', 
                    array('%jobDescription%' => $Job->getDescription(), '%supplierName%' => (string)$Job->getWarehouse()->getSupplier()), 
                    'Admingenerator'),
            ));
    }

    /**
     * Calculate rules by job
     *
     * @param integer $pk Job id
     *
     * @return RedirectResponse
     */
    protected function attemptObjectCalculate($pk)
    {
        // получить прайс
        $Job = $this->getObject($pk);
        // применение входящих и выходящих правил
        $this->acceptRules($Job);
        // перенаправить пользователя на список
        return new RedirectResponse($this->generateUrl("Nitra_IntegraBundle_Job_list"));
    }

    /**
     * Применить все правила
     *
     * @Route("/accept-all", name="Nitra_IntegraBundle_Job_calculate")
     *
     * @return RedirectResponse
     *
     * @throws \Exception
     */
    public function attemptCalculate()
    {
        set_time_limit(600);

        // входящие правила
        $InRules = $this->getIntegraParameters()->get('dm')
            ->getRepository($this->getIntegraParameters()->getDocument('inRules'))
            ->findBy(array(), array('sortOrder' => -1));
        $this->acceptInRules($InRules);

        // исходящие правила
        $OutRules = $this->getIntegraParameters()->get('dm')
            ->getRepository($this->getIntegraParameters()->getDocument('outRules'))
            ->findBy(array(), array('sortOrder' => -1));
        $this->acceptOutRules($OutRules);

        // перенаправить пользователя на список
        return new RedirectResponse($this->generateUrl("Nitra_IntegraBundle_Job_list"));
    }

    /**
     * Запустить обработку прайса
     *
     * @param \Nitra\IntegraBundle\Entity\Model\JobInterface $Job прайс
     *
     * @return bool
     */
    protected function jobExec(\Nitra\IntegraBundle\Entity\Model\JobInterface $Job) 
    {
        // получить все job
        $jobs = $this->getIntegraParameters()->get('jobs[handlers]', array(), true);

        // проверить загрузчик
        if (!isset($jobs[$Job->getName()])) {
            $this->get('session')->getFlashBag()->add('error', 'Не найден обработчик прайса.');
            return false;
        }

        // запускаемый файл job
        $jobFileSh = $this->getIntegraParameters()->get('jobs[handler_dir]', null, true).$jobs[$Job->getName()].'.sh';
        // проверить файл прайса
        if (!is_file($jobFileSh) || !file_exists($jobFileSh)) {
            $this->get('session')->getFlashBag()->add('error', 'Не найден файл-обработчик прайса.');
            return false;
        }

        // если используется стандартный загрузчик
        if ($Job->getIsStandart()) {
            // запустить стандартный загрузчик 
            $output = shell_exec($jobFileSh. ' --context_param id='.$Job->getId());
        } else {
            // запуск индивидуального загрузчика
            $output = shell_exec($jobFileSh . ' 2>&1 > /dev/null &');
        }

        // если результат содержить exeption
        if (stristr($output, 'exeption')) {
            $this->get('session')->getFlashBag()->add('error', 'Ошибка обработки прайса.');
            return false;
        }

        // прайс загружен успешно
        $this->get('session')->getFlashBag()->add('success', 'Прайс загружен.');
        return true;
    }

    /**
     * Применение входящих и выходящих правил для прайса
     *
     * @param \Nitra\IntegraBundle\Entity\Model\JobInterface $Job - прайс
     *
     * @throws \Exception
     */
    protected function acceptRules(\Nitra\IntegraBundle\Entity\Model\JobInterface $Job)
    {
        $this->job = $Job;
        set_time_limit(600);
        $supplierId = $Job->getWarehouse()->getSupplier()->getId();
        $warehouseQb = $this->getIntegraParameters()->get('em')
            ->createQueryBuilder()
            ->select('w')
            ->from($this->getIntegraParameters()->getEntity('warehouse'), 'w')
            ->where('w.supplier = ' . $supplierId);
        $warehouses = $warehouseQb->getQuery()->getResult();
        $warehousesId = array();
        foreach ($warehouses as $warehouse) {
            $warehousesId[] = $warehouse->getId();
        }

        // входящие правила
        $InRules = $this->getIntegraParameters()->get('dm')
            ->getRepository($this->getIntegraParameters()->getDocument('inRules'))
            ->findBy(
                array('warehouseId' => array( '$in' => $warehousesId )),
                array('sortOrder' => -1)
            );
        $this->acceptInRules($InRules);

        // исходящие правила
        $OutRules = $this->getIntegraParameters()->get('dm')
            ->getRepository($this->getIntegraParameters()->getDocument('outRules'))
            ->findBy(
                array(
                    '$or' => array(
                        array('supplierId' => $supplierId),
                        array('supplierId' => array('$exists' => false)),
                    )
                ),
                array('sortOrder' => -1)
            );        
        $this->acceptOutRules($OutRules);
    }

    /**
     * Принять исходящие правила
     *
     * @param array $OutRules массив исходящиз правил
     */
    protected function acceptOutRules($OutRules)
    {
        $success     = 0;
        $errors      = 0;
        $prodUpdates = array();
        $time        = 0;
        foreach($OutRules as $OutRule) {
            $start = microtime(true);
            $js    = $this->getRulesProcessor()->getOutRuleJS($OutRule);
            $ret   = $this->getRulesProcessor()->executeJs($js);
            $time += microtime(true) - $start;

            if ($ret['ok']) {
                $success ++;
            } else {
                $errors ++;
            }
            if (isset($ret['retval'])) {
                foreach ($ret['retval'] as $prod) {
                    if (!in_array($prod, $prodUpdates)) {
                        $prodUpdates[] = $prod;
                    }
                }
            }
        }
        if (!count($OutRules)) {
            $msg = 'Выходящие правила отсутсвуют.';
            $type = 'warning';
        } elseif ($success > 0 && $errors > 0) {
            $msg = 'Выходящие правила применены с ошибками(' . $errors . '/' . ($success+$errors) . '). Обновлено ' . count($prodUpdates) . ' товар(ов).';
            $type = 'warning';
        } elseif ($success > 0) {
            $msg = 'Все выходящие правила применены ('. $success .'). Обновлено ' . count($prodUpdates) . ' товар(ов).';
            $type = 'success';
        } elseif ($errors > 0) {
            $msg = 'Выходящие правила не применены (' . $errors . ' правил)';
            $type = 'error';
        } else {
            $msg = 'Товары для найденных правил отсутсвуют';
            $type = 'warning';
        }
        $msg .= ' Затрачено ' . $this->getRulesProcessor()->normalizeTime($time);
        $this->get('session')->getFlashBag()->add($type, $msg);
    }

    /**
     * Применить входящие правила
     *
     * @param array $InRules массив входящих правил
     */
    protected function acceptInRules($InRules)
    {
        $success     = 0;
        $errors      = 0;
        $prodUpdates = array();
        $time        = 0;
        foreach ($InRules as $InRule) {
            $start = microtime(true);
            $js    = $this->getRulesProcessor()->getInRuleJS($InRule);
            $ret   = $this->getRulesProcessor()->executeJs($js);
            $time += microtime(true) - $start;
            if ($ret['ok']) {
                $success++;
            } else {
                $errors++;
            }
            if (isset($ret['retval'])) {
                foreach ($ret['retval'] as $prod) {
                    if (!in_array($prod, $prodUpdates)) {
                        $prodUpdates[] = $prod;
                    }
                }
            }
        }
        if (!count($InRules)) {
            if ($this->job && $this->job->getWarehouse()->getSupplier()) {
                $msg = 'Правила для поставщика "' . $this->job->getWarehouse()->getSupplier()->getName() . '" отсутсвуют.';
            } else {
                $msg = 'Правила входа отсутсвуют.';
            }
            $type = 'warning';
        } elseif ($success > 0 && $errors > 0) {
            $msg  = 'Входящие правила применены с ошибками(' . $errors . '/' . ($success+$errors) . '). Обновлено ' . count($prodUpdates) . ' сток(ов).';
            $type = 'warning';
        } elseif ($success > 0) {
            $msg  = 'Все входящие правила применены ('. $success .'). Обновлено ' . count($prodUpdates) . ' сток(ов).';
            $type = 'success';
        } elseif ($errors > 0) {
            $msg  = 'Входящие правила не применены (' . $errors . ' правил)';
            $type = 'error';
        } else {
            if ($this->job && $this->job->getWarehouse()->getSupplier()) {
                $msg = 'Стоки для правил поставщика "' . $this->job->getWarehouse()->getSupplier()->getName() . '" отсутсвуют';
            } else {
                $msg = 'Стоки для правил входа отсутсвуют';
            }
            $type = 'warning';
        }
        $msg .= ' Затрачено ' . $this->getRulesProcessor()->normalizeTime($time);
        $this->get('session')->getFlashBag()->add($type, $msg);
    }
}