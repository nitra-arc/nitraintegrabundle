<?php

namespace Nitra\IntegraBundle\Controller\Job;

use Admingenerated\NitraIntegraBundle\BaseJobController\EditController as BaseEditController;
use Symfony\Component\HttpFoundation\Response;

class EditController extends BaseEditController
{
    /**
     * Получить параметры интегры
     * @return \Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters
     */
    protected function getIntegraParameters()
    {
        return $this->get('nitra_integra.parameters');
    }

    /**
     * Получить превью прайса
     * @return \Nitra\IntegraBundle\Lib\JobPreview\JobPreview
     */
    protected function getJobPreview()
    {
        return $this->get('nitra_integra.job.preview');
    }

    /**
     * {@inheritdoc}
     * добавляем превью прайса через render
     * исключаем PHP Strict Standards:  Declaration ... может принадлежать любому namespace
     */
    public function render($view, array $parameters = array(), Response $response = null)
    {

        // обнулить начальное значение рещультат превью файла
        $parameters['jobPreview'] = null;

        // получить превью файла прайса
        $jobClass = $this->getIntegraParameters()->getEntity('job');
        if (isset($parameters['Job']) && $parameters['Job'] instanceof $jobClass
            // если используется стандартный загрузчик
            && $parameters['Job']->getIsStandart()
            && $this->get('request')->getMethod() == 'GET'
        ) {
            // получить превью файла прайса
            $jobPreview = $this->getJobPreview()->preview($parameters['Job']);

            // отображение ошибок
            // если есть данные парсинга файла прайса
            if ($jobPreview && isset($jobPreview['errors']) &&
                // если в результате парсинга есть ошибки
                $jobPreview['errors']
            ) {
                // создать отображение ошибок
                foreach($jobPreview['errors'] as $error) {
                    $this->get('session')->getFlashBag()->add($error['type'],  $error['message']);
                }
            }

            // добавить в массив параметров превью файла
            $parameters['jobPreview'] = $jobPreview;
        }

        // отобразить родителем
        return parent::render($view, $parameters, $response);
    }

    /**
     * Получить тип формы прайса
     * @return \Nitra\IntegraBundle\Form\Type\Job\EditType
     */
    protected function getEditType()
    {
        // получить форму родителем
        $type = parent::getEditType();
        // добавить параметры интегры
        $type->setIntegraParameters($this->getIntegraParameters());
        // вернуть форму
        return $type;
    }
}