<?php

namespace Nitra\IntegraBundle\Controller\Supplier;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Nitra\IntegraBundle\Form\Type\Supplier\AnalyticsFiltersType;

/**
 * Аналитика поставщиков
 */
class AnalyticsController extends Controller
{
    /**
     * Получить параметры интегры
     * @return \Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters
     */
    protected function getIntegraParameters()
    {
        return $this->get('nitra_integra.parameters');
    }

    /**
     * аналитика поставщиков
     * кол-во товаров на складе: подвязанных, не подвязанных, в чрном списке
     * @Route("/", name="Nitra_IntegraBundle_SupplierAnalytics_list")
     * @Template("NitraIntegraBundle:SupplierAnalytics:index.html.twig")
     */
    public function indexAction()
    {

        // форма фильтра
        $form = $this->getFilterForm();

        // запрос получения поставщиков
        $query = $this->getIntegraParameters()
            ->get('em')
            ->createQueryBuilder()
            ->select('s')
            ->from($this->getIntegraParameters()->getEntity('supplier'), 's', 's.id')
            ->orderBy('s.name', 'ASC');

        // добавить в запрос фильтры
        $this->processFilters($query);

        // получить поставщиков
        $suppliers = $query
            ->getQuery()
            ->getArrayResult();

        // склады поставщиков
        $warehouses = $this->getIntegraParameters()
            ->get('em')
            ->createQueryBuilder()
            ->select('w, s')
            ->from($this->getIntegraParameters()->getEntity('warehouse'), 'w', 'w.id')
            ->innerJoin('w.supplier', 's')
            ->where('s.id IN(:supplierIds)')->setParameter('supplierIds', array_keys($suppliers))
            ->getQuery()
            ->getArrayResult();

        // обойти все склады
        foreach($warehouses as $warehouse) {
            // поставщик
            $supplier = $warehouse['supplier'];

            // проверить хранилище ID складов у поставщика
            if (!isset($suppliers[$supplier['id']]['warehouseIds'])) {
                $suppliers[$supplier['id']]['warehouseIds'] = array();
            }

            // запомнить ID склада для поставщика
            $suppliers[$supplier['id']]['warehouseIds'][] = $warehouse['id'];
        }

        // итоговый суммарный массив данных
        $total = array(
            // кол-во подвязанных товаров
            'countBinded' => 0,
            // кол-во товаров в черном списке
            'countBlack' => 0,
            // кол-во товаров всего
            'countTotal' => 0,
        );

        // обойти всех поставщиков
        // получить аналитику по каждому поставщику
        foreach($suppliers as $supplierId => $supplier) {

            // кол-во подвязанных товаров
            $countBinded = 0;
            // кол-во товаров в черном списке
            $countBlack = 0;
            // кол-во товаров всего
            $countTotal = 0;

            // если у поставщика есть склады
            if ($supplier['warehouseIds']) {

                // получить стоки складов поставщика
                $stocks = $this->getIntegraParameters()
                    ->get('dm')
                    ->createQueryBuilder($this->getIntegraParameters()->getDocument('stock'))
                    ->field('warehouseId')->in($supplier['warehouseIds'])
                    ->field('quantity')->gt(0)
                    ->hydrate(false)
                    ->getQuery()
                    ->execute()->toArray();

                // кол-во товаров всего
                $countTotal = count($stocks);

                // обойти все стоки поставщика
                foreach($stocks as $stock) {
                    // если подвязанный товар
                    if (isset($stock['productId']) && !is_null($stock['productId'])) {
                        ++$countBinded;
                    }

                    // если в черном списке
                    if (isset($stock['inBlackList']) && $stock['inBlackList']) {
                        ++$countBlack;
                    }
                }
            }

            // обновить счетчики поставщика
            $suppliers[$supplierId]['countBinded'] = $countBinded;
            $suppliers[$supplierId]['countBlack'] = $countBlack;
            $suppliers[$supplierId]['countTotal'] = $countTotal;

            // обновить итогоый массив
            $total['countBinded'] += $countBinded;
            $total['countBlack'] += $countBlack;
            $total['countTotal'] += $countTotal;
        }

        // передаем данные в шаблон
        return array(
            'suppliers' => $suppliers,
            'total'     => $total,
            'form'      => $form->createView(),
        );
    }

    /**
     * детализация предложений поставщика по каждому складу
     * @Route("/details", name="Nitra_IntegraBundle_SupplierAnalytics_details")
     * @Method({"POST"})
     * @Template("NitraIntegraBundle:SupplierAnalytics:details.html.twig")
     */
    public function detailsAction()
    {

        // получить поставщика
        $supplier = $this->getIntegraParameters()
            ->get('em')
            ->getRepository($this->getIntegraParameters()->getEntity('supplier'))
            ->find($this->getRequest()->get('supplierId', false));

        // поставщик не найден
        if (!$supplier) {
            $errorMessage = $this->get('translator')->trans('Поставщик не найден.');
            throw new NotFoundHttpException($errorMessage);
        }

        //получаем склады поставщика
        $warehouses = $this->getIntegraParameters()->get('em')->createQueryBuilder()
            ->select('w')
            ->from($this->getIntegraParameters()->getEntity('warehouse'), 'w', 'w.id')
            ->where('w.supplier = :supplierId')
            ->setParameter('supplierId', $supplier->getId())
            ->getQuery()
            ->getArrayResult();

        // проверить склады
        if (!$warehouses) {
            $errorMessage = $this->get('translator')->trans('У поставщика "%supplier%" нет ни одного склада.', array('%supplier%' => (string)$supplier));
            throw new NotFoundHttpException($errorMessage);
        }

        // обойти все склад поставщика
        foreach($warehouses as $warehouseId => $warehouse) {

            // получить стоки склада поставщика
            $stocks = $this->getIntegraParameters()
                ->get('dm')
                ->createQueryBuilder($this->getIntegraParameters()->getDocument('stock'))
                ->field('warehouseId')->equals($warehouse['id'])
                ->field('quantity')->gt(0)
                ->hydrate(false)
                ->getQuery()
                ->execute()->toArray();

            // кол-во подвязанных товаров
            $countBinded = 0;
            // кол-во товаров в черном списке
            $countBlack = 0;
            // кол-во товаров всего
            $countTotal = count($stocks);

            // обойти все стоки
            foreach($stocks as $stock) {
                // если подвязанный товар
                if (isset($stock['productId']) && !is_null($stock['productId'])) {
                    ++$countBinded;
                }

                // если в черном списке
                if (isset($stock['inBlackList']) && $stock['inBlackList']) {
                    ++$countBlack;
                }
            }

            // обновить счетчики склада
            $warehouses[$warehouseId]['countBinded'] = $countBinded;
            $warehouses[$warehouseId]['countBlack'] = $countBlack;
            $warehouses[$warehouseId]['countTotal'] = $countTotal;
        }

        // передаем данные в шаблон
        return array(
            'supplier'      => $supplier,
            'warehouses'    => $warehouses,
        );
    }

    /**
     * Установить фильтр аналитики
     * @Route("/filters", name="Nitra_IntegraBundle_SupplierAnalytics_filters")
     *
     */
    public function filtersAction()
    {
        // сбросить фильтр
        if ($this->get('request')->get('reset')) {
            $this->setFilters(array());
            return new RedirectResponse($this->generateUrl('Nitra_IntegraBundle_SupplierAnalytics_list'));
        }

        // получить фильтры из POST
        if ($this->getRequest()->getMethod() == 'POST') {
            $form = $this->getFilterForm();
            $form->bind($this->get('request'));
            if ($form->isValid()) {
                $filters = $form->getViewData();
            }
        }

        // получить фильтры из GET
        if ($this->getRequest()->getMethod() == 'GET') {
            $filters = $this->getRequest()->query->all();
        }

        // установить фильтры
        if (isset($filters)) {
            $this->setFilters($filters);
        }

        // перенаправить пользователя на список
        return new RedirectResponse($this->generateUrl('Nitra_IntegraBundle_SupplierAnalytics_list'));
    }

    /**
     * Store in the session service the current filters
     * @param array the filters
     */
    protected function setFilters($filters)
    {
        $this->get('session')->set('Nitra\IntegraBundle\SupplierAnalytics\Filters', $filters);
    }

    /**
     * Get filters from session
     */
    protected function getFilters()
    {
        $filters = $this->get('session')->get('Nitra\IntegraBundle\SupplierAnalytics\Filters', array());
        return $filters;
    }

    /**
     * обновить запрос получения списка
     * @param \Doctrine\ORM\QueryBuilder $query - запрос получения списка
     */
    protected function processFilters(\Doctrine\ORM\QueryBuilder $query)
    {
        // получить фильтры
        $filters = $this->getFilters();

        // фильтр по имени поставщика
        if (isset($filters['name']) && $filters['name']) {
            $r = $query->getRootAlias();
            $query
                ->andWhere($r . '.name LIKE :name')
                ->setParameter('name', '%' . $filters['name'] . '%');
        }

    }

    /**
     * Получить форму фильтра
     */
    protected function getFilterForm()
    {
        $filters = $this->getFilters();
        return $this->createForm($this->getFiltersType(), $filters);
    }

    /**
     * Получить тип формы фильтра
     * @return AnalyticsFiltersType
     */
    protected function getFiltersType()
    {
        $type = new AnalyticsFiltersType();
        return $type;
    }
}