<?php

namespace Nitra\IntegraBundle\Controller\Warehouse;

use Admingenerated\NitraIntegraBundle\BaseWarehouseController\ShowController as BaseShowController;

class ShowController extends BaseShowController
{
    /**
     * {@inheritdoc}
     */
    protected function getQueryBuilder($pk)
    {
        // получить запрос родитлея
        $query = parent::getQueryBuilder($pk);

        // добавить в запрос огрниачение по складу поставщика
        $r = $query->getRootAlias();
        $query
            ->andWhere($r.'.delivery IS NULL')
            ->andWhere($r.'.supplier IS NOT NULL');

        // вернуть запрос
        return $query;
    }
}