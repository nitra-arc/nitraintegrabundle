<?php

namespace Nitra\IntegraBundle\Controller\Warehouse;

use Admingenerated\NitraIntegraBundle\BaseWarehouseController\ListController as BaseListController;

class ListController extends BaseListController
{
    /**
     * {@inheritdoc}
     */
    protected function processQuery($query)
    {
        // добавить в запрос огрниачение по складу поставщика
        $r = $query->getRootAlias();
        $query
            ->andWhere($r.'.delivery IS NULL')
            ->andWhere($r.'.supplier IS NOT NULL');

        // вернуть запрос
        return $query;
    }
}