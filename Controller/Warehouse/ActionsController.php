<?php

namespace Nitra\IntegraBundle\Controller\Warehouse;

use Admingenerated\NitraIntegraBundle\BaseWarehouseController\ActionsController as BaseActionsController;

class ActionsController extends BaseActionsController
{
    /**
     * {@inheritdoc}
     */
    protected function getObjectQueryBuilder($pk)
    {
        // получить запрос родитлея
        $query = parent::getObjectQueryBuilder($pk);

        // добавить в запрос огрниачение по складу поставщика
        $r = $query->getRootAlias();
        $query
            ->andWhere($r.'.delivery IS NULL')
            ->andWhere($r.'.supplier IS NOT NULL');

        // вернуть запрос
        return $query;
    }
}