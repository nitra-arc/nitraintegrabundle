<?php

namespace Nitra\IntegraBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nitra\IntegraBundle\Form\Type\IntegraFiltersLeft;
use Nitra\IntegraBundle\Form\Type\IntegraFiltersRight;

/**
 * Связывание товаров
 */
class IntegraController extends Controller
{
    /**
     * Получить параметры интегры
     *
     * @return \Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters
     */
    protected function getIntegraParameters()
    {
        return $this->get('nitra_integra.parameters');
    }

    /**
     * @return \Doctrine\ODM\MongoDB\DocumentManager
     */
    protected function getDocumentManager()
    {
        return $this->getIntegraParameters()->get('dm');
    }

    /**
     * @Route("/", name="Nitra_IntegraBundle_Bind_list")
     * @Template()
     *
     * @return array Template context
     */
    public function indexAction()
    {
        // фильтры слева
        $filterLeft = $this->getFiltersLeftForm();

        // фильтры справа
        $filterRight = $this->getFiltersRightForm();

        // передать данные в шаблон
        return array(
            'filterLeft'  => $filterLeft->createView(),
            'filterRight' => $filterRight->createView(),
        );
    }

    /**
     * Левая панель
     * @Route("/left-panel", name="Nitra_IntegraBundle_Bind_left_panel")
     * Method({"POST"})
     * @Template()
     *
     * @param Request $request
     *
     * @return array
     *
     * @throws \Exception
     */
    public function leftPanelAction(Request $request)
    {
        // форма фильтра
        $form = $this->getFiltersLeftForm();

        // получить все данные формы
        $formData = $request->request->get($form->getName());
        $productSearchReg = '';

        // категория товара
        $categoryId = $formData['category_id'];
        // бренд товара
        $brandId = $formData['brand'];

        // фраза поиска по продуктам
        $productSearch = trim($formData['name'], '/)([]{}*|\\!@#$%^&-=_+:;\'"<>,.? ');
        $productSearch = mb_ereg_replace("/[[:blank:]]+/", ' ', $productSearch);
        $productSearch = preg_quote($productSearch, '/');

        // ксли введено несколко фраз - разбиваем по пробелу
        if ($productSearch) {
            if (strstr($productSearch, ' ')) {
                $productSearchReg = '/(?=.*' . str_replace(' ', ')(?=.*', $productSearch) . ')/i';
            } else {
                $productSearchReg = '/' . $productSearch . '/ui';
            }
        }

        // поиск товаров по найденным моделям
        $qb = $this->getDocumentManager()
            ->createQueryBuilder($this->getIntegraParameters()->getDocument('product'));

        // Поиск по категории и/или бренду
        if ($categoryId || $brandId) {
            $modelsQb = $this->getDocumentManager()
                ->createQueryBuilder('NitraMainBundle:Model')
                ->distinct('_id');
            if ($categoryId) {
                $modelsQb->field('category.id')->equals($categoryId);
            }
            if ($brandId) {
                $modelsQb->field('brand.id')->equals($brandId);
            }
            $modelsIds = $modelsQb->getQuery()->execute()->toArray();

            $qb->field('model.$id')->in($modelsIds);
        }

        // фильтр по подвязанности
        if (!is_null($formData['binding']) && $formData['binding'] != '') {
            $boundStrIds = $this->getDocumentManager()
                ->createQueryBuilder($this->getIntegraParameters()->getDocument('stock'))
                ->select()
                ->distinct('productId')
                ->getQuery()
                ->execute()
                ->toArray();

            $boundIds = array_map(function($id) {
                return new \MongoId($id);
            }, $boundStrIds);

            $qb->field('_id')->{$formData['binding'] ? 'notIn' : 'in'}($boundIds);
        }

        // Поиск по введенной фразе
        if ($productSearchReg) {
            // по имени товара для поиска
            $qb->addOr($qb->expr()->field('fullNameForSearch')->equals(new \MongoRegex($productSearchReg)));
            // по имени подвязанных стоков
            $qb->addOr($qb->expr()->field('_id')->in($this->searchBindedStocks($productSearchReg)));
        }

        // получить продукты
        $products = $qb->sort('name', 'asc')
            ->skip($formData['leftPage'] * 20) // Смещение
            ->limit(20)                        // лимит отображения товаров
            ->getQuery()
            ->execute();

        // Поиск всех складов по поставщикам
        $suppliersByWarehouse = $this->getSuppliersByWarehouse();

        $productsToTemplate = array();
        if ($products) {
            foreach ($products as $product) {
                // Стоки, связанные с товаром в БД магазина
                $stocks = $this->getDocumentManager()
                    ->createQueryBuilder($this->getIntegraParameters()->getDocument('stock'))
                    ->field('productId')->equals($product->getId())
                    ->field('warehouseId')->in(array_keys($suppliersByWarehouse))
                    ->getQuery()
                    ->execute();

                $productsToTemplate[] = array(
                    'obj'       => $product,
                    'stocks'    => $stocks,
                );
            }
        }

        // вернуть результирующий массив
        return array(
            'products'               => $productsToTemplate,
            'suppliers_by_warehouse' => $suppliersByWarehouse,
            'out_rules'              => $this->getAllOutRules(),
            'store_id'               => $this->getStoreId(),
        );
    }

    /**
     * Правая панель
     *
     * @Route("/right-panel", name="Nitra_IntegraBundle_Bind_right_panel")
     * @Method({"POST"})
     * @Template()
     *
     * @param Request $request
     *
     * @return array
     *
     * @throws \Exception
     */
    public function rightPanelAction(Request $request)
    {
        // Поиск НЕПОДВЯЗАННЫХ товаров
        // - по названию
        // - по поставщику
        $stocksQb = $this->getDocumentManager()
            ->createQueryBuilder($this->getIntegraParameters()->getDocument('stock'));

        $stocksQb->addAnd(
            $stocksQb->expr()
                ->addOr($stocksQb->expr()->field('productId')->equals(null))
                ->addOr($stocksQb->expr()->field('productId')->exists(false))
        );

        // форма фильтра
        $form = $this->getFiltersRightForm();

        // получить все данные формы
        $formData = $request->request->get($form->getName());

        // массив складов выбранного поставщика
        $supplierId = $formData['supplier'];
        // фраза для поиска по названию товара (прайса)
        $stockSearch = $formData['name'];

        if (!array_key_exists('all', $formData) || !$formData['all']) {
            $stocksQb->addAnd(
                $stocksQb->expr()->addOr(
                    $stocksQb->expr()->field('inBlackList')->equals(false)
                )->addOr(
                    $stocksQb->expr()->field('inBlackList')->exists(false)
                )
            );
        }
        // Поиск всех поставщиков
        $suppliersQb = $this->getIntegraParameters()->get('em')->createQueryBuilder()
            ->select('w.id, s.name, s.priceInColumn1, s.priceInColumn2, s.priceInColumn3, s.priceInColumn4')
            ->from($this->getIntegraParameters()->getEntity('supplier'), 's')
            ->innerJoin($this->getIntegraParameters()->getEntity('warehouse'), 'w', 'WITH', 'w.supplier = s.id')
            ->getQuery()
            ->getResult();
        $suppliersByWarehouse = array();
        foreach ($suppliersQb as $supplier) {
            $suppliersByWarehouse[$supplier['id']] = $supplier;
        }

        // Поиск по поставщику, если выбран
        if ($supplierId) {
            $warehouseQb = $this->getIntegraParameters()->get('em')->createQueryBuilder()
                ->select('w')
                ->from($this->getIntegraParameters()->getEntity('warehouse'), 'w');
            $warehouseQb
                ->where('w.supplier = ' . $supplierId);
            $warehouses = $warehouseQb
                ->getQuery()
                ->getResult();
            $wIds = array();
            foreach ($warehouses as $w) {
                $wIds[] = $w->getId();
            }

            $stocksQb->field('warehouseId')->in($wIds);
        }

        // Обработка фразы для поиска
        if ($stockSearch) {
            // Добавление пробела до и после последовательности цифр
            $stockSearch = preg_replace_callback("/\d+/", function ($matches) {
                return " " . $matches[0] . " ";
            }, $stockSearch);

            $stockSearch = trim($stockSearch, '/)([]{}*|\\!@#$%^&-=_+:;\'"<>,.? ');
            // Исключаем все не символы или цифры
            $stockSearch = mb_ereg_replace("/[^a-zA-ZА-Яа-я0-9\s]/", " ", $stockSearch);
            // Исключаем лишние пробелы
            $stockSearch = mb_ereg_replace("/[[:blank:]]+/", ' ', $stockSearch);
        }

        // Если введено несколько фраз - разбиваем по пробелу, ищем по каждой
        if ($stockSearch) {
            if (strstr($stockSearch, ' ')) {
                foreach (explode(' ', $stockSearch) as $keyword) {
                    $stocksQb->addAnd(
                        $stocksQb->expr()->field('priceName')->equals(array(
                            '$regex' => new \MongoRegex('/' . $keyword . '/i'),
                        ))
                    );
                }
            } else {
                $stocksQb->field('priceName')->equals(array(
                    '$regex' => new \MongoRegex('/' . $stockSearch . '/i'),
                ));
            }
        }

        $offset = $formData['rightPage'] * 20; // Смещение
        $limit  = 20;                          // К-во отображаемых товаров
        $stocks = $stocksQb
            ->limit($limit)
            ->skip($offset)
            ->getQuery()
            ->execute()->toArray();

        return array(
            'stocks'                 => $stocks,
            'suppliers_by_warehouse' => $suppliersByWarehouse,
        );
    }

    /**
     * Связывание товаров
     *
     * @Route("/dobind", name="Nitra_IntegraBundle_Bind_dobind")
     * @Method({"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse|Response
     *
     * @throws \Doctrine\ODM\MongoDB\LockException
     * @throws \Exception
     */
    public function bindAction(Request $request)
    {
        $rData = $request->request->all();
        if (!$rData['product'] || !$rData['unbinds']) {
            return new Response('Некорректные параметры', 404);
        }

        $qb = $this->getDocumentManager()
            ->createQueryBuilder($this->getIntegraParameters()->getDocument('stock'))
            ->field('_id')->in($rData['unbinds']);

        $stocks = $qb->getQuery()->execute();
        foreach ($stocks as $stock) {
            $stock->setProductId($rData['product']);
        }
        $this->getDocumentManager()->flush();

        $twig = $this->get('templating');

        return new JsonResponse(array(
            'product'           => $twig->render('NitraIntegraBundle:Integra:leftPanel.html.twig', array(
                'store_id'                  => $this->getStoreId(),
                'suppliers_by_warehouse'    => $this->getSuppliersByWarehouse(),
                'products'                  => array(
                    array(
                        'obj'               => $this->getDocumentManager()->getRepository($this->getIntegraParameters()->getDocument('product'))->find($rData['product']),
                        'stocks'            => $this->getDocumentManager()->getRepository($this->getIntegraParameters()->getDocument('stock'))->findByProductId($rData['product']),
                    ),
                ),
                'out_rules'                 => $this->getAllOutRules(),
            ))
        ));
    }

    /**
     * Отвязывание товаров
     *
     * @Route("/unbind", name="Nitra_IntegraBundle_Bind_unbind")
     * @Method({"POST"})
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws \Doctrine\ODM\MongoDB\LockException
     * @throws \Exception
     */
    public function unbindAction(Request $request)
    {
        if (!$request->request->has('stockId')) {
            return new Response('Не указан сток.', 404);
        }
        $stockId = $request->request->get('stockId');

        $stock = $this->getDocumentManager()->getRepository($this->getIntegraParameters()->getDocument('stock'))->find($stockId);
        $stock->setProductId(null);
        $stock->setStorePrice(null);
        $stock->setPriceIn(null);
        $this->getDocumentManager()->flush();

        return new Response('ok');
    }

    /**
     * Добавление/удаление стоков в/из черного списка
     *
     * @Route("/black-list", name="Nitra_IntegraBundle_Bind_black_list")
     * @Method({"POST"})
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function blackListAction(Request $request)
    {
        $stockId = $request->request->get('id');
        $inBlackList = ($request->request->get('checked') == 'true') ? true : false;

        $stock = $this->getDocumentManager()->find($this->getIntegraParameters()->getDocument('stock'), $stockId);
        $stock->setInBlackList($inBlackList);
        $this->getDocumentManager()->flush($stock);

        return new Response();
    }

    /**
     * Получить форму фильтра справа
     *
     * @return \Symfony\Component\Form\Form
     */
    public function getFiltersRightForm()
    {
        $filters = array();
        return $this->createForm($this->getFiltersRightType(), $filters);
    }

    /**
     * Получить форму фильтра слева
     *
     * @return \Symfony\Component\Form\Form
     */
    public function getFiltersLeftForm()
    {
        $filters = array();
        return $this->createForm($this->getFiltersLeftType(), $filters);
    }

    /**
     * Получить тип формы фильтра слева
     *
     * @return IntegraFiltersLeft
     */
    protected function getFiltersLeftType()
    {
        $type = new IntegraFiltersLeft();
        $type->setIntegraParameters($this->getIntegraParameters());

        return $type;
    }

    /**
     * Получить тип формы фильтра справа
     *
     * @return IntegraFiltersRight
     */
    protected function getFiltersRightType()
    {
        $type = new IntegraFiltersRight();
        $type->setIntegraParameters($this->getIntegraParameters());

        return $type;
    }

    /**
     * Получить ID магазина
     *
     * @return mixed
     *          integer - ID магазина
     *          null    - магазин не определен
     */
    protected function getStoreId()
    {
        //  получить магазин
        $store = $this->getStore();

        // вернуть ID магазина
        return ($store)
            ? $store->getId()
            : null; // магазин не определен
    }

    /**
     * Получить магазин
     *
     * @return \Nitra\IntegraBundle\Document\Model\StoreInterface
     *
     * @throws \Doctrine\ODM\MongoDB\LockException
     * @throws \Exception
     */
    protected function getStore()
    {
        // проверить ниличие переменной ID магазина для минитетрадки
        // если есть переменная то интегра в минитетрадке
        if ($this->container->hasParameter('nitra_minitetradka_store_id')) {
            // минитетрадка
            // филиала в минитетрадке нет
            // получить магазин для минитетрадки по параметру
            $store = $this->getDocumentManager()
                ->getRepository($this->getIntegraParameters()->getDocument('store'))
                ->find($this->container->getParameter('nitra_minitetradka_store_id'));

            // проверить магазин
            if ($store) {
                // магазин найден, вернуть магазин
                return $store;
            }

            // вернуть первый найденный магази
            // если магазин не найден врнет null
            return $this->getDocumentManager()
                ->getRepository($this->getIntegraParameters()->getDocument('store'))
                ->findOneBy(array());
        }

        // получение магазина для полной тетрадки
        // получаем магазин из филиала
        $session = $this->getRequest()->getSession();
        $filial  = $this->getIntegraParameters()->get('em')->find('NitraFilialBundle:Filial', $session->get('myFilialDefault'));

        // вернуть магазин из филиала пользователя по умолчанию
        return $this->getDocumentManager()
            ->find($this->getIntegraParameters()->getDocument('store'), $filial->getStoreId());
    }

    /**
     * Поиск всех складов по поставщикам
     *
     * @return array
     */
    protected function getSuppliersByWarehouse()
    {
        $suppliersQb = $this->getIntegraParameters()->get('em')->createQueryBuilder()
            ->select('w.id, s.name')
            ->from($this->getIntegraParameters()->getEntity('supplier'), 's')
            ->innerJoin($this->getIntegraParameters()->getEntity('warehouse'), 'w', 'WITH', 'w.supplier = s.id')
            ->getQuery()
            ->getResult();
        $suppliersByWarehouse = array();
        foreach ($suppliersQb as $supplier) {
            $suppliersByWarehouse[$supplier['id']] = $supplier['name'];
        }

        return $suppliersByWarehouse;
    }

    /**
     * Получить id товаров из стоков, у которых priceName подходит под поисковый запрос
     *
     * @param string $keyWords
     *
     * @return array array of \MongoId
     */
    protected function searchBindedStocks($keyWords)
    {
        $stocksQb = $this->getDocumentManager()
            ->createQueryBuilder($this->getIntegraParameters()->getDocument('stock'))->distinct('productId');
        $stocksQb->addAnd($stocksQb->expr()->field('productId')->notEqual(null));
        $stocksQb->addAnd($stocksQb->expr()->field('productId')->exists(true));
        $stocksQb->field('priceName')->equals(array(
            '$regex' => new \MongoRegex($keyWords),
        ));
        $productIds = $stocksQb->getQuery()->execute()->toArray();

        $mongoProductIds = array();
        foreach ($productIds as $productId) {
            $mongoProductIds[] = new \MongoId($productId);
        }

        return $mongoProductIds;
    }

    /**
     * Поиск всех правил формирования цен выхода (OutRules)
     *
     * @return array array of OutRules
     */
    protected function getAllOutRules()
    {
        $outRules = $this->getDocumentManager()
            ->getRepository($this->getIntegraParameters()->getDocument('outRules'))
            ->findAll();

        $formatted = array();
        foreach ($outRules as $outRule) {
            $formatted[$outRule->getId()] = $outRule->getDescription();
        }

        return $formatted;
    }

    /**
     * AutoComplete for parameters
     *
     * @Route("/integra-parameters-auto-complete", name="integra_parameters_auto_complete")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function parameterAutoCompleteAction(Request $request)
    {
        $term       = $request->request->get('term');
        $regex      = trim($term)
            ? '/(?=.*' . implode(')(?=.*', explode(' ', preg_replace('/\s{2,}/', ' ', trim($term)))) . ')/i'
            : '/.*/';

        $parameters = $this->getDocumentManager()
            ->createQueryBuilder($this->getIntegraParameters()->getDocument('parameter'))
            ->field('name')->equals(new \MongoRegex($regex))
            ->sort('name')
            ->getQuery()->execute();

        $result = array();
        foreach ($parameters as $parameter) {
            $result[$parameter->getId()] = $parameter->getName();
        }

        return new JsonResponse($result);
    }

    /**
     * AutoComplete for parameter values
     *
     * @Route("/integra-parameter-values-auto-complete", name="integra_parameter_values_auto_complete")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function parametersValuesAutoCompleteAction(Request $request)
    {
        $id = json_decode($request->request->get('id'), true)['id'];

        if (!$id) {
            return new JsonResponse();
        }

        $values = $this->getDocumentManager()
            ->createQueryBuilder($this->getIntegraParameters()->getDocument('parameterValues'))
            ->hydrate(false)
            ->select('name')
            ->field('parameter.id')->equals($id)
            ->getQuery()
            ->execute()
            ->toArray();

        return new JsonResponse(array_map(function ($value) {
            return $value['name'];
        }, $values));
    }
}