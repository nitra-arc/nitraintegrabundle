<?php

namespace Nitra\IntegraBundle\Controller\OutRules;

use Admingenerated\NitraIntegraBundle\BaseOutRulesController\EditController as BaseEditController;
use Symfony\Component\HttpFoundation\Response;
use Nitra\IntegraBundle\Form\Type\OutRules\EditType;

class EditController extends BaseEditController
{
    /**
     * Получить параметры интегры
     * @return \Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters
     */
    protected function getIntegraParameters()
    {
        return $this->get('nitra_integra.parameters');
    }

    /**
     * получить форму
     */
    protected function getEditType()
    {
        $type = new EditType();
        $type->setIntegraParameters($this->getIntegraParameters());
        return $type;
    }

    /**
     * {@inheritdoc}
     * добавляем доп.параметры для шаблона серез render
     * исключаем PHP Strict Standards:  Declaration ... может принадлежать любому namespace
     */
    public function render($view, array $parameters = array(), Response $response = null)
    {
        // обнулить начальное значение доп.параметров
        $parameters['supplierPricesName'] = null;

        // получить дополнительные параметры
        $outRulesClass = $this->getIntegraParameters()->getDocument('outRules');
        if (isset($parameters['OutRules']) && $parameters['OutRules'] instanceof $outRulesClass) {

            // получить расценки поставщиков
            $suppliers = $this->getIntegraParameters()->get('em')->createQueryBuilder()
                ->select('s.id, s.priceInColumn1, s.priceInColumn2, s.priceInColumn3, s.priceInColumn4')
                ->from($this->getIntegraParameters()->getEntity('supplier'), 's')
                ->getQuery()->getArrayResult();

            // преобразовать масив цен поставщтков в формат расчета
            $supplierPrice = array();
            foreach ($suppliers as $supplier) {
                $id = $supplier['id'];
                unset($supplier['id']);
                foreach ($supplier as $priceName) {
                    $supplierPrice[$id][] = $priceName;
                }
            }

            // обновить доп параметр
            $parameters['supplierPricesName'] = json_encode($supplierPrice);
        }

        // отобразить родителем
        return parent::render($view, $parameters, $response);
    }
}