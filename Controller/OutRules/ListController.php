<?php

namespace Nitra\IntegraBundle\Controller\OutRules;

use Admingenerated\NitraIntegraBundle\BaseOutRulesController\ListController as BaseListController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Nitra\IntegraBundle\Form\Type\OutRules\FiltersType;

class ListController extends BaseListController
{
    /**
     * Получить параметры интегры
     * @return \Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters
     */
    protected function getIntegraParameters()
    {
        return $this->get('nitra_integra.parameters');
    }

    /**
     * Получить форму фильтра
     * @return
     */
    protected function getFiltersType()
    {
        $type = new FiltersType();
        $type->setIntegraParameters($this->getIntegraParameters());
        return $type;
    }

    /**
     * Отображение списка
     */
    public function indexAction()
    {
        // получить фильтры
        $filters = $this->getFilters();

        // фильтр магазин по умолчанию
        if(!isset($filters['store'])) {

            // фильтр магазин по умолчанию первый в справочнике
            $filters['store'] = $this->getDocumentManager()
                ->getRepository($this->getIntegraParameters()->getDocument('store'))
                ->findOneBy(array());

            // запомнить фильтр
            $this->setFilters($filters);
        }

        // отобразить страницу родителем
        return parent::indexAction();
    }

    /**
     * Получить массив доп параметров передаваемый в шаблон
     * @return array
     */
    public function getAdditionalRenderParameters()
    {
        // получить поставщиков
        $suppliers = $this->getIntegraParameters()->get('em')->createQueryBuilder()
            ->select('s.id, s.name')
            ->from($this->getIntegraParameters()->getEntity('supplier'), 's', 's.id')
            ->getQuery()
            ->getArrayResult();

        $filters = $this->getFilters();
        unset($filters['store']);
        foreach (array('category', 'brand', 'supplierId',) as $key) {
            if (array_key_exists($key, $filters) && !$filters[$key]) {
                unset($filters[$key]);
            }
        }

        // вернуть результируюший массив
        return array(
            'suppliers' => $suppliers,
            'filters' => $filters,
        );
    }

    protected function processFilterSupplierid($queryFilter, $filterObject)
    {
        if (isset($filterObject['supplierId']) && null !== $filterObject['supplierId']) {
            $queryFilter->getQuery()->field('supplierId')->equals($filterObject['supplierId']->getId());
        }
    }
    
    protected function setFilters($filters)
    {
        if (isset($filters['supplierId'])) {
            $filters['supplierId'] = array(
                'id'         => $filters['supplierId']->getId(),
                'entityName' => $this->getIntegraParameters()->getEntity('supplier'),
            );
        }

        parent::setFilters($filters);
    }

    protected function getFilters()
    {
        if ($this->getRequest()->query->has('noFilter')) {
            return array();
        }

        $filters = parent::getFilters();
        if (isset($filters['supplierId'])) {
            $filters['supplierId'] = $this->getDoctrine()
                ->getManagerForClass($filters['supplierId']['entityName'])
                ->find($filters['supplierId']['entityName'], $filters['supplierId']['id']);
        }

        return $filters;
    }
    
    /**
     * изменение порядка сортировки
     * @Route("/change-rules-sort-order", name="Nitra_IntegraBundle_change_out_rules_sort_order")
     */
    public function changeSortOrderAction()
    {
        // получить приоритетность, данные сортировки
        $priorities = $this->get('request')->get('priorities', array());
        if (!$priorities) {
            // нет данных изменения приоритетности
            return new Response();
        }

        // получить правила
        $rules = $this->getIntegraParameters()->get('dm')
            ->createQueryBuilder($this->getIntegraParameters()->getDocument('outRules'))
            ->field('id')->in(array_keys($priorities))
            ->getQuery()
            ->execute();

        // проверить полученные правила
        if (!$rules->count()) {
            // нет данных изменения приоритетности
            return new Response();
        }

        // обойти все правила установить приоритетность
        foreach($rules as $rule) {
            // пропустить правило если его нет в массиве сортировки
            if (!isset($priorities[$rule->getId()])) {
                continue;
            }

            // утановить приоритет для правила
            $priority = $priorities[$rule->getId()];
            $rule->setSortOrder((int)$priority);
        }

        // сохранить
        $this->getIntegraParameters()->get('dm')->flush();

        // обновление сортировки завершено успешно
        return new Response();
    }
}