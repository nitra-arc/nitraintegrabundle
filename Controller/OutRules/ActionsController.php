<?php

namespace Nitra\IntegraBundle\Controller\OutRules;

use Admingenerated\NitraIntegraBundle\BaseOutRulesController\ActionsController as BaseActionsController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ActionsController extends BaseActionsController
{
    /**
     * Получить параметры интегры
     *
     * @return \Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters
     */
    protected function getIntegraParameters()
    {
        return $this->get('nitra_integra.parameters');
    }

    /**
     * получить прайсообработчик
     *
     * @return \Nitra\IntegraBundle\Lib\RulesProcessor\RulesProcessor
     */
    protected function getRulesProcessor()
    {
        return $this->get('nitra_integra.rules.processor');
    }

    /**
     * @param \Nitra\IntegraBundle\Document\Model\OutRulesInterface $OutRule    Правило выхода
     * @param bool                                                  $acceptAll  Применяются все или нет
     * @param bool                                                  $isLast     Последнее правило или нет
     *
     * @return array
     */
    protected function acceptAction(\Nitra\IntegraBundle\Document\Model\OutRulesInterface $OutRule, $acceptAll = false, $isLast = false)
    {
        $rulesProcessor = $this->getRulesProcessor();
        $js             = $rulesProcessor->getOutRuleJS($OutRule, $acceptAll, $isLast);

        return $rulesProcessor->executeJs($js);
    }

    /**
     * применить правило
     *
     * @param string $pk ID правила
     *
     * @return RedirectResponse
     *
     * @throws \Exception
     */
    protected function attemptObjectAccept($pk)
    {
        $rulesProcessor = $this->getRulesProcessor();
        $OutRule = $this->getDocumentManager()->find($this->getIntegraParameters()->getDocument('outRules'), $pk);

        $start  = microtime(true);
        $ret    = $this->acceptAction($OutRule);
        if ($ret['ok']) {
            if (count($ret['retval']) > 0) {
                $type = 'success';
                $msg = 'Обновлено ' . count($ret['retval']) . ' товар(ов).';
            } else {
                $type = 'error';
                $msg = 'Товары для данного правила не найдены.';
            }
            $msg .= ' Затрачено ' . $rulesProcessor->normalizeTime(microtime(true) - $start);
        } else {
            $type = 'error';
            $msg = 'Ошибка: ' . $ret['errmsg'];
        }
        $this->get('session')->getFlashBag()->add($type, $msg);

        // перенаправить пользователя на список
        return new RedirectResponse($this->generateUrl("Nitra_IntegraBundle_OutRules_list"));
    }

    /**
     * применить все правила
     *
     * @Route("/accept-all", name="Nitra_IntegraBundle_OutRules_accept_all")
     */
    public function acceptAll()
    {
        $rulesProcessor = $this->getRulesProcessor();
        $OutRules = $this->getDocumentManager()->getRepository($this->getIntegraParameters()->getDocument('outRules'))->findBy(
            array(),
            array('sortOrder' => -1)
        );

        $success        = 0;
        $errors         = 0;
        $prodUpdates    = array();
        $time           = 0;
        foreach ($OutRules as $OutRule) {
            $start = microtime(true);
            $ret = $this->acceptAction($OutRule, true, ($success + $errors) == (count($OutRules) - 1));
            $time += microtime(true) - $start;

            if ($ret['ok']) {
                $success ++;
                foreach ($ret['retval'] as $prod) {
                    if (!in_array($prod, $prodUpdates)) {
                        $prodUpdates[] = $prod;
                    }
                }
            } else {
                $errors ++;
            }
        }
        if ($errors > 0) {
            $msg = 'Правила применены с ошибками(' . $errors . '/' . ($success + $errors) . '). Обновлено ' . count($prodUpdates) . ' товаров.';
            $type = 'warning';
        } else {
            $msg = 'Все правила применены. Обновлено ' . count($prodUpdates) . ' товаров.';
            $type = 'success';
        }
        if ($success == 0) {
            $msg = 'Правила не применены.';
            $type = 'error';
        }
        $msg .= ' Затрачено ' . $rulesProcessor->normalizeTime($time);
        $this->get('session')->getFlashBag()->add($type, $msg);

        // перенаправить пользователя на список
        return new RedirectResponse($this->generateUrl("Nitra_IntegraBundle_OutRules_list"));
    }
}