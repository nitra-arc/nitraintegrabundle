<?php

namespace Nitra\IntegraBundle\Controller\OutRules;

use Admingenerated\NitraIntegraBundle\BaseOutRulesController\NewController as BaseNewController;
use Symfony\Component\HttpFoundation\Response;
use Nitra\IntegraBundle\Form\Type\OutRules\NewType;

class NewController extends BaseNewController
{
    /**
     * Получить параметры интегры
     * @return \Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters
     */
    protected function getIntegraParameters()
    {
        return $this->get('nitra_integra.parameters');
    }

    /**
     * получить форму
     */
    protected function getNewType()
    {
        $type = new NewType();
        $type->setIntegraParameters($this->getIntegraParameters());
        return $type;
    }

    /**
     * {@inheritdoc}
     * добавляем доп.параметры для формы серез createForm
     * исключаем PHP Strict Standards:  Declaration ... может принадлежать любому namespace
     */
    public function createForm($type, $data = null, array $options = array())
    {
        // обнулить начальное значение доп.параметров
        $options['storeFilter'] = null;

        // проверить тип формы
        if ($type instanceof NewType) {
            // получить фильтр листинга
            $filters = $this
                ->get('session')
                ->get('Nitra\IntegraBundle\OutRulesList\Filters', array());

            // обновить доп параметр
            $options['storeFilter'] = (isset($filters['store']['id']))
                    ? $this->getIntegraParameters()->get('dm')->getRepository($this->getIntegraParameters()->getDocument('store'))->find($filters['store']['id'])
                    : null;
        }

        // построить форму родителем
        return parent::createForm($type, $data, $options);
    }

    /**
     * {@inheritdoc}
     * добавляем доп.параметры для шаблона серез render
     * исключаем PHP Strict Standards:  Declaration ... может принадлежать любому namespace
     */
    public function render($view, array $parameters = array(), Response $response = null)
    {
        // обнулить начальное значение доп.параметров
        $parameters['supplierPricesName'] = null;

        // получить дополнительные параметры
        $outRulesClass = $this->getIntegraParameters()->getDocument('outRules');
        if (isset($parameters['OutRules']) && $parameters['OutRules'] instanceof $outRulesClass) {

            // получить расценки поставщиков
            $suppliers = $this->getIntegraParameters()->get('em')->createQueryBuilder()
                ->select('s.id, s.priceInColumn1, s.priceInColumn2, s.priceInColumn3, s.priceInColumn4')
                ->from($this->getIntegraParameters()->getEntity('supplier'), 's')
                ->getQuery()->getArrayResult();

            // преобразовать масив цен поставщтков в формат расчета
            $supplierPrice = array();
            foreach ($suppliers as $supplier) {
                $id = $supplier['id'];
                unset($supplier['id']);
                foreach ($supplier as $priceName) {
                    $supplierPrice[$id][] = $priceName;
                }
            }

            // обновить доп параметр
            $parameters['supplierPricesName'] = json_encode($supplierPrice);
        }

        // отобразить родителем
        return parent::render($view, $parameters, $response);
    }
}