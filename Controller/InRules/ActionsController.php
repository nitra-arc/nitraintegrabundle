<?php

namespace Nitra\IntegraBundle\Controller\InRules;

use Admingenerated\NitraIntegraBundle\BaseInRulesController\ActionsController as BaseActionsController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ActionsController extends BaseActionsController
{
    /**
     * Получить параметры интегры
     *
     * @return \Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters
     */
    protected function getIntegraParameters()
    {
        return $this->get('nitra_integra.parameters');
    }

    /**
     * Получить прайсообработчик
     *
     * @return \Nitra\IntegraBundle\Lib\RulesProcessor\RulesProcessor
     */
    protected function getRulesProcessor()
    {
        return $this->get('nitra_integra.rules.processor');
    }

    /**
     * Применить правило
     *
     * @param \Nitra\IntegraBundle\Document\Model\InRulesInterface $InRule правило
     *
     * @return array
     */
    protected function acceptAction(\Nitra\IntegraBundle\Document\Model\InRulesInterface $InRule)
    {
        $rulesProcessor = $this->getRulesProcessor();
        $js             = $rulesProcessor->getInRuleJS($InRule);

        return $rulesProcessor->executeJs($js);
    }

    /**
     * Применить правило
     *
     * @param string $pk ID правила
     *
     * @return RedirectResponse
     */
    protected function attemptObjectAccept($pk)
    {
        $rulesProcessor = $this->getRulesProcessor();
        $InRules        = $this->getDocumentManager()->find($this->getIntegraParameters()->getDocument('inRules'), $pk);

        $start = microtime(true);
        $ret   = $this->acceptAction($InRules);
        if ($ret['ok']) {
            if (count($ret['retval']) > 0) {
                $type = 'success';
                $msg  = 'Обновлено ' . count($ret['retval']) . ' товар(ов).';
            } else {
                $type = 'error';
                $msg  = 'Товары для данного правила не найдены.';
            }
            $msg .= ' Затрачено ' . $rulesProcessor->normalizeTime(microtime(true) - $start);
        } else {
            $type = 'error';
            $msg  = 'Ошибка: ' . $ret['errmsg'];
        }
        $this->get('session')->getFlashBag()->add($type, $msg);

        // перенаправить пользователя на список
        return new RedirectResponse($this->generateUrl("Nitra_IntegraBundle_InRules_list"));
    }

    /**
     * Применить все правила
     *
     * @Route("/accept-all", name="Nitra_IntegraBundle_InRules_accept_all")
     *
     * @return RedirectResponse
     *
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Exception
     */
    public function acceptAll()
    {
        $rulesProcessor = $this->getRulesProcessor();
        $filters = $this->get('session')->get('Nitra\IntegraBundle\InRulesList\Filters', array());
        $rule    = $this->getIntegraParameters()->getDocument('inRules');
        $qb      = $this->getDocumentManager()->createQueryBuilder($rule);
        $qb->sort('sortOrder', -1);
        if (isset($filters['warehouseId'])) {
            $qb->field('warehouseId')->equals($filters['warehouseId']);
        }
        $InRules = $qb->getQuery()->execute();

        $success        = 0;
        $errors         = 0;
        $time           = 0;
        $prodUpdates    = array();
        foreach ($InRules as $InRule) {
            $start = microtime(true);
            $ret   = $this->acceptAction($InRule);
            $time += microtime(true) - $start;

            if ($ret['ok']) {
                $success ++;
                foreach ($ret['retval'] as $prod) {
                    if (!in_array($prod, $prodUpdates)) {
                        $prodUpdates[] = $prod;
                    }
                }
            } else {
                $errors ++;
            }
        }
        if ($errors > 0) {
            $msg = 'Правила применены с ошибками(' . $errors . '/' . ($success+$errors) . '). Обновлено ' . count($prodUpdates) . ' товаров.';
            $type = 'warning';
        } else {
            $msg = 'Все правила применены. Обновлено ' . count($prodUpdates) . ' товаров.';
            $type = 'success';
        }
        if ($success == 0) {
            $msg = 'Правила не применены.';
            $type = 'error';
        }
        $msg .= ' Затрачено ' . $rulesProcessor->normalizeTime($time);
        $this->get('session')->getFlashBag()->add($type, $msg);

        // перенаправить пользователя на список
        return new RedirectResponse($this->generateUrl("Nitra_IntegraBundle_InRules_list"));
    }
}