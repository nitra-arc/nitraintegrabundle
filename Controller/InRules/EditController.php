<?php

namespace Nitra\IntegraBundle\Controller\InRules;

use Admingenerated\NitraIntegraBundle\BaseInRulesController\EditController as BaseEditController;
use Symfony\Component\HttpFoundation\Response;
use Nitra\IntegraBundle\Form\Type\InRules\EditType;

class EditController extends BaseEditController
{
    /**
     * Получить параметры интегры
     * @return \Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters
     */
    protected function getIntegraParameters()
    {
        return $this->get('nitra_integra.parameters');
    }

    /**
     * получить форму
     */
    protected function getEditType()
    {
        $type = new EditType();
        $type->setIntegraParameters($this->getIntegraParameters());

        return $type;
    }

    /**
     * {@inheritdoc}
     * добавляем доп.параметры для шаблона серез render
     * исключаем PHP Strict Standards:  Declaration ... может принадлежать любому namespace
     */
    public function render($view, array $parameters = array(), Response $response = null)
    {
        // get warehouse entity name from configuration
        $warehouseEntity = $this->getIntegraParameters()->getEntity('warehouse');
        // get supplier entity name from configuration
        $supplierEntity = $this->getIntegraParameters()->getEntity('supplier');
        // get all warehouses ids with supplier prices names
        $warehouses = $this->getIntegraParameters()->get('em')
            ->createQueryBuilder()
            ->from($warehouseEntity, 'w', 'w.id')
            ->select('w.id, s.priceInColumn1, s.priceInColumn2, s.priceInColumn3, s.priceInColumn4')
            ->leftJoin($supplierEntity, 's', 'WITH', 's.id = w.supplier')
            ->andWhere('w.supplier IS NOT NULL')
            ->getQuery()
            ->getArrayResult();

        // add to parameters
        $parameters['pricesBySupplier'] = $warehouses;

        // отобразить родителем
        return parent::render($view, $parameters, $response);
    }
}