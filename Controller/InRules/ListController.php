<?php

namespace Nitra\IntegraBundle\Controller\InRules;

use Admingenerated\NitraIntegraBundle\BaseInRulesController\ListController as BaseListController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Nitra\IntegraBundle\Form\Type\InRules\FiltersType;

class ListController extends BaseListController
{
    /**
     * Получить параметры интегры
     * @return \Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters
     */
    protected function getIntegraParameters()
    {
        return $this->get('nitra_integra.parameters');
    }

    /**
     * Получить форму фильтра
     * @return \Symfony\Component\Form\FormTypeInterface
     */
    protected function getFiltersType()
    {
        $type = new FiltersType();
        $type->setIntegraParameters($this->getIntegraParameters());

        return $type;
    }

    /**
     * Получить массив доп параметров передаваемый в шаблон
     * @return array
     */
    public function getAdditionalRenderParameters()
    {
        // получить склады поставщиков
        $warehouses = $this->getIntegraParameters()->get('em')->createQueryBuilder()
            ->select('w.id, s.name, w.address')
            ->from($this->getIntegraParameters()->getEntity('warehouse'), 'w', 'w.id')
            ->innerJoin('w.supplier', 's')
            ->getQuery()
            ->getArrayResult();

        $filters = $this->getFilters();
        foreach (array('category', 'brand', 'warehouseId',) as $key) {
            if (array_key_exists($key, $filters) && !$filters[$key]) {
                unset($filters[$key]);
            }
        }
        
        // вернуть результируюший массив
        return array(
            'warehouses' => $warehouses,
            'filters' => $filters,
        );
    }

    /**
     * изменение порядка сортировки
     * @Route("/change-rules-sort-order", name="Nitra_IntegraBundle_change_in_rules_sort_order")
     */
    public function changeSortOrderAction()
    {
        // получить приоритетность, данные сортировки
        $priorities = $this->get('request')->get('priorities', array());
        if (!$priorities) {
            // нет данных изменения приоритетности
            return new Response();
        }

        // получить правила
        $rules = $this->getIntegraParameters()->get('dm')
            ->createQueryBuilder($this->getIntegraParameters()->getDocument('inRules'))
            ->field('id')->in(array_keys($priorities))
            ->getQuery()
            ->execute();

        // проверить полученные правила
        if (!$rules->count()) {
            // нет данных изменения приоритетности
            return new Response();
        }

        // обойти все правила установить приоритетность
        foreach ($rules as $rule) {
            // пропустить правило если его нет в массиве сортировки
            if (!isset($priorities[$rule->getId()])) {
                continue;
            }

            // утановить приоритет для правила
            $priority = $priorities[$rule->getId()];
            $rule->setSortOrder((int)$priority);
        }

        // сохранить
        $this->getIntegraParameters()->get('dm')->flush();

        // обновление сортировки завершено успешно
        return new Response();
    }

    /**
     * Получить форму фильтра
     * @return \Symfony\Component\Form\Form
     */
    protected function getFilterForm()
    {
        return $this->createForm($this->getFiltersType(), $this->getFilters(), array(
            'supplier_warehouse' => $this->getSupplierWarehouse(),
        ));
    }

    /**
     * Получить массив складов поставщиков
     * @return array
     */
    protected function getSupplierWarehouse()
    {
        // получить склады поставщиков
        $warehouses = $this->getIntegraParameters()->get('em')
            ->createQueryBuilder()
            ->select('w.id, w.address, s.name')
            ->from($this->getIntegraParameters()->getEntity('warehouse'), 'w')
            ->innerJoin('w.supplier', 's')
            ->getQuery()
            ->getArrayResult();

        // результирующий массив
        $return = array();
        foreach($warehouses as $warehouse) {
            $return[$warehouse['id']] = $warehouse['name'] . ' - ' . $warehouse['address'];
        }

        // вернуть результирующий массив
        return $return;
    }
}