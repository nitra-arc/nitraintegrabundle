<?php

namespace Nitra\IntegraBundle\Controller\InRules;

use Admingenerated\NitraIntegraBundle\BaseInRulesController\NewController as BaseNewController;
use Symfony\Component\HttpFoundation\Response;
use Nitra\IntegraBundle\Form\Type\InRules\NewType;

class NewController extends BaseNewController
{
    /**
     * Получить параметры интегры
     * @return \Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters
     */
    protected function getIntegraParameters()
    {
        return $this->get('nitra_integra.parameters');
    }

    /**
     * получить форму
     */
    protected function getNewType()
    {
        $type = new NewType();
        $type->setIntegraParameters($this->getIntegraParameters());
        return $type;
    }

    /**
     * {@inheritdoc}
     * добавляем доп.параметры для формы серез createForm
     * исключаем PHP Strict Standards:  Declaration ... может принадлежать любому namespace
     */
    public function createForm($type, $data = null, array $options = array())
    {
        // обнулить начальное значение доп.параметров
        $options['warehouseIdFilter'] = null;

        // проверить тип формы
        if ($type instanceof NewType) {
            // получить фильтр листинга
            $filters = $this
                ->get('session')
                ->get('Nitra\IntegraBundle\InRulesList\Filters', array());

            // обновить доп параметр
            $options['warehouseIdFilter'] = isset($filters['warehouseId']) ? $filters['warehouseId'] : null;
        }

        // построить форму родителем
        return parent::createForm($type, $data, $options);
    }

    /**
     * {@inheritdoc}
     * добавляем доп.параметры для шаблона серез render
     * исключаем PHP Strict Standards:  Declaration ... может принадлежать любому namespace
     */
    public function render($view, array $parameters = array(), Response $response = null)
    {
        // get warehouse entity name from configuration
        $warehouseEntity = $this->getIntegraParameters()->getEntity('warehouse');
        // get supplier entity name from configuration
        $supplierEntity = $this->getIntegraParameters()->getEntity('supplier');
        // get all warehouses ids with supplier prices names
        $warehouses = $this->getIntegraParameters()->get('em')
            ->createQueryBuilder()
            ->from($warehouseEntity, 'w', 'w.id')
            ->select('w.id, s.priceInColumn1, s.priceInColumn2, s.priceInColumn3, s.priceInColumn4')
            ->leftJoin($supplierEntity, 's', 'WITH', 's.id = w.supplier')
            ->andWhere('w.supplier IS NOT NULL')
            ->getQuery()
            ->getArrayResult();

        // add to parameters
        $parameters['pricesBySupplier'] = $warehouses;

        // отобразить родителем
        return parent::render($view, $parameters, $response);
    }
}