<?php

namespace Nitra\IntegraBundle\Form\Type;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParametersAwareInterface;

/**
 * Связывание товаров фильтр слева
 */
class IntegraFiltersLeft extends AbstractType implements IntegraParametersAwareInterface
{
    /**
     * @var IntegraParameters $IntegraParameters
     */
    protected $integraParameters;

    /**
     * Установить параметры интегры
     *
     * @param IntegraParameters|null $integraParameters
     */
    public function setIntegraParameters(IntegraParameters $integraParameters)
    {
        $this->integraParameters = $integraParameters;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // категория
        $formOptions = $this->getFormOption('category_id', array(
            'class'              => $this->integraParameters->getDocument('category'),
            'multiple'           => false,
            'required'           => false,
            'property'           => 'indentedName',
            'label'              => ' ',
            'empty_value'        => 'Выберите категорию',
            'translation_domain' => 'NitraIntegraBundle',
        ));
        $builder->add('category_id', 'document', $formOptions);

        // бренд
        $formOptions = $this->getFormOption('brand', array(
            'class'              => $this->integraParameters->getDocument('brand'),
            'multiple'           => false,
            'required'           => false,
            'label'              => ' ',
            'empty_value'        => 'Выберите бренд',
            'empty_data'         => null,
            'translation_domain' => 'NitraIntegraBundle',
            'query_builder'      => function(DocumentRepository $dr) {
                return $dr->createQueryBuilder()->sort('name', 'ASC');
            },
        ));
        $builder->add('brand', 'document', $formOptions);

        // флаг подвязки
        $formOptions = $this->getFormOption('binding', array(
            'required'           => false,
            'label'              => ' ',
            'empty_value'        => 'Все',
            'translation_domain' => 'NitraIntegraBundle',
            'choices'            => array(
                1 => 'Не подвязанные',
                0 => 'Подвязанные',
            ),
        ));
        $builder->add('binding', 'choice', $formOptions);

        // название
        $formOptions = $this->getFormOption('name', array(
            'required' => false,
        ));
        $builder->add('name', 'text', $formOptions);

        // счетчик страниц
        $formOptions = $this->getFormOption('leftPage', array(
            'data' => 0,
        ));
        $builder->add('leftPage', 'hidden', $formOptions);
    }

    /**
     * Получить параметры виджета
     *
     * @param string $name          название виджета
     * @param array  $formOptions   параметры виджета
     *
     * @return array
     */
    protected function getFormOption($name, array $formOptions)
    {
        return $formOptions;
    }

    /**
     * Получить имя формы
     *
     * @return string
     */
    public function getName()
    {
        return 'integra_filters_left';
    }
}