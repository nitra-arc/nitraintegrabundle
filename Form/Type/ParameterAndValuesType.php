<?php

namespace Nitra\IntegraBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints;
use Doctrine\ODM\MongoDB\DocumentManager;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters;
use Nitra\IntegraBundle\Form\DataTransformer\ObjectTransformer;

class ParameterAndValuesType extends AbstractType
{
    /**
     * @var DocumentManager
     */
    protected $dm;

    /**
     * @var IntegraParameters
     */
    protected $integraParameters;

    /**
     * Constructor
     *
     * @param DocumentManager $dm
     * @param IntegraParameters $integraParameters
     */
    public function __construct(DocumentManager $dm, IntegraParameters $integraParameters)
    {
        $this->dm                = $dm;
        $this->integraParameters = $integraParameters;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('parameter', 'genemu_jqueryselect2_hidden', array(
            'label'          => 'Параметр',
            'error_bubbling' => false,
            'attr'           => array(
                'class' => 'selectParameter',
            ),
            'configs' => array(
                'query'         => 'functionParametersAutocomplete',
                'width'         => 'element',
                'initSelection' => 'functionInitParametersSelection',
            ),
            'constraints' => array(
                new Constraints\NotBlank(),
            ),
        ));

        $builder->add('parameterValue', 'genemu_jqueryselect2_hidden', array(
            'label'          => 'Значение параметра',
            'error_bubbling' => false,
            'attr'           => array(
                'class' => 'selectParameterValue',
            ),
            'configs' => array(
                'data' => 'function() { return {results: []}; }',
            ),
            'constraints' => array(
                new Constraints\NotBlank(),
            ),
        ));

        $parameterClass      = $this->integraParameters->getDocument('parameter');
        $parameterValueClass = $this->integraParameters->getDocument('parameterValues');

        $builder->get('parameter')
            ->addModelTransformer(new ObjectTransformer($this->dm, $parameterClass));
        $builder->get('parameterValue')
            ->addModelTransformer(new ObjectTransformer($this->dm, $parameterValueClass));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'parameters_values';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'error_bubbling' => false,
            'data_class'     => $this->integraParameters->getDocument('parameterAndValue'),
        ));
    }
}