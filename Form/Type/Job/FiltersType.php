<?php

namespace Nitra\IntegraBundle\Form\Type\Job;

use Admingenerated\NitraIntegraBundle\Form\BaseJobType\FiltersType as BaseFiltersType;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParametersAwareInterface;

class FiltersType extends BaseFiltersType implements IntegraParametersAwareInterface
{
    /**
     * @var IntegraParameters $integraParameters
     */
    protected $integraParameters;

    /**
     * Установить параметры интегры
     *
     * @param IntegraParameters|null $integraParameters
     */
    public function setIntegraParameters(IntegraParameters $integraParameters)
    {
        $this->integraParameters = $integraParameters;
    }

    /**
     * Получить массив настроек $options для виджета $name
     *
     * @param string $name - имя виджета
     * @param array  $formOptions - массив настроек виджета
     *
     * @return array - массив настроек виджета
     */
    protected function getFormOption($name, array $formOptions)
    {
        // fix select2
        if ($name == 'warehouse') {
            unset($formOptions['configs']);
            $formOptions['class'] = $this->integraParameters->getEntity($name);
        }

        // вернуть массив настроек виджета
        return $formOptions;
    }
}