<?php

namespace Nitra\IntegraBundle\Form\Type\Job;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class JobLoadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // виджет загрузки файлов
        $builder->add('price', 'file', array('required' => false, 'label' => 'Прайс', 'translation_domain' => 'Admin'));
    }

    /**
     * получить имя формы
     *
     * @return string
     */
    public function getName()
    {
        return 'job_load';
    }
}