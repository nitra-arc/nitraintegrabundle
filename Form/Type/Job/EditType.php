<?php

namespace Nitra\IntegraBundle\Form\Type\Job;

use Admingenerated\NitraIntegraBundle\Form\BaseJobType\EditType as BaseEditType;
use Symfony\Component\Form\FormBuilderInterface;
use Nitra\IntegraBundle\Form\EventListener\JobSubscriber;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParametersAwareInterface;

class EditType extends BaseEditType implements IntegraParametersAwareInterface
{
    /**
     * @var IntegraParameters $integraParameters
     */
    protected $integraParameters;

    /**
     * Установить параметры интегры
     * @param IntegraParameters|null $integraParameters
     */
    public function setIntegraParameters(IntegraParameters $integraParameters)
    {
        $this->integraParameters = $integraParameters;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // построить форму родителейм
        parent::buildForm($builder, $options);

        // добавить листенер формы
        $jobSubscriber = new JobSubscriber();
        $jobSubscriber->setIntegraParameters($this->integraParameters);
        $builder->addEventSubscriber($jobSubscriber);
    }

    /**
     * Получить имя формы
     *
     * @return string
     */
    public function getName()
    {
        return 'job';
    }
}