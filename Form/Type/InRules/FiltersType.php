<?php

namespace Nitra\IntegraBundle\Form\Type\InRules;

use Admingenerated\NitraIntegraBundle\Form\BaseInRulesType\FiltersType as BaseFiltersType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParametersAwareInterface;

class FiltersType extends BaseFiltersType implements IntegraParametersAwareInterface
{
    /**
     * @var IntegraParameters $integraParameters
     */
    protected $integraParameters;

    /**
     * Установить параметры интегры
     *
     * @param IntegraParameters|null $integraParameters
     */
    public function setIntegraParameters(IntegraParameters $integraParameters)
    {
        $this->integraParameters = $integraParameters;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // построить форму родителем
        parent::buildForm($builder, $options);

        // склад поставщика
        $builder->add('warehouseId', 'choice', array(
            'choices'            => $options['supplier_warehouse'],
            'empty_value'        => '',
            'multiple'           => false,
            'required'           => false,
            'label'              => 'Поставщик',
            'translation_domain' => 'Admin',
        ));
    }

    /**
     * настройки формы по умолчанию
     *
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $options_default = array(
            'supplier_warehouse' => array(),
        );
        $resolver->setOptional(array_keys($options_default));
        $resolver->setDefaults($options_default);
    }

    /**
     * получить праметры виджета
     *
     * @param string $name
     * @param array  $formOptions
     *
     * @return array
     */
    protected function getFormOption($name, array $formOptions)
    {
        // обновить класс для виджетов категории и бренда
        if (in_array($name, array('category', 'brand'))) {
            $formOptions['class'] = $this->integraParameters->getDocument($name);
        }

        // вернуть параметры виджета
        return $formOptions;
    }
}