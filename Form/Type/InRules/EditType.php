<?php

namespace Nitra\IntegraBundle\Form\Type\InRules;

use Admingenerated\NitraIntegraBundle\Form\BaseInRulesType\EditType as BaseEditType;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Form\FormBuilderInterface;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParametersAwareInterface;

class EditType extends BaseEditType implements IntegraParametersAwareInterface
{
    /**
     * @var IntegraParameters $IntegraParameters
     */
    protected $integraParameters;

    /**
     * Установить параметры интегры
     * @param IntegraParameters|null $integraParameters
     */
    public function setIntegraParameters(IntegraParameters $integraParameters)
    {
        $this->integraParameters = $integraParameters;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // построить форму родителем
        parent::buildForm($builder, $options);

        // склады поставщиков
        $wareChoices = array();
        $wrehousesSup = $this->integraParameters->get('em')
            ->createQueryBuilder()
            ->from($this->integraParameters->getEntity('warehouse'), 'w')
            ->select('w.id, s.name, w.address')
            ->leftJoin($this->integraParameters->getEntity('supplier'), 's', 'WITH', 's.id=w.supplier')
            ->andwhere('w.supplier IS NOT NULL')
            ->getQuery()
            ->getArrayResult();

        foreach ($wrehousesSup as $warehouse) {
            $wareChoices[$warehouse['id']] = $warehouse['name'] . ' ' . $warehouse['address'];
        }

        // виджет складов
        $formOption = $this->getFormOption('warehouseId', array('choices' => $wareChoices,'label'=>'Поставщик'));
        $builder->add("warehouseId", 'choice', $formOption);
    }

    /**
     * получить праметры виджета
     * @return array
     */
    protected function getFormOption($name, array $formOptions)
    {
        // виджет должен быть заполнен
        if (in_array($name, array('warehouseId', 'formula'))) {
            $formOptions['required'] = true;
            $formOptions['constraints'] = array(new Constraints\NotBlank());
        }

        // обновить класс для виджетов категории и бренда
        if (in_array($name, array('category', 'brand'))) {
            $formOptions['class'] = $this->integraParameters->getDocument($name);
        }

        // вернуть параметры виджета
        return $formOptions;
    }
}