<?php

namespace Nitra\IntegraBundle\Form\Type\InRules;

use Admingenerated\NitraIntegraBundle\Form\BaseInRulesType\NewType as BaseNewType;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParametersAwareInterface;

class NewType extends BaseNewType implements IntegraParametersAwareInterface
{
    /**
     * @var IntegraParameters $integraParameters
     */
    protected $integraParameters;

    /**
     * Установить параметры интегры
     *
     * @param IntegraParameters|null $integraParameters
     */
    public function setIntegraParameters(IntegraParameters $integraParameters)
    {
        $this->integraParameters = $integraParameters;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // построить форму родителем
        parent::buildForm($builder, $options);

        // склады поставщиков
        $wareChoices = array();
        $wrehousesSup = $this->integraParameters->get('em')
            ->createQueryBuilder()
            ->from($this->integraParameters->getEntity('warehouse'), 'w')
            ->select('w.id, s.name, w.address')
            ->leftJoin($this->integraParameters->getEntity('supplier'), 's', 'WITH', 's.id=w.supplier')
            ->andwhere('w.supplier IS NOT NULL')
            ->getQuery()
            ->getArrayResult();
        foreach ($wrehousesSup as $warehouse) {
            $wareChoices[$warehouse['id']] = $warehouse['name'] . ' ' . $warehouse['address'];
        }

        // виджет складов
        $formOption = $this->getFormOption('warehouseId', array(
            'choices'   => $wareChoices,
            'label'     => 'Поставщик',
            'data'      => $options['warehouseIdFilter'],
        ));
        $builder->add("warehouseId", 'choice', $formOption);
    }

    /**
     * настройки формы по умолчанию
     *
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'warehouseIdFilter' => null,
        ));
    }

    /**
     * получить праметры виджета
     *
     * @param string $name
     * @param array  $formOptions
     *
     * @return array
     */
    protected function getFormOption($name, array $formOptions)
    {
        // виджет должен быть заполнен
        if (in_array($name, array('warehouseId', 'formula'))) {
            $formOptions['required'] = true;
            $formOptions['constraints'] = array(new Constraints\NotBlank());
        }

        // обновить класс для виджетов категории и бренда
        if (in_array($name, array('category', 'brand'))) {
            $formOptions['class'] = $this->integraParameters->getDocument($name);
        }

        // вернуть параметры виджета
        return $formOptions;
    }
}