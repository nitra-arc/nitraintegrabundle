<?php

namespace Nitra\IntegraBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParametersAwareInterface;

/**
 * Связывание товаров фильтр слева
 */
class IntegraFiltersRight extends AbstractType implements IntegraParametersAwareInterface
{
    /**
     * @var IntegraParameters $IntegraParameters
     */
    protected $integraParameters;

    /**
     * Установить параметры интегры
     * @param IntegraParameters|null $integraParameters
     */
    public function setIntegraParameters(IntegraParameters $integraParameters)
    {
        $this->integraParameters = $integraParameters;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // поставщик
        $formOptions = $this->getFormOption('supplier', array(
            'class'              => $this->integraParameters->getEntity('supplier'),
            'multiple'           => false,
            'required'           => false,
            'label'              => ' ',
            'empty_value'        => 'Выберите поставщика',
            'empty_data'         => null,
            'translation_domain' => 'NitraIntegraBundle',
        ));
        $builder->add('supplier', 'entity', $formOptions);

        // название
        $formOptions = $this->getFormOption('name', array(
            'required' => false,
        ));
        $builder->add('name', 'text', $formOptions);

        // флаг все товары
        $formOptions = $this->getFormOption('all', array(
            'required'           => false,
            'label'              => 'Все товары',
            'translation_domain' => 'NitraIntegraBundle',
        ));
        $builder->add('all', 'checkbox', $formOptions);

        // счетчик страниц
        $formOptions = $this->getFormOption('rightPage', array(
            'data' => 0,
        ));
        $builder->add('rightPage', 'hidden', $formOptions);
    }

    /**
     * Получить параметры виджета
     *
     * @param string $name        название виджета
     * @param array  $formOptions параметры виджета
     *
     * @return array
     */
    protected function getFormOption($name, array $formOptions)
    {
        return $formOptions;
    }

    /**
     * Получить имя формы
     *
     * @return string
     */
    public function getName()
    {
        return 'integra_filters_right';
    }
}