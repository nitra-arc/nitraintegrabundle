<?php

namespace Nitra\IntegraBundle\Form\Type\Supplier;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Фильтр аналитика по поставщику
 */
class AnalyticsFiltersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $formOptions = $this->getFormOption('name', array('required' => false, 'label' => 'Название поставщика', 'translation_domain' => 'Admin',));
        $builder->add('name', 'text', $formOptions);
    }

    /**
     * Получить параметры виджета
     *
     * @param string $name        название виджета
     * @param array  $formOptions параметры виджета
     *
     * @return array
     */
    protected function getFormOption($name, array $formOptions)
    {
        return $formOptions;
    }

    /**
     * Получить имя формы
     *
     * @return string
     */
    public function getName()
    {
        return 'filters_supplier_analytics';
    }
}