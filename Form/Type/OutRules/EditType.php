<?php

namespace Nitra\IntegraBundle\Form\Type\OutRules;

use Admingenerated\NitraIntegraBundle\Form\BaseOutRulesType\EditType as BaseEditType;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Form\FormBuilderInterface;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParametersAwareInterface;

class EditType extends BaseEditType implements IntegraParametersAwareInterface
{
    /**
     * @var IntegraParameters $IntegraParameters
     */
    protected $integraParameters;

    /**
     * Установить параметры интегры
     * @param IntegraParameters|null $integraParameters
     */
    public function setIntegraParameters(IntegraParameters $integraParameters)
    {
        $this->integraParameters = $integraParameters;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // построить форму родителем
        parent::buildForm($builder, $options);

        // поставщики
        $supplierChoices = array();
        $suppliers = $this->integraParameters->get('em')->createQueryBuilder()
            ->select('s.id, s.name')
            ->from($this->integraParameters->getEntity('supplier'), 's')
            ->getQuery()
            ->getArrayResult();
        foreach ($suppliers as $supplier) {
            $supplierChoices[$supplier['id']] = $supplier['name'];
        }

        // виджет склад поставщика
        $formOption = $this->getFormOption('supplierId', array(
            'choices' => $supplierChoices,
            'label' => 'Поставщик',
        ));
        $builder->add('supplierId', 'choice', $formOption);
    }

    /**
     * получить настройки виджета
     *
     * @param string $name        название виджета
     * @param array  $formOptions настройки виджета
     *
     * @return array
     */
    protected function getFormOption($name, array $formOptions)
    {
        // виджет должен быть заполнен
        if (in_array($name, array('supplierId', 'store', 'formula'))) {
            $formOptions['required'] = true;
            $formOptions['constraints'] = array(new Constraints\NotBlank());
        }

        // обновить класс для виджетов категории и бренда и магазина
        if (in_array($name, array('category', 'brand', 'store'))) {
            $formOptions['class'] = $this->integraParameters->getDocument($name);
        }

        // вернуть массив настроек виджета
        return $formOptions;
    }
}