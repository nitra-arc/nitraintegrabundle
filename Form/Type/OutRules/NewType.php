<?php

namespace Nitra\IntegraBundle\Form\Type\OutRules;

use Admingenerated\NitraIntegraBundle\Form\BaseOutRulesType\NewType as BaseNewType;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParametersAwareInterface;

class NewType extends BaseNewType implements IntegraParametersAwareInterface
{
    /**
     * @var IntegraParameters $integraParameters
     */
    protected $integraParameters;

    /**
     * Установить параметры интегры
     *
     * @param IntegraParameters|null $integraParameters
     */
    public function setIntegraParameters(IntegraParameters $integraParameters)
    {
        $this->integraParameters = $integraParameters;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // построить форму родителем
        parent::buildForm($builder, $options);

        // поставщики
        $supplierChoices = array();
        $suppliers = $this->integraParameters->get('em')->createQueryBuilder()
            ->select('s.id, s.name')
            ->from($this->integraParameters->getEntity('supplier'), 's')
            ->getQuery()
            ->getArrayResult();
        foreach ($suppliers as $supplier) {
            $supplierChoices[$supplier['id']] = $supplier['name'];
        }

        // виджет склад поставщика
        $formOption = $this->getFormOption('supplierId', array(
            'choices' => $supplierChoices,
            'label' => 'Поставщик',
        ));
        $builder->add('supplierId', 'choice', $formOption);

        // виджет магазин
        $formOptions = $this->getFormOption('store', array(
            'class'     => $this->integraParameters->getDocument('store'),
            'multiple'  => false,
            'data'      => $options['storeFilter'],
            'label'     => 'Магазин',  'translation_domain' => 'Admin',));
        $builder->add('store', 'document', $formOptions);
    }

    /**
     * получить настройки виджета
     *
     * @param string $name        название виджета
     * @param array  $formOptions настройки виджета
     *
     * @return array
     */
    protected function getFormOption($name, array $formOptions)
    {
        // виджет должен быть заполнен
        if (in_array($name, array('supplierId', 'store', 'formula'))) {
            $formOptions['required'] = true;
            $formOptions['constraints'] = array(new Constraints\NotBlank());
        }

        // обновить класс для виджетов категории и бренда и магазина
        if (in_array($name, array('category', 'brand', 'store'))) {
            $formOptions['class'] = $this->integraParameters->getDocument($name);
        }

        // вернуть массив настроек виджета
        return $formOptions;
    }

    /**
     * установить параметры $options формы
     *
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        // параметры по умолчнию
        $resolver->setDefaults(array(
            'storeFilter' => null,
        ));
    }
}