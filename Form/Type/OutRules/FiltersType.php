<?php

namespace Nitra\IntegraBundle\Form\Type\OutRules;

use Admingenerated\NitraIntegraBundle\Form\BaseOutRulesType\FiltersType as BaseFiltersType;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParametersAwareInterface;

class FiltersType extends BaseFiltersType implements IntegraParametersAwareInterface
{
    /**
     * @var IntegraParameters $integraParameters
     */
    protected $integraParameters;

    /**
     * Установить параметры интегры
     *
     * @param IntegraParameters|null $integraParameters
     */
    public function setIntegraParameters(IntegraParameters $integraParameters)
    {
        $this->integraParameters = $integraParameters;
    }

    /**
     * получить настройки виджета
     *
     * @param string $name        название виджета
     * @param array  $formOptions настройки виджета
     *
     * @return array
     */
    protected function getFormOption($name, array $formOptions)
    {
        if ($name == 'store') {
            $formOptions['required'] = true;
        }

        // обновить класс для виджетов категории и бренда и магазина
        if (in_array($name, array('category', 'brand', 'store'))) {
            $formOptions['class'] = $this->integraParameters->getDocument($name);
        }

        if ($name == 'supplierId') {
            $formOptions['class'] = $this->integraParameters->getEntity('supplier');
        }
        
        // вернуть массив настроек виджета
        return $formOptions;
    }
}