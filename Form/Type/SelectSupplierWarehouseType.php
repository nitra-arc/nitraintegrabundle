<?php

namespace Nitra\IntegraBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Doctrine\ORM\EntityRepository;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters;

/**
 * Виджет для выбора складов поставщика
 */
class SelectSupplierWarehouseType extends AbstractType
{
    /**
     * @var IntegraParameters параметры интегры
     */
    protected $integraParameters;

    /**
     * @var array $suppliers массив поставщиков
     */
    protected $suppliers;

    /**
     * @var array $warehouses массив складов
     */
    protected $warehouses;

    /**
     * constructor
     * @param Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters $integraParameters параметры интегры
     */
    public function __construct(IntegraParameters $integraParameters)
    {
        // установить параметры интегры
        $this->integraParameters = $integraParameters;
    }

    /**
     * buildView
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        // анонимная функция из формы, возвращающая query_builder
        $queryBuilderClosure = $options['query_builder'];
        $warehouseRepository = $this->integraParameters->get('em')->getRepository($this->integraParameters->getEntity('warehouse'));

        // queryBuilder
        $queryBuilder = $queryBuilderClosure($warehouseRepository);
        $r = $queryBuilder->getRootAlias();

        // получить склады
        $warehouses = $queryBuilder
            ->addSelect('s')
            ->innerJoin($r.'.supplier', 's')
            ->getQuery()
            ->getArrayResult();

        // обнуление массива поставщиков и складов
        // чтобы не задваивались склады
        // если виджет используется на странице больше одного раза
        $this->suppliers = array();
        $this->warehouses = array();

        // массив сортировки по названию поставщика
        $sortBySupplierName = array();
        // массив сортировки по названию склада
        $sortByWarehouseName = array();

        // обойти все склады сформировать массивы
        foreach ($warehouses as $warehouse) {
            // поставщик
            $supplier = $warehouse['supplier'];

            // обновить массив сортировки названий складов
            $sortByWarehouseName[] = $warehouse['address'];

            // запомнить склад
            $this->warehouses[] = array(
                'id'         => $warehouse['id'],
                'supplierId' => $supplier['id'],
                'priority'   => $warehouse['priority'],
                'address'    => $warehouse['address'],
            );

            // проверить добавлен ли поставщик в массив поставщиков
            // составить массив поставщиков
            if (!isset($this->suppliers[$supplier['id']])) {
                // обновить массив сортировки названий поставщиков
                $sortBySupplierName[] = $supplier['name'];

                // обновить массив поставщиков
                $this->suppliers[$supplier['id']] = array(
                    'id'   => $supplier['id'],
                    'name' => $supplier['name'],
                );
            }

        }

        // выполнить сортировку городов
        array_multisort(
            $sortBySupplierName, SORT_STRING, SORT_ASC,
            $this->suppliers
        );

        // выполнить сортировку складов
        array_multisort(
            $sortByWarehouseName, SORT_STRING, SORT_ASC,
            $this->warehouses
        );

        // запомнить данные виджета
        $view->vars['suppliers']  = $this->suppliers;
        $view->vars['warehouses'] = $this->warehouses;
        $view->vars['name']       = $form->getName();

        // сформировать ID виджетов для поставщика и склада
        $view->vars['widget_id_supplier']  = $form->getParent()->getName() . '_select_supplier_for_' . $form->getName();
        $view->vars['widget_id_warehouse'] = $form->getParent()->getName() . '_' . $form->getName();

        // вызов родителя Builds the form view
        parent::buildView($view, $form, $options);
    }

    /**
     * значения по умолчанию
     *
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'virtual'       => false,
            'required'      => true,
            'empty_value'   => ' ',
            'class'         => $this->integraParameters->getEntity('warehouse'),
            'property'      => 'address',
            'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('q')
                    ->addOrderBy('q.address')
                    ->andwhere('q.supplier IS NOT NULL');
            },
        ));
    }

    /**
     * Получить родителя
     *
     * @return string
     */
    public function getParent()
    {
        return 'entity';
    }

    /**
     * Получить имя формы
     *
     * @return string
     */
    public function getName()
    {
        return 'nitra_integra_select_supplier_warehouse';
    }
}