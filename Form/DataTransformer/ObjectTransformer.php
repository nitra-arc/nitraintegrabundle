<?php

namespace Nitra\IntegraBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Doctrine\ODM\MongoDB\DocumentManager;

class ObjectTransformer implements DataTransformerInterface
{
    /** @var \Doctrine\ODM\MongoDB\DocumentManager */
    protected $dm;
    /** @var string */
    protected $class;
    /** @var boolean */
    protected $json;

    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     * @param string                                $class
     * @param boolean                               $json
     */
    public function __construct(DocumentManager $dm, $class, $json = true)
    {
        $this->dm    = $dm;
        $this->class = $class;
        $this->json  = $json;
    }

    /**
     * to object
     *
     * @param string $value
     *
     * @return object
     */
    public function reverseTransform($value)
    {
        $id = $this->json
            ? json_decode($value, true)['id']
            : $value;

        return $this->dm->find($this->class, $id);
    }

    /**
     * to string
     *
     * @param object $object
     *
     * @return string
     */
    public function transform($object)
    {
        if (!$object) {
            return;
        }

        return !$this->json
            ? $object->getId()
            : json_encode(array(
                'id'    => $object->getId(),
                'text'  => $object->getName(),
            ));
    }
}