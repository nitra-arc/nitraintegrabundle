<?php

namespace Nitra\IntegraBundle\Form\EventListener;

use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters;
use Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParametersAwareInterface;

/**
 * Обработчик событий для формы прайса
 */
class JobSubscriber implements EventSubscriberInterface, IntegraParametersAwareInterface
{
    /**
     * @var \Nitra\IntegraBundle\Lib\IntegraParameters\IntegraParameters $integraParameters
     * параметры интегры
     */
    protected $integraParameters;

    /**
     * @var \Nitra\IntegraBundle\Lib\JobPreview\JobPreview $jobPreview
     * превью прайсов
     */
    protected $jobPreview;

    /**
     * Установить параметры интегры
     * @param IntegraParameters|null $integraParameters
     */
    public function setIntegraParameters(IntegraParameters $integraParameters)
    {
        // установить зависимости
        $this->integraParameters = $integraParameters;
        $this->jobPreview = $integraParameters->get('container')->get('nitra_integra.job.preview');
    }

    /**
     * получить массив событий
     */
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SUBMIT  => 'preSubmit',
            FormEvents::SUBMIT      => 'submit',
            FormEvents::POST_SUBMIT => 'postSubmit',
        );
    }

    /**
     * preSubmit
     *
     * @param FormEvent $event
     */
    public function preSubmit(FormEvent $event)
    {
        // получить данные введенные пользователем
        $formData = $event->getData();

        // получить массив слов [t1, t2, t3] получить массив из [t1,t2 t3]
        $words = preg_split('/[\s,;]/', $formData['columnName'], null, PREG_SPLIT_NO_EMPTY);

        // преобразовать строку название
        // преобразовать к виду [t1, t2, t3]
        $formData['columnName'] = implode(', ', array_unique($words));

        // сохранить данные формы
        $event->setData($formData);
    }

    /**
     * submit - устанавливаем данные формы по умолчанию
     *
     * @param FormEvent $event
     */
    public function submit(FormEvent $event)
    {
        // получить форму
        $form = $event->getForm();

        // получить массив буквенных столюцов в файле excell
        $excellColumnLetters = $this->jobPreview->getExcellColumnLetters();

        // максимальный столбец
        $maxExcellColumn = count($excellColumnLetters);

        // получить массив слов t1,t2 t3 получить массив из [t1, t2, t3]
        $words = preg_split('/[\s,]/', $form->get('columnName')->getData(), null, PREG_SPLIT_NO_EMPTY);
        foreach ($words as $word) {
            // слово должно быть числом
            if (!is_numeric($word)) {
                // номер столбца не является числом
                $form->get('columnName')
                    ->addError(new FormError('"'.$word.'" не является числом.'));
            // номер ячейки не может нулем
            } elseif ($word == 0) {
                $form->get('columnName')
                    ->addError(new FormError('"'.$word.'" не может принимать нулевое значение.'));
            // номер ячейки не может быть отрицательным
            } elseif ($word < 0) {
                $form->get('columnName')
                    ->addError(new FormError('"'.$word.'" не может быть отрицательным.'));
            // проверить максимальн-доступный алфавит
            } elseif ($word > $maxExcellColumn) {
                $form->get('columnName')
                    ->addError(new FormError('"'.$word.'" не может быть больше '.$maxExcellColumn.'.'));
            }
        }

        // получить сущность формы
        $job = $event->getData();

        // получить склад
        $warehouse = $form->get('warehouse')->getData();
        // если выбран склад
        if ($warehouse) {
            // установить поставщика
            $job->setSupplier($warehouse->getSupplier());
        }
    }

    /**
     * postSubmit - проверяем превтю файла прайса
     *
     * @param FormEvent $event
     */
    public function postSubmit(FormEvent $event)
    {
        // получить сущность формы
        $job = $event->getData();

        // получить форму
        $form = $event->getForm();

        // если используется стандартный загрузчик
        if ($job->isStandart()) {
            // получить превью файла прайса
            $jobPreview = $this->jobPreview->preview($job);
            // отображение ошибок
            if ($jobPreview && isset($jobPreview['errors']) && $jobPreview['errors']) {
                // создать отображение ошибок
                foreach ($jobPreview['errors'] as $error) {
                    $form->addError(new FormError($error['message']));
                }
            }
        }
    }
}