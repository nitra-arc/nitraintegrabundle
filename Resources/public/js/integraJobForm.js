/**
 * работа с формарами integra-Job
 **/

// удалить заголовок для параметров стандартного загрузчика
$('.form_fieldset_IsStandart legend').html('Настройки загрузчика');
// спрятать все параметры стандартного загрузчика (по умолчанию не отображаются)
$('.form_fieldset_IsStandart').hide();
$('#job_preview').hide();

// страница загружена     
$(document).ready(function() {
    
    // Показать настройки при первой загрузке страницы
    if ($('#job_isStandart').is(':checked')) {
        $('.form_fieldset_IsStandart').show();
        $('#job_preview').show();
        $('#job_name').attr('readonly', 'readonly');
    }
    
    // введенное (начальное) значение имя job
    var job_name = $('#job_name').val();
    // при вводе названия job обновляем начальное значение имя job
    $('#job_name').on('keyup', function(e){
        // если не стандартный загрузчик
        if (!$('#job_isStandart').is(':checked')) {
            job_name = $(this).val();
        }
    });
    
    // спрятатьт показать настройки стандартного загрузчика
    $('#job_isStandart').click(function(){
        if ($(this).is(':checked')) {
            // стандартный загрузчик
            $('.form_fieldset_IsStandart').show();
            $('#job_preview').show();
            // установить название стандартного загрузчика
            $('#job_name').val('standart').attr('readonly', 'readonly');
            $('#job_name').attr('readonly', 'readonly');
            
        } else {
            // не стандартный загрузчик
            $('.form_fieldset_IsStandart').hide();
            $('#job_preview').hide();
            // job name возвращаем старый если не пустое значение
            if (job_name) {
                $('#job_name').val(job_name);
            }
            $('#job_name').removeAttr('readonly');
        }
    });
    
});

