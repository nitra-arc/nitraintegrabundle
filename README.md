# NitraIntegraBundle

Aвтоматическое обновления цен, прайсов и статуса товаров на сайте.

*****

## [Подключение](#markdown-header-_2)
* [composer.json](#markdown-header-composerjson)
* [app/AppKernel.php](#markdown-header-appappkernelphp)
* [app/config/config.yml](#markdown-header-appconfigconfigyml)
* [app/config/routing.yml](#markdown-header-appconfigroutingyml)
* [app/config/menu.yml](#markdown-header-appconfigmenuyml)
* [обновить базу данных](#markdown-header-_3)
* [обновить assets](#markdown-header-assets)

## [Конфигурация](#markdown-header-_4)
* [по умолчанию](#markdown-header-_5)

*****

### Подключение
Для избежания конфликтов версий бандла doctrine/mongodb-odm выполнять подключение интегры в указанной ниже последовательности

#### composer.json
подключаем бандл **doctrine/mongodb-odm** нужной версии
```json
{
    ...
    "require": {
        ...
        "doctrine/mongodb-odm": "dev-master#a388984ef9df78c95744a0643dcbb9714b871c18"
        ...
    }
    ...
}
```
#### console
обновляем vendors
```console
composer update "doctrine/mongodb-odm"
```

#### composer.json
Подключаем Интегру **nitra/integra-bundle**
```json
{
    ...
    "require": {
        ...
        "nitra/integra-bundle": "dev-master"
        ...
    }
    ...
}
```
#### console
обновляем vendors
```console
composer update "nitra/integra-bundle"
```

#### app/AppKernel.php
```php
<?php
// ...
class AppKernel extends Kernel
{
    // ...
    public function registerBundles()
    {
        // ...
        $bundles = array(
            // ...
            // NitraIntegraBundle
            new Nitra\IntegraBundle\NitraIntegraBundle(),
            new Nitra\ExtensionsAdminBundle\NitraExtensionsAdminBundle(),
            // ...
        );
        // ...
        return $bundles;
    }
    // ...
}
// ...
```

#### app/config/config.yml
```yml
...
admingenerator_generator:
    use_doctrine_orm: true
    use_doctrine_odm: true
...
```

#### app/config/routing.yml
```yml
...
NitraIntegraBundle:
    resource: "@NitraIntegraBundle/Resources/config/routing.yml"
    prefix:   /
...
```


#### app/config/menu.yml
```yml
millwright_menu:
    # ...
    items:
        # ...
        integra:
            label: "Интегра"
        integra_product:
            label: 'Товары'
            route: Nitra_IntegraBundle_Bind_list
        integra_job:
            label: 'Прайсы'
            route: Nitra_IntegraBundle_Job_list
        integra_in_rules:
            label: 'Правила входящих цен'
            route: Nitra_IntegraBundle_InRules_list
        integra_out_rules:
            label: 'Правила выходящих цен'
            route: Nitra_IntegraBundle_OutRules_list
        integra_supplier:
            label: 'Поставщики'
            route: Nitra_IntegraBundle_Supplier_list
        integra_warehouse:
            label: 'Склады поставщиков'
            route: Nitra_IntegraBundle_Warehouse_list
        integra_supplier_analytics:
            label: 'Аналитика поставщиков'
            route: Nitra_IntegraBundle_SupplierAnalytics_list
        # ...
    # ...
    tree: 
        main: 
            type: navigation 
            children:
                # ...
                integra:
                    type: navigation
                    children:
                        integra_product:    ~
                        integra_job:        ~
                        integra_in_rules:   ~
                        integra_out_rules:  ~
                        integra_supplier:   ~
                        integra_warehouse:  ~
                        integra_supplier_analytics:   ~
                # ...
    # ...

# ...
```

#### Обновить базу данных
console
```console
php app/console doctrine:schema:update --dump-sql

```

#### Обновить assets
console
```console
php app/console assets:install
php app/console assetic:dump

```


*****
### Конфигурация

#### по умолчанию

app/config/config.yml
```yml
nitra_integra:
    # настройка правил интегры
    rules:
        active_products:      true
        unactive_products:    true
        put_out_of_stock:     false
        deactivate_out_of_stock:  false
        activate_locked_products:  false

    # документы с которыми работает интегра
    document:
        inRules:    Nitra\IntegraBundle\Document\InRules
        outRules:   Nitra\IntegraBundle\Document\OutRules
        stock:      Nitra\MainBundle\Document\Stock
        store:      Nitra\MainBundle\Document\Store
        brand:      Nitra\MainBundle\Document\Brand
        category:   Nitra\MainBundle\Document\Category
        product:    Nitra\MainBundle\Document\Product
        
    # сущности с которыми работает интегра
    entity:
        job:        Nitra\TetradkaIntegraBundle\Entity\Job
        delivery:   Nitra\MainBundle\Entity\Delivery
        warehouse:  Nitra\MainBundle\Entity\Warehouse
        supplier:   Nitra\MainBundle\Entity\Supplier
        city:       Nitra\TetradkaGeoBundle\Entity\City
        region:     Nitra\TetradkaGeoBundle\Entity\Region
        currency:   Nitra\MainBundle\Entity\Currency

    # Настройка прайсообработчика
    jobs:
        # путь к директории в которую складываются прайсы поставщиков
        excel_dir:            /home/user/temp/integra/excel/
        # путь к директории в которой лежат обработчики прайстов поставщиков
        handler_dir:          /home/user/temp/integra/handler/
        # массив обработчиков прайсов [ улюч => значение ]
        handlers:
            standart:        standart_0.1/standart/standart

```

*****
