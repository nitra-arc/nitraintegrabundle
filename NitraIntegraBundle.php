<?php

namespace Nitra\IntegraBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Nitra\IntegraBundle\DependencyInjection\Compiler\TwigFormResourcesCompilerPass;

class NitraIntegraBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        // построить бандл родителем
        parent::build($container);

        // добавляем ресурсы форм для интегры
        $container->addCompilerPass(new TwigFormResourcesCompilerPass());
    }
}