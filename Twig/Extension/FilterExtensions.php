<?php

namespace Nitra\IntegraBundle\Twig\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;

class FilterExtensions extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'filter.extension';
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return array(
            'json_decode' => new \Twig_Filter_Method($this, 'jsonDecode'),
        );
    }

    /**
     * Decode json string
     *
     * @param string $str
     * @param mixed  $key
     *
     * @return mixed
     */
    public function jsonDecode($str, $key = false)
    {
        return $key !== false
            ? json_decode($str, true)[$key]
            : json_decode($str);
    }
}