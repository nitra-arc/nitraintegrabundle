<?php

namespace Nitra\IntegraBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

/**
 * добавляем ресурсы форм для интегры
 */
class TwigFormResourcesCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        // получить ресурсы twig
        $resources = $container->getParameter('twig.form.resources');

        // если ресуср интегры не добавлен
        if (!in_array('NitraIntegraBundle:Form:fields.html.twig', $resources)) {
            // добавить ресурс
            $resources[] = 'NitraIntegraBundle:Form:fields.html.twig';
            $container->setParameter('twig.form.resources', $resources);
        }
    }
}