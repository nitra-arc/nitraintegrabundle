<?php

namespace Nitra\IntegraBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Doctrine\Common\Inflector\Inflector;

class NitraIntegraExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        // получить настройки
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        // загрузить сервисы
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        // установить массив моделнй для сервис-виджета
        $integraRregistry = $container->getDefinition('nitra_integra.parameters');

        $integraRregistry->addMethodCall('set', array('rules',          $config['rules']));
        $integraRregistry->addMethodCall('set', array('document',       $config['document']));
        $integraRregistry->addMethodCall('set', array('entity',         $config['entity']));
        $integraRregistry->addMethodCall('set', array('jobs',           $config['jobs']));
        $integraRregistry->addMethodCall('set', array('currencyCode',   $this->getCurrencyCode($container)));
    }

    /**
     * Получить код валюты
     * @param ContainerBuilder $container
     * @return string код валюты
     * @throws \Exception нет кода валюты
     */
    protected function getCurrencyCode(ContainerBuilder $container)
    {
        // проверить код валюты для полной тетрадки
        if ($container->hasParameter('currency_code')) {
            return $container->getParameter('currency_code');
        }

        // проверить код валюты для мини тетрадки
        if ($container->hasParameter('nitra_minitetradka_currency_code')) {
            return $container->getParameter('nitra_minitetradka_currency_code');
        }

        // ошибка нет кода валюты
        throw new \Exception('Не установлен код валюты.');
    }

    /**
     * @inheritdoc
     */
    public function prepend(ContainerBuilder $container)
    {
        // Изменить настройки doctrine ORM
        $this->prependORM($container);

        // Изменить настройки doctrine ODM
        $this->prependODM($container);
    }

    /**
     * Изменить настройки doctrine ORM
     */
    protected function prependORM(ContainerBuilder $container)
    {
        // если не установлена doctrine
        if (!$container->hasExtension('doctrine')) {
            // прервать выполнение
            return;
        }

        // получить конфигурацию бандла
        $extensionConfig = $container->getExtensionConfig('nitra_integra');

        // провеверить если в config.yml НЕ добавлены настройки
        // прерываем выполнение
        if (!isset($extensionConfig[0]['entity'])) {
            return;
        }

        // массив интерфейсов сущностей
        $resolveTargetEntities = array(
            'orm' => array(
                'resolve_target_entities' => array(),
            ),
        );

        // обойти все сушности банлда
        foreach($extensionConfig[0]['entity'] as $modelName => $modelClass) {
            // интерфейс сущности
            $interfaceName = 'Nitra\\IntegraBundle\\Entity\Model\\' . Inflector::classify($modelName) . 'Interface';
            $resolveTargetEntities['orm']['resolve_target_entities'][$interfaceName] = $modelClass;

            // создать параметр сущности
            $parameterName = 'nitra_integra.entity.'.$modelName;
            $container->setParameter($parameterName, $modelClass);
        }

        // создать параметры интерфейсов реализующих сушности
        $container->setParameter('nitra_integra.resolve_target_entities', $resolveTargetEntities['orm']['resolve_target_entities']);

        // добавить массив интерефейсов в настройки doctrine
        $container->prependExtensionConfig('doctrine', $resolveTargetEntities);
    }

    /**
     * Изменить настройки doctrine ODM
     */
    protected function prependODM(ContainerBuilder $container)
    {
        // если не установлена doctrine
        if (!$container->hasExtension('doctrine_mongodb')) {
            // прервать выполнение
            return;
        }

        // получить конфигурацию бандла
        $extensionConfig = $container->getExtensionConfig('nitra_integra');

        // провеверить если в config.yml НЕ добавлены настройки
        // прерываем выполнение
        if (!isset($extensionConfig[0]['document'])) {
            return;
        }

        // массив интерфейсов документов
        $resolveTargetDocuments = array(
            'resolve_target_documents' => array()
        );

        // обойти все документы банлда
        foreach ($extensionConfig[0]['document'] as $modelName => $modelClass) {
            // интерфейс документа
            $interfaceName = 'Nitra\\IntegraBundle\\Document\\Model\\' . Inflector::classify($modelName) . 'Interface';
            $resolveTargetDocuments['resolve_target_documents'][$interfaceName] = $modelClass;

            // создать параметр документа
            $parameterName = 'nitra_integra.document.'.$modelName;
            $container->setParameter($parameterName, $modelClass);
        }

        // создать параметры интерфейсов реализующих сушности
        $container->setParameter('nitra_integra.resolve_target_documents', $resolveTargetDocuments['resolve_target_documents']);

        // добавить массив интерефейсов в настройки doctrine
        $container->prependExtensionConfig('doctrine_mongodb', $resolveTargetDocuments);
    }
}