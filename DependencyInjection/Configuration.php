<?php

namespace Nitra\IntegraBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        // дерево настроек
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('nitra_integra')->addDefaultsIfNotSet();

        // добавить настройки правил ценообразования
        $this->addRules($rootNode);

        // добавить настройки документов
        $this->addDocument($rootNode);

        // добавить настройки сущностей
        $this->addEntity($rootNode);

        // добавить настройки job
        $this->addJobs($rootNode);

        // вернуть дерево настроек
        return $treeBuilder;
    }

    /**
     * Добавить настройки ценообразования
     *
     * @param ArrayNodeDefinition $treeBuilder
     */
    protected function addRules(ArrayNodeDefinition $treeBuilder)
    {
        $treeBuilder->addDefaultsIfNotSet()
            ->children()
                ->arrayNode('rules')
                ->addDefaultsIfNotSet()
                    ->children()

                        // ставить активный, если товар подвязан
                        ->booleanNode('active_products')->defaultTrue()->end()

                        // ставить не активные если нет ниодного подвязанного стока
                        ->booleanNode('unactive_products')->defaultTrue()->end()

                        // ставить нет в наличии если у всех подвязанных стоков количество 0
                        ->booleanNode('put_out_of_stock')->defaultFalse()->end()

                        // ставить не кативен если у всех подвязанных стоков количество 0
                        ->booleanNode('deactivate_out_of_stock')->defaultFalse()->end()

                        // активировать заблокированные товары (у которых запонены не все параметры, у которых активно "отображать в фильтре")
                        ->booleanNode('activate_locked_products')->defaultFalse()->end()

                    ->end()
            ->end();
    }

    /**
     * добавить настройки документов
     *
     * @param ArrayNodeDefinition $treeBuilder
     */
    protected function addDocument(ArrayNodeDefinition $treeBuilder)
    {
        $treeBuilder->addDefaultsIfNotSet()
            ->children()
                ->arrayNode('document')->isRequired()->cannotBeEmpty()
                ->addDefaultsIfNotSet()
                    ->children()

                        // документ входящего правила
                        ->scalarNode('inRules')
                            ->example('Nitra\\IntegraBundle\\Document\\InRules')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()

                        // документ исходящего правила
                        ->scalarNode('outRules')
                            ->example('Nitra\\IntegraBundle\\Document\\OutRules')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()

                        // документ параметров и их значений в правилах
                        ->scalarNode('parameterAndValue')
                            ->example('Nitra\\IntegraBundle\\Document\\ParameterAndValue')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()

                        // документ значений параметров
                        ->scalarNode('parameterValues')
                            ->example('Nitra\\IntegraBundle\\Document\\ParameterValues')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()

                        // документ параметра
                        ->scalarNode('parameter')
                            ->example('Nitra\\MiniTetradkaBundle\\Document\\Parameter')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()

                        // документ стока
                        ->scalarNode('stock')
                            ->example('Nitra\\MiniTetradkaBundle\\Document\\Stock')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()

                        // документ магазина
                        ->scalarNode('store')
                            ->example('Nitra\\MiniTetradkaBundle\\Document\\Store')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()

                        // документ бренда
                        ->scalarNode('brand')
                            ->example('Nitra\\MiniTetradkaBundle\\Document\\Brand')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()

                        // документ категории продукта
                        ->scalarNode('category')
                            ->example('Nitra\\MiniTetradkaBundle\\Document\\Category')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()

                        // документ продукта
                        ->scalarNode('product')
                            ->example('Nitra\\MiniTetradkaBundle\\Document\\Product')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()

                        // документ модели
                        ->scalarNode('model')
                            ->example('Nitra\\MiniTetradkaBundle\\Document\\Model')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()

                    ->end()
                ->end()
            ->end();
    }

    /**
     * добавить настройки сущностей
     *
     * @param ArrayNodeDefinition $treeBuilder
     */
    protected function addEntity(ArrayNodeDefinition $treeBuilder)
    {
        // установить конфигурационные данные
        $treeBuilder->addDefaultsIfNotSet()
            ->children()
                ->arrayNode('entity')->isRequired()->cannotBeEmpty()
                ->addDefaultsIfNotSet()
                    ->children()

                        // сущность прайса
                        ->scalarNode('job')
                            ->example('Nitra\\TetradkaIntegraBundle\\Entity\\Job')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()

                        // сущность ТК
                        ->scalarNode('delivery')
                            ->example('Nitra\\TetradkaIntegraBundle\\Entity\\Delivery')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()

                        // сущность склада
                        ->scalarNode('warehouse')
                            ->example('Nitra\\TetradkaIntegraBundle\\Entity\\Warehouse')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()

                        // сущность поставщика
                        ->scalarNode('supplier')
                            ->example('Nitra\\TetradkaIntegraBundle\\Entity\\Supplier')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()

                        // сущность города
                        ->scalarNode('city')
                            ->example('Nitra\\TetradkaGeoBundle\\Entity\\City')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()

                        // сущность региона
                        ->scalarNode('region')
                            ->example('Nitra\\TetradkaGeoBundle\\Entity\\Region')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()

                        // сущность валюты
                        ->scalarNode('currency')
                            ->example('Nitra\\MiniTetradkaBundle\\Entity\\Currency')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()

                    ->end()
                ->end()
            ->end();
    }

    /**
     * добавить настройки Job
     *
     * @param ArrayNodeDefinition $treeBuilder
     */
    protected function addJobs(ArrayNodeDefinition $treeBuilder)
    {
        // установить дерево настроек
        $treeBuilder->addDefaultsIfNotSet()
            ->children()
                ->arrayNode('jobs')
                ->addDefaultsIfNotSet()
                    ->children()
                        // путь к директории в которую складываются прайсы
                        ->scalarNode('excel_dir')->defaultValue('/home/user/temp/integra/excel/')->end()
                        // путь к директории в которую находятся обработчики прайсов
                        ->scalarNode('handler_dir')->defaultValue('/home/user/temp/integra/handler/')->end()
                        // массив обработчиков прайсов
                        ->arrayNode('handlers')
                        ->prototype('scalar')->end()
                        ->defaultValue(array(
                            'standart' => 'standart_0.1/standart/standart',
                        ))
                    ->end()
                ->end()
            ->end();
    }
}