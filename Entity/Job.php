<?php

namespace Nitra\IntegraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\MappedSuperclass
 * @Assert\Callback(methods={"isStandartValid" })
 */
class Job implements Model\JobInterface
{
    use ORMBehaviors\Timestampable\Timestampable;
    use ORMBehaviors\Blameable\Blameable;
    use ORMBehaviors\SoftDeletable\SoftDeletable;
    use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;

    /**
     * @var integer $id
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;

    /**
     * @var string $processName - загрузчик
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Не указан загрузчик")
     * @Assert\Length(max="255")
     */
    protected $name;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $downloadedAt;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(max="255")
     */
    protected $description;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $fileExtention;

    /**
     * @var SupplierInterface поставщик
     * @ORM\ManyToOne(targetEntity="Nitra\IntegraBundle\Entity\Model\SupplierInterface")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан поставщик.")
     */
    protected $supplier;

    /**
     * @var WarehouseInterface склад поставщика
     * @ORM\ManyToOne(targetEntity="Nitra\IntegraBundle\Entity\Model\WarehouseInterface")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан склад поставщика.")
     */
    protected $warehouse;

    /**
     * @var bool $isStandart - флаг стандартный загрузчик
     * @ORM\Column(type="boolean")
     */
    protected $isStandart;

    /**
     * @var integer $columnName - номера ячеек "Название"
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    protected $columnName;

    /**
     * @var integer $columnAvailiable - номер ячейки "Наличие"
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Range(min=1, max=26)
     */
    protected $columnAvailiable;

    /**
     * @var integer - Стартовая строка, количество строк которое нужно пропустить
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Range(min=1, max=999999)
     */
    protected $startRow;

    /**
     * @var integer $columnPriceIn1 - номер ячейки "Цена1"
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Range(min=1, max=26)
     */
    protected $columnPriceIn1;

    /**
     * @var integer $columnPriceIn2 - номер ячейки "Цена2"
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Range(min=1, max=26)
     */
    protected $columnPriceIn2;

    /**
     * @var integer $columnPriceIn3 - номер ячейки "Цена3"
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Range(min=1, max=26)
     */
    protected $columnPriceIn3;

    /**
     * @var integer $columnPriceIn4 - номер ячейки "Цена1"
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Range(min=1, max=26)
     */
    protected $columnPriceIn4;

    /**
     * @var CurrencyInterface валюта
     * @ORM\ManyToOne(targetEntity="Nitra\IntegraBundle\Entity\Model\CurrencyInterface")
     * @ORM\JoinColumn(referencedColumnName="code", nullable=true)
     */
    protected $currency;

    /**
     * object to string
     * @return string
     */
    public function __toString()
    {
        // вернуть описание прайса
        return (string) $this->getDescription();
    }

    /**
     * Get isStandart
     * @return boolean
     */
    public function isStandart()
    {
        return $this->isStandart;
    }

    /**
     * Callback валидатор стандартный загрузчик
     * @param \Symfony\Component\Validator\ExecutionContextInterface $context
     */
    public function isStandartValid(\Symfony\Component\Validator\ExecutionContextInterface $context)
    {
        // если стандартный загрузчик проверить значение
        if ($this->getIsStandart()) {
            // массив валидируемых столбцов
            $validColumns = array(
                'columnName'        => "Не указаны номера столбцов для названия продукта.",
                'columnAvailiable'  => "Не указан номер колонки для столбца \"Наличие\".",
                'startRow'          => "Не указана \"Стартовая строка\".",
                'columnPriceIn1'    => "Не указан номер колонки для столбца \"Цена 1\".",
            );

            // обойти все валидируемы столбцы
            foreach ($validColumns as $column => $message) {
                // проверить столбец
                if (!$this->$column) {
                    // добавить сообщение ошибки валидации
                    $context->addViolationAt($column, $message, array(), null);
                }
            }
        }
    }

    /**
     * Получить имя файла прайса
     * @return string - путь к файлу прайса
     */
    public function getNameFilePrice()
    {
        // расширение файла прайса
        $fileExt = $this->getFileExtention();
        // имя файла прайса
        return ($this->getIsStandart())
            // стандартный загрузчик
            ? $this->getName() .'_id'. $this->getId() . '.' . $fileExt
            // индивидуальный загрузчик
            : $this->getName() . '.' . $fileExt;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->startRow = 1;
    }

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return Job
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set downloadedAt
     * @param \DateTime $downloadedAt
     * @return Job
     */
    public function setDownloadedAt($downloadedAt)
    {
        $this->downloadedAt = $downloadedAt;

        return $this;
    }

    /**
     * Get downloadedAt
     * @return \DateTime
     */
    public function getDownloadedAt()
    {
        return $this->downloadedAt;
    }

    /**
     * Set description
     * @param string $description
     * @return Job
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set fileExtention
     * @param string $fileExtention
     * @return Job
     */
    public function setFileExtention($fileExtention)
    {
        $this->fileExtention = $fileExtention;
        return $this;
    }

    /**
     * Get fileExtention
     * @return string
     */
    public function getFileExtention()
    {
        return $this->fileExtention;
    }

    /**
     * Set isStandart
     * @param boolean $isStandart
     * @return Job
     */
    public function setIsStandart($isStandart)
    {
        $this->isStandart = $isStandart;
        return $this;
    }

    /**
     * Get isStandart
     * @return boolean
     */
    public function getIsStandart()
    {
        return $this->isStandart;
    }

    /**
     * Set columnName
     * @param string $columnName
     * @return Job
     */
    public function setColumnName($columnName)
    {
        $this->columnName = $columnName;
        return $this;
    }

    /**
     * Get columnName
     * @return string
     */
    public function getColumnName()
    {
        return $this->columnName;
    }

    /**
     * Set columnAvailiable
     * @param integer $columnAvailiable
     * @return Job
     */
    public function setColumnAvailiable($columnAvailiable)
    {
        $this->columnAvailiable = $columnAvailiable;
        return $this;
    }

    /**
     * Get columnAvailiable
     * @return integer
     */
    public function getColumnAvailiable()
    {
        return $this->columnAvailiable;
    }

    /**
     * Set startRow
     * @param integer $startRow
     * @return Job
     */
    public function setStartRow($startRow)
    {
        $this->startRow = $startRow;
        return $this;
    }

    /**
     * Get startRow
     * @return integer
     */
    public function getStartRow()
    {
        return $this->startRow;
    }

    /**
     * Set columnPriceIn1
     * @param integer $columnPriceIn1
     * @return Job
     */
    public function setColumnPriceIn1($columnPriceIn1)
    {
        $this->columnPriceIn1 = $columnPriceIn1;
        return $this;
    }

    /**
     * Get columnPriceIn1
     * @return integer
     */
    public function getColumnPriceIn1()
    {
        return $this->columnPriceIn1;
    }

    /**
     * Set columnPriceIn2
     * @param integer $columnPriceIn2
     * @return Job
     */
    public function setColumnPriceIn2($columnPriceIn2)
    {
        $this->columnPriceIn2 = $columnPriceIn2;

        return $this;
    }

    /**
     * Get columnPriceIn2
     * @return integer
     */
    public function getColumnPriceIn2()
    {
        return $this->columnPriceIn2;
    }

    /**
     * Set columnPriceIn3
     * @param integer $columnPriceIn3
     * @return Job
     */
    public function setColumnPriceIn3($columnPriceIn3)
    {
        $this->columnPriceIn3 = $columnPriceIn3;
        return $this;
    }

    /**
     * Get columnPriceIn3
     * @return integer
     */
    public function getColumnPriceIn3()
    {
        return $this->columnPriceIn3;
    }

    /**
     * Set columnPriceIn4
     * @param integer $columnPriceIn4
     * @return Job
     */
    public function setColumnPriceIn4($columnPriceIn4)
    {
        $this->columnPriceIn4 = $columnPriceIn4;
        return $this;
    }

    /**
     * Get columnPriceIn4
     * @return integer
     */
    public function getColumnPriceIn4()
    {
        return $this->columnPriceIn4;
    }

    /**
     * Set supplier
     * @param \Nitra\IntegraBundle\Entity\Model\SupplierInterface $supplier
     * @return Job
     */
    public function setSupplier(\Nitra\IntegraBundle\Entity\Model\SupplierInterface $supplier)
    {
        $this->supplier = $supplier;
        return $this;
    }

    /**
     * Get supplier
     * @return \Nitra\IntegraBundle\Entity\Model\SupplierInterface
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Set warehouse
     * @param \Nitra\IntegraBundle\Entity\Model\WarehouseInterface $warehouse
     * @return Job
     */
    public function setWarehouse(\Nitra\IntegraBundle\Entity\Model\WarehouseInterface $warehouse)
    {
        $this->warehouse = $warehouse;
        return $this;
    }

    /**
     * Get warehouse
     * @return \Nitra\IntegraBundle\Entity\Model\WarehouseInterface
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * Set currency
     * @param \Nitra\IntegraBundle\Entity\Model\CurrencyInterface $currency
     * @return Job
     */
    public function setCurrency(\Nitra\IntegraBundle\Entity\Model\CurrencyInterface $currency = null)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * Get currency
     * @return \Nitra\IntegraBundle\Entity\Model\CurrencyInterface
     */
    public function getCurrency()
    {
        return $this->currency;
    }
}