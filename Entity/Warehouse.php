<?php

namespace Nitra\IntegraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\MappedSuperclass
 */
class Warehouse implements Model\WarehouseInterface
{
    use ORMBehaviors\Timestampable\Timestampable;
    use ORMBehaviors\Blameable\Blameable;
    use ORMBehaviors\SoftDeletable\SoftDeletable;
    use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * адрес склада
     * @var string $address
     * @ORM\Column(type="string", nullable=true, length=255)
     * @Assert\Length(max="255")
     */
    protected $address;

    /**
     * @var SupplierInterface поставщик
     * @ORM\ManyToOne(targetEntity="Nitra\IntegraBundle\Entity\Model\SupplierInterface")
     * @Assert\NotBlank(message="Не указан поставщик.")
     */
    protected $supplier;

    /**
     * @var DeliveryInterface ТК
     * @ORM\ManyToOne(targetEntity="Nitra\IntegraBundle\Entity\Model\DeliveryInterface")
     */
    protected $delivery;

    /**
     * @var CityInterface город
     * @ORM\ManyToOne(targetEntity="Nitra\GeoBundle\Entity\Model\CityInterface")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан город")
     */
    protected $city;

    /**
     * Приоритет склада для интегры
     * @var int $priority
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $priority;

    /**
     * object to string
     * @return string
     */
    public function __toString()
    {
        // венуть адрес склада
        return (string) $this->getAddress();
    }

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address
     * @param string $address
     * @return Warehouse
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * Get address
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set priority
     * @param integer $priority
     * @return Warehouse
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * Get priority
     * @return integer
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set supplier
     * @param \Nitra\IntegraBundle\Entity\Model\SupplierInterface $supplier
     * @return Warehouse
     */
    public function setSupplier(\Nitra\IntegraBundle\Entity\Model\SupplierInterface $supplier = null)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     * @return \Nitra\IntegraBundle\Entity\Model\SupplierInterface
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Set delivery
     * @param \Nitra\IntegraBundle\Entity\Model\DeliveryInterface $delivery
     * @return Warehouse
     */
    public function setDelivery(\Nitra\IntegraBundle\Entity\Model\DeliveryInterface $delivery = null)
    {
        $this->delivery = $delivery;
        return $this;
    }

    /**
     * Get delivery
     * @return \Nitra\IntegraBundle\Entity\Model\DeliveryInterface
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * Set city
     * @param \Nitra\GeoBundle\Entity\Model\CityInterface $city
     * @return Warehouse
     */
    public function setCity(\Nitra\GeoBundle\Entity\Model\CityInterface $city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get city
     * @return \Nitra\GeoBundle\Entity\Model\CityInterface
     */
    public function getCity()
    {
        return $this->city;
    }
}