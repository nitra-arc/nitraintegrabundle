<?php

namespace Nitra\IntegraBundle\Entity\Model;

/**
 * ТК
 */
interface DeliveryInterface
{
    /**
     * Get id
     * @return id $id
     */
    public function getId();

    /**
     * this object to string
     * @return string
     */
    public function __toString();

    /**
     * Get name
     * @return string
     */
    public function getName();

    /**
     * Get businessKey
     * @return integer
     */
    public function getBusinessKey();
}