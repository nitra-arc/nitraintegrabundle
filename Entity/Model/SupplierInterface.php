<?php

namespace Nitra\IntegraBundle\Entity\Model;

/**
 * поставщик
 */
interface SupplierInterface
{
    /**
     * Get id
     * @return id $id
     */
    public function getId();

    /**
     * object to string
     * @return string
     */
    public function __toString();

    /**
     * Получить массив стобцов цен
     * @return array
     */
    public function getPricesNames();

    /**
     * Get name
     * @return string
     */
    public function getName();

    /**
     * Get exchange
     * @return float
     */
    public function getExchange();

    /**
     * Get priceInColumn1
     *
     * @return string
     */
    public function getPriceInColumn1();

    /**
     * Get priceInColumn2
     * @return string
     */
    public function getPriceInColumn2();

    /**
     * Get priceInColumn3
     * @return string
     */
    public function getPriceInColumn3();

    /**
     * Get priceInColumn4
     * @return string
     */
    public function getPriceInColumn4();

    /**
     * Get currency
     * @return \Nitra\IntegraBundle\Entity\Model\CurrencyInterface
     */
    public function getCurrency();
}