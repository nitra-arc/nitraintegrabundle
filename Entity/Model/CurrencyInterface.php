<?php

namespace Nitra\IntegraBundle\Entity\Model;

/**
 * Валюта
 */
interface CurrencyInterface
{
    /**
     * object to string
     * @return string
     */
    public function __toString();

    /**
     * Get code
     * @return string
     */
    public function getCode();

    /**
     * Get name
     * @return string
     */
    public function getName();

    /**
     * Get exchange
     * @return float
     */
    public function getExchange();

    /**
     * Get symbol
     * @return string
     */
    public function getSymbol();
}