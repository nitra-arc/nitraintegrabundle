<?php

namespace Nitra\IntegraBundle\Entity\Model;

/**
 * склад
 */
interface WarehouseInterface
{
    /**
     * Get id
     * @return id $id
     */
    public function getId();

    /**
     * this object to string
     * @return string
     */
    public function __toString();

    /**
     * Get address
     * @return string
     */
    public function getAddress();

    /**
     * Get priority
     * @return int
     */
    public function getPriority();

    /**
     * Get supplier
     * @return \Nitra\IntegraBundle\Entity\Model\SupplierInterface
     */
    public function getSupplier();

    /**
     * Get delivery
     * @return \Nitra\IntegraBundle\Entity\Model\DeliveryInterface
     */
    public function getDelivery();

    /**
     * Get city
     * @return \Nitra\GeoBundle\Entity\Model\CityInterface
     */
    public function getCity();
}