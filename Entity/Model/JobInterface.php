<?php

namespace Nitra\IntegraBundle\Entity\Model;

/**
 * прайс
 */
interface JobInterface
{
    /**
     * Get id
     * @return id $id
     */
    public function getId();

    /**
     * object to string
     * @return string
     */
    public function __toString();

    /**
     * Get isStandart
     * @return boolean
     */
    public function isStandart();

    /**
     * Callback валидатор стандартный загрузчик
     * @param \Symfony\Component\Validator\ExecutionContextInterface $context
     */
    public function isStandartValid(\Symfony\Component\Validator\ExecutionContextInterface $context);

    /**
     * Получить имя файла прайса
     * @return string - путь к файлу прайса
     */
    public function getNameFilePrice();

    /**
     * Get name
     * @return string
     */
    public function getName();


    /**
     * Get downloadedAt
     * @return \DateTime
     */
    public function getDownloadedAt();

    /**
     * Get description
     * @return string
     */
    public function getDescription();

    /**
     * Get fileExtention
     * @return string
     */
    public function getFileExtention();

    /**
     * Get isStandart
     * @return boolean
     */
    public function getIsStandart();

    /**
     * Get columnName
     * @return string
     */
    public function getColumnName();

    /**
     * Get columnAvailiable
     * @return integer
     */
    public function getColumnAvailiable();

    /**
     * Get startRow
     * @return integer
     */
    public function getStartRow();

    /**
     * Get columnPriceIn1
     * @return integer
     */
    public function getColumnPriceIn1();

    /**
     * Get columnPriceIn2
     * @return integer
     */
    public function getColumnPriceIn2();

    /**
     * Get columnPriceIn3
     * @return integer
     */
    public function getColumnPriceIn3();

    /**
     * Get columnPriceIn4
     * @return integer
     */
    public function getColumnPriceIn4();

    /**
     * Get supplier
     * @return \Nitra\IntegraBundle\Entity\Model\SupplierInterface
     */
    public function getSupplier();

    /**
     * Get warehouse
     * @return \Nitra\IntegraBundle\Entity\Model\WarehouseInterface
     */
    public function getWarehouse();

    /**
     * Get currency
     * @return \Nitra\IntegraBundle\Entity\Model\CurrencyInterface
     */
    public function getCurrency();
}