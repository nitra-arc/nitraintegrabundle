<?php

namespace Nitra\IntegraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\MappedSuperclass
 */
class Supplier implements Model\SupplierInterface
{
    use ORMBehaviors\Timestampable\Timestampable;
    use ORMBehaviors\Blameable\Blameable;
    use ORMBehaviors\SoftDeletable\SoftDeletable;
    use \Admingenerator\GeneratorBundle\Traits\ValidForDelete;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string $name
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    protected $name;

    /**
     * @var CurrencyInterface валюта
     * @ORM\ManyToOne(targetEntity="Nitra\IntegraBundle\Entity\Model\CurrencyInterface")
     * @ORM\JoinColumn(referencedColumnName="code")
     */
    protected $currency;

    /**
     * @var string $priceInColumn1
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    protected $priceInColumn1;

    /**
     * @var string $priceInColumn2
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    protected $priceInColumn2;

    /**
     * @var string $priceInColumn3
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    protected $priceInColumn3;

    /**
     * @var string $priceInColumn4
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    protected $priceInColumn4;

    /**
     * @var decimal $exchange
     * @ORM\Column(type="decimal", scale=3, nullable=true)
     * @Assert\Range(min=0.00001, max=999.999)
     */
    protected $exchange;

    /**
     * object to string
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * Получить массив стобцов цен
     * @return array
     */
    public function getPricesNames()
    {
        return array(
            $this->getPriceInColumn1(),
            $this->getPriceInColumn2(),
            $this->getPriceInColumn3(),
            $this->getPriceInColumn4(),
        );
    }

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return Supplier
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set priceInColumn1
     * @param string $priceInColumn1
     * @return Supplier
     */
    public function setPriceInColumn1($priceInColumn1)
    {
        $this->priceInColumn1 = $priceInColumn1;

        return $this;
    }

    /**
     * Get priceInColumn1
     * @return string
     */
    public function getPriceInColumn1()
    {
        return $this->priceInColumn1;
    }

    /**
     * Set priceInColumn2
     * @param string $priceInColumn2
     * @return Supplier
     */
    public function setPriceInColumn2($priceInColumn2)
    {
        $this->priceInColumn2 = $priceInColumn2;
        return $this;
    }

    /**
     * Get priceInColumn2
     * @return string
     */
    public function getPriceInColumn2()
    {
        return $this->priceInColumn2;
    }

    /**
     * Set priceInColumn3
     * @param string $priceInColumn3
     * @return Supplier
     */
    public function setPriceInColumn3($priceInColumn3)
    {
        $this->priceInColumn3 = $priceInColumn3;
        return $this;
    }

    /**
     * Get priceInColumn3
     * @return string
     */
    public function getPriceInColumn3()
    {
        return $this->priceInColumn3;
    }

    /**
     * Set priceInColumn4
     * @param string $priceInColumn4
     * @return Supplier
     */
    public function setPriceInColumn4($priceInColumn4)
    {
        $this->priceInColumn4 = $priceInColumn4;
        return $this;
    }

    /**
     * Get priceInColumn4
     * @return string
     */
    public function getPriceInColumn4()
    {
        return $this->priceInColumn4;
    }

    /**
     * Set exchange
     * @param string $exchange
     * @return Supplier
     */
    public function setExchange($exchange)
    {
        $this->exchange = $exchange;
        return $this;
    }

    /**
     * Get exchange
     * @return string
     */
    public function getExchange()
    {
        return $this->exchange;
    }

    /**
     * Set currency
     * @param \Nitra\IntegraBundle\Entity\Model\CurrencyInterface $currency
     * @return Supplier
     */
    public function setCurrency(\Nitra\IntegraBundle\Entity\Model\CurrencyInterface $currency = null)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * Get currency
     * @return \Nitra\IntegraBundle\Entity\Model\CurrencyInterface
     */
    public function getCurrency()
    {
        return $this->currency;
    }
}