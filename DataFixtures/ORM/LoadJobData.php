<?php

namespace Nitra\IntegraBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadJobData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        // фиктуры интегры заливаются в самый последний момент
        return 1102;
    }

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        // уствноыить контейнер
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        // получить параметры интегры
        $integraParamters = $this->container->get('nitra_integra.parameters');

        // получить валюту
        $currencyUAH = $manager
            ->getRepository($integraParamters->getEntity('currency'))
            ->find($integraParamters->get('currencyCode'));

        // получить склды поставщиков
        $warehouses = $integraParamters->get('em')
            ->createQueryBuilder()
            ->select('w, s')
            ->from($integraParamters->getEntity('warehouse'), 'w')
            ->innerJoin('w.supplier', 's')
            ->where('w.supplier IS NOT NULL')
            ->groupBy('w.supplier')
            ->getQuery()
            ->execute()
            ;

        // для каждого склада поставщика посавщика создать по 2 прайста
        // стандарт и не стандарт
        foreach($warehouses as $warehouse) {

            // класс реализуюший прайс
            $jobClass = $integraParamters->getEntity('job');

            // стандартный прайс для поставщика
            $job1 = new $jobClass();
            $job1
                ->setName('standart')
                ->setIsStandart(true)
                ->setColumnName('3, 1, 2, 4')
                ->setColumnAvailiable(7)
                ->setStartRow(6)
                ->setColumnPriceIn1(6)
                ->setCurrency($currencyUAH)
                ->setDescription('Стандартный прайс поставщика '.(string)$warehouse->getSupplier()->getName().' '.(string)$warehouse->getAddress())
                ->setWarehouse($warehouse)
                ->setSupplier($warehouse->getSupplier());

            // запомнить для сохранения
            $manager->persist($job1);

            // не стандартный прайс для поставщика
            $job2 = new $jobClass();
            $job2
                ->setName('no_standart')
                ->setIsStandart(false)
                ->setDescription('Не стандартный прайс поставщика '.(string)$warehouse->getSupplier()->getName().' '.(string)$warehouse->getAddress())
                ->setWarehouse($warehouse)
                ->setSupplier($warehouse->getSupplier());

            // запомнить для сохранения
            $manager->persist($job2);
        }

        // сохранить
        $manager->flush();
    }
}