<?php

namespace Nitra\IntegraBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadSupplierWarehouseData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        // фиктуры интегры заливаются в самый последний момент
        return 1101;
    }

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        // уствноыить контейнер
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        // получить параметры интегры
        $integraParamters = $this->container->get('nitra_integra.parameters');

        // создать регион
        $regionClass = $integraParamters->getEntity('region');
        $region = new $regionClass();
        $region->setName('integraRegion');
        $manager->persist($region);
        $this->addReference('integraRegion', $region);

        // создать город
        $cityClass = $integraParamters->getEntity('city');
        $city = new $cityClass();
        $city
            ->setRegion($region)
            ->setName('integraCity');
        $manager->persist($city);
        $this->addReference('integraCity', $city);

        // получить всех поставщиков
        $suppliers = $manager
            ->getRepository($integraParamters->getEntity('supplier'))
            ->findAll();

        // для каждого посавщика создать по одному складу
        foreach($suppliers as $supplier) {

            // модель склада
            $warehouseClass = $integraParamters->getEntity('warehouse');

            // название переменной
            $objName = 'warehouse'.$supplier->getId();

            // создать объект
            $$objName = new $warehouseClass();
            $$objName
                ->setAddress('Адрес склада поставщика '.(string)$supplier)
                ->setCity($city)
                ->setSupplier($supplier);

            // запомнить для сохранения
            $manager->persist($$objName);
        }

        // сохранить
        $manager->flush();
    }
}