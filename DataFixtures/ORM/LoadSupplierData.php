<?php

namespace Nitra\IntegraBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadSupplierData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        // фиктуры интегры заливаются в самый последний момент
        return 1100;
    }

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        // уствноыить контейнер
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        // получить параметры интегры
        $integraParamters = $this->container->get('nitra_integra.parameters');

        // получить валюту
        $currencyUAH = $manager
            ->getRepository($integraParamters->getEntity('currency'))
            ->find($integraParamters->get('currencyCode'));

        // создать поставщиков
        for ($index = 1; $index <= 10; $index++) {

            // модель поставщика
            $supplierClass = $integraParamters->getEntity('supplier');

            // название переменной
            $objName = 'IntegraSupplier'.$index;

            // создать объект
            $$objName = new $supplierClass();
            $$objName
                ->setName('Интегра-поставщик '.$index)
                ->setCurrency($currencyUAH)
                ->setExchange($currencyUAH->getExchange());

            // сапомнить для сохранения
            $manager->persist($$objName);

            // запомнить
            $this->addReference($objName, $$objName);
        }

        // сохранить
        $manager->flush();
    }
}