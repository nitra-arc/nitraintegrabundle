<?php

namespace Nitra\IntegraBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\Common\Collections as Collections;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\ExecutionContext;

/**
 * @ODM\Document
 * @Assert\Callback(methods={"isFormulaCorrect"})
 */
class InRules implements Model\InRulesInterface
{
    /**
     * @var string Identifier
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string Rule description
     * @ODM\String
     */
    protected $description;

    /**
     * @var integer Supplier warehouse identifier
     * @ODM\Int
     */
    protected $warehouseId;

    /**
     * @var Model\ParameterAndValueInterface[]
     * @ODM\EmbedMany(targetDocument="Nitra\IntegraBundle\Document\Model\ParameterAndValueInterface", strategy="setArray")
     */
    private $parameterValues;

    /**
     * @var integer sortOrder - порядковый номер правила
     * @ODM\Int
     * @Assert\Range(min = 0)
     */
    protected $sortOrder = 0;

    /**
     * @var Model\CategoryInterface
     * @ODM\ReferenceOne(targetDocument="Nitra\IntegraBundle\Document\Model\CategoryInterface")
     */
    protected $category;

    /**
     * @var Model\BrandInterface
     * @ODM\ReferenceOne(targetDocument="Nitra\IntegraBundle\Document\Model\BrandInterface")
     */
    protected $brand;

    /**
     * @var string keyWord - ключевое слово
     * @ODM\String
     */
    protected $keyWord;

    /**
     * @var string formula - формула для расчета
     * @ODM\String
     */
    protected $formula;

    /**
     * @var string condition - формула для расчета
     * @ODM\String
     */
    protected $condition;

    public function __construct()
    {
        $this->parameterValues = new ArrayCollection();
    }

    /**
     * Ошибка!
     *
     * @param \Symfony\Component\Validator\ExecutionContext $context
     */
    public function isFormulaCorrect(ExecutionContext $context)
    {
        if (!preg_match('/[A-Za-z]/', $this->getFormula())) {
            $context->addViolationAt('formula', 'Выберите хотя бы одну цену для формулы!', array(), null);
        }
        if (preg_match('/}[\d.]+|[\d.]+{/', $this->getFormula())) {
            $context->addViolationAt('formula', 'Выберите хотя бы одну из математических операций!', array(), null);
        }
        if (preg_match('/[+-\/*]{2,}/', $this->getFormula())) {
            $context->addViolationAt('formula', 'Нельзя выбирать 2 математические операции подряд!', array(), null);
        }
        if (preg_match('/^[-+*\/]+|[-+*\/]+$/', $this->getFormula())) {
            $context->addViolationAt('formula', 'Формула не может начинаться или заканчиваться на математическую операцию!', array(), null);
        }
    }

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set warehouseId
     * @param int $warehouseId
     * @return $this
     */
    public function setWarehouseId($warehouseId)
    {
        $this->warehouseId = $warehouseId;
        return $this;
    }

    /**
     * Get warehouseId
     * @return int $warehouseId
     */
    public function getWarehouseId()
    {
        return $this->warehouseId;
    }

    /**
     * Set sortOrder
     * @param int $sortOrder
     * @return $this
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * Get sortOrder
     * @return int $sortOrder
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set category
     * @param Model\CategoryInterface|null $category
     * @return $this
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * Get category
     * @return Model\CategoryInterface|null $category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set brand
     * @param Model\BrandInterface|null $brand
     * @return $this
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
        return $this;
    }

    /**
     * Get brand
     * @return Model\BrandInterface|null $brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Clear brand
     */
    public function clearBrand()
    {
        $this->brand = null;
    }

    /**
     * Set keyWord
     * @param string $keyWord
     * @return self
     */
    public function setKeyWord($keyWord)
    {
        $this->keyWord = $keyWord;
        return $this;
    }

    /**
     * Get keyWord
     * @return string $keyWord
     */
    public function getKeyWord()
    {
        return $this->keyWord;
    }

    /**
     * Set formula
     * @param string $formula
     * @return self
     */
    public function setFormula($formula)
    {
        $this->formula = $formula;
        return $this;
    }

    /**
     * Get formula
     * @return string $formula
     */
    public function getFormula()
    {
        return $this->formula;
    }

    /**
     * Set condition
     * @param string $condition
     * @return self
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
        return $this;
    }

    /**
     * Get condition
     * @return string $condition
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * Set description
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add parameterValue
     * @param Model\ParameterAndValueInterface $parameterValue
     * @return $this
     */
    public function addParameterValue(Model\ParameterAndValueInterface $parameterValue)
    {
        $this->parameterValues[] = $parameterValue;
        return $this;
    }

    /**
     * Remove parameterValue
     * @param Model\ParameterAndValueInterface $parameterValue
     * @return $this
     */
    public function removeParameterValue(Model\ParameterAndValueInterface $parameterValue)
    {
        $this->parameterValues->removeElement($parameterValue);
        return $this;
    }

    /**
     * Get parameterValues
     * @return Model\ParameterAndValueInterface[]
     */
    public function getParameterValues()
    {
        return $this->parameterValues;
    }
}