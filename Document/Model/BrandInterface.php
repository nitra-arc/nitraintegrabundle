<?php

namespace Nitra\IntegraBundle\Document\Model;

/**
 * бренд
 */
interface BrandInterface
{
    /**
     * Get id
     * @return id $id
     */
    public function getId();

    /**
     * object to string
     * @return string
     */
    public function __toString();

    /**
     * Get name
     * @return string $name
     */
    public function getName();
}