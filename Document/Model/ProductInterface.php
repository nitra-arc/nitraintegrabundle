<?php

namespace Nitra\IntegraBundle\Document\Model;

/**
 * продукт
 */
interface ProductInterface
{
    /**
     * Get id
     * @return id $id
     */
    public function getId();

    /**
     * object to string
     * @return string
     */
    public function __toString();

    /**
     * Get name
     * @return string $name
     */
    public function getName();

    /**
     * Get fullNameForSearch
     * @return string
     */
    public function getFullNameForSearch();

    /**
     * Get description
     * @return string $description
     */
    public function getDescription();

    /**
     * Get model
     * @return Nitra\MainBundle\Document\Model $model
     */
    public function getModel();

    /**
     * Get parameters
     * @return \Nitra\MainBundle\Document\ProductParameter[] $parameters
     */
    public function getParameters();

    /**
     * Get storePrice
     * @return  $storePrice
     */
    public function getStorePrice();

    /**
     * Get article
     * @return string $article
     */
    public function getArticle();

    /**
     * Get $width
     * @return hash $width
     */
    public function getWidth();

    /**
     * Get $height
     * @return hash $height
     */
    public function getHeight();

    /**
     * Get $length
     * @return hash $length
     */
    public function getLength();

    /**
     * Get $weight
     * @return hash $weight
     */
    public function getWeight();
}