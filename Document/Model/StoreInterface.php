<?php

namespace Nitra\IntegraBundle\Document\Model;

/**
 * магазин
 */
interface StoreInterface
{
    /**
     * Get id
     * @return id $id
     */
    public function getId();

    /**
     * object to string
     * @return string
     */
    public function __toString();

    /**
     * Get name
     * @return string $name
     */
    public function getName();

    /**
     * Get host
     * @return string $host
     */
    public function getHost();

    /**
     * Get addres
     * @return string $addres
     */
    public function getAddres();
}