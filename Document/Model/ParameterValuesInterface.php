<?php

namespace Nitra\IntegraBundle\Document\Model;

interface ParameterValuesInterface
{
    /**
     * Get id
     *
     * @return string $id
     */
    public function getId();

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName();

    /**
     * Get sortOrder
     *
     * @return string $sortOrder
     */
    public function getSortOrder();

    /**
     * Get isPopular
     *
     * @return string $isPopular
     */
    public function getIsPopular();
}