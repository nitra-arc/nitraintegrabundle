<?php

namespace Nitra\IntegraBundle\Document\Model;

/**
 * Сток
 */
interface StockInterface
{
    /**
     * Get id
     * @return id $id
     */
    public function getId();

    /**
     * проверить новый ли сток (существует ли у него цена)
     * ($this->getPriceIn() && $this->getQuantity())
     * @return boolean
     */
    public function isNew();

    /**
     * получить количество свободных товаров
     * @return integer разница ($quantity-$reserved)
     */
    public function getQuantityFree();

    /**
     * Get warehouseId
     * @return integer $warehouseId
     */
    public function getWarehouseId();

    /**
     * Get productId
     * @return string $productId
     */
    public function getProductId();

    /**
     * Get stockParams
     * @return string $stockParams
     */
    public function getStockParams();

    /**
     * Get priceName
     * @return string $priceName
     */
    public function getPriceName();

    /**
     * Get priceArticle
     * @return string $priceArticle
     */
    public function getPriceArticle();

    /**
     * Get isBanned
     * @return boolean $isBanned
     */
    public function getIsBanned();

    /**
     * Get bindedAt
     * @return date $bindedAt
     */
    public function getBindedAt();

    /**
     * Get bindedBy
     * @return int $bindedBy
     */
    public function getBindedBy();

    /**
     * Get quantity
     * @return int $quantity
     */
    public function getQuantity();

    /**
     * Get reserved
     * @return int $reserved
     */
    public function getReserved();

    /**
     * Get currency
     * @return string $currency
     */
    public function getCurrency();

    /**
     * Get currencyPrice
     * @return float $currencyPrice
     */
    public function getCurrencyPrice();

    /**
     * Get priceIn
     * @return float $priceIn
     */
    public function getPriceIn();

    /**
     * Get comment
     * @return string $comment
     */
    public function getComment();

    /**
     * Get price1
     * @return float $price1
     */
    public function getPrice1();

    /**
     * Get price2
     * @return float $price2
     */
    public function getPrice2();

    /**
     * Get price3
     * @return float $price3
     */
    public function getPrice3();

    /**
     * Get price4
     * @return float $price4
     */
    public function getPrice4();

    /**
     * Get storePrice
     * @return  $storePrice
     */
    public function getStorePrice();

    /**
     * получить цену выхода для магазина
     * @param MongoId $storeId идентификатор магазина
     * @return Float цена для магазина
     * @return 0 цена для магазина не найдена
     */
    public function getStorePriceOut($storeId);

    /**
     * Get inBlackList
     * @return bool $inBlackList
     */
    public function getInBlackList();
}