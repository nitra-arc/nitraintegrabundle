<?php

namespace Nitra\IntegraBundle\Document\Model;

interface ParameterAndValueInterface
{
    /**
     * Get id
     *
     * @return string $id
     */
    public function getId();

    /**
     * Get parameter
     *
     * @return \Nitra\IntegraBundle\Document\Model\ParameterInterface Parameter
     */
    public function getParameter();

    /**
     * Get parameter value
     *
     * @return \Nitra\IntegraBundle\Document\Model\ParameterValuesInterface Value
     */
    public function getParameterValue();
}