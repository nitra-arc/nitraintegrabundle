<?php

namespace Nitra\IntegraBundle\Document\Model;

/**
 * Категория продукта
 */
interface CategoryInterface
{
    /**
     * Get id
     * @return id $id
     */
    public function getId();

    /**
     * object to string
     * @return string
     */
    public function __toString();

    /**
     * Get name
     * @return string $name
     */
    public function getName();

    /**
     * Return indent name
     * @return string return str_repeat("--", $this->level-1) . $this->name;
     */
    public function getIndentedName();

    /**
     * Get sortOrder
     * @return int $sortOrder
     */
    public function getSortOrder();

    /**
     * Get isActive
     * @return int $isActive
     */
    public function getIsActive();

    /**
     * Get parent
     *
     * @return Nitra\MiniTetradkaBundle\Document\Category $parent
     */
    public function getParent();

    /**
     * Get path
     * @return string $path
     */
    public function getPath();

    /**
     * Get level
     * @return int $level
     */
    public function getLevel();
}