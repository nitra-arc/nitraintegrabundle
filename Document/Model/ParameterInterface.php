<?php

namespace Nitra\IntegraBundle\Document\Model;

interface ParameterInterface
{
    /**
     * Get id
     *
     * @return string $id
     */
    public function getId();

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName();
}