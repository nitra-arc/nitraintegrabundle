<?php

namespace Nitra\IntegraBundle\Document\Model;

/**
 * модель
 */
interface ModelInterface
{
    /**
     * Get id
     * @return id $id
     */
    public function getId();

    /**
     * object to string
     * @return string
     */
    public function __toString();

    /**
     * Get name
     * @return string $name
     */
    public function getName();

    /**
     * Get description
     * @return string $description
     */
    public function getDescription();

    /**
     * Get products
     * @return Nitra\MainBundle\Document\Product[]
     */
    public function getProducts();
}