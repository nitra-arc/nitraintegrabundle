<?php

namespace Nitra\IntegraBundle\Document\Model;

/**
 * Входящее правило
 */
interface InRulesInterface
{
    /**
     * Get id
     * @return id $id
     */
    public function getId();

    /**
     * Get warehouseId
     * @return int $warehouseId
     */
    public function getWarehouseId();

    /**
     * Get sortOrder
     * @return int $sortOrder
     */
    public function getSortOrder();

    /**
     * Get category
     * @return \Nitra\IntegraBundle\Document\Model\CategoryInterface $category
     */
    public function getCategory();

    /**
     * Get brand
     * @return \Nitra\IntegraBundle\Document\Model\BrandInterface $brand
     */
    public function getBrand();

    /**
     * Clear brand
     */
    public function clearBrand();

    /**
     * Get keyWord
     * @return string $keyWord
     */
    public function getKeyWord();

    /**
     * Get formula
     * @return string $formula
     */
    public function getFormula();

    /**
     * Get condition
     * @return string $condition
     */
    public function getCondition();

    /**
     * Get description
     * @return string $description
     */
    public function getDescription();

    /**
     * Get parameterValues
     *
     * @return ParameterAndValueInterface[] $parameterValues
     */
    public function getParameterValues();
}