<?php

namespace Nitra\IntegraBundle\Document\Model;

/**
 * Исходящее правило
 */
interface OutRulesInterface
{
    /**
     * Get id
     * @return id $id
     */
    public function getId();

    /**
     * Get sortOrder
     * @return int $sortOrder
     */
    public function getSortOrder();

    /**
     * Get category
     * @return \Nitra\IntegraBundle\Document\Model\CategoryInterface $category
     */
    public function getCategory();

    /**
     * Clear category
     */
    public function clearCategory();

    /**
     * Get brand
     * @return \Nitra\IntegraBundle\Document\Model\BrandInterface $brand
     */
    public function getBrand();

    /**
     * Clear brand
     */
    public function clearBrand();

    /**
     * Get store
     * @return \Nitra\IntegraBundle\Document\Model\StoreInterface $store
     */
    public function getStore();

    /**
     * Get description
     * @return string $description
     */
    public function getDescription();

    /**
     * Get minMargin
     * @return float $minMargin
     */
    public function getMinMargin();

    /**
     * Get maxMargin
     * @return float $maxMargin
     */
    public function getMaxMargin();

    /**
     * Get formula
     * @return string $formula
     */
    public function getFormula();

    /**
     * Get condition
     * @return string $condition
     */
    public function getCondition();

    /**
     * Get supplierId
     * @return int $supplierId
     */
    public function getSupplierId();
}