<?php

namespace Nitra\IntegraBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument
 */
class ParameterAndValue implements Model\ParameterAndValueInterface
{
    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @var \Nitra\IntegraBundle\Document\Model\ParameterInterface
     * @ODM\ReferenceOne(targetDocument="Nitra\IntegraBundle\Document\Model\ParameterInterface")
     */
    protected $parameter;

    /**
     * @var \Nitra\IntegraBundle\Document\Model\ParameterValuesInterface
     * @ODM\ReferenceOne(targetDocument="Nitra\IntegraBundle\Document\Model\ParameterValuesInterface")
     */
    protected $parameterValue;

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parameter
     * @param Model\ParameterInterface $parameter
     * @return $this
     */
    public function setParameter(Model\ParameterInterface $parameter)
    {
        $this->parameter = $parameter;
        return $this;
    }

    /**
     * Get parameter
     * @return Model\ParameterInterface $parameter
     */
    public function getParameter()
    {
        return $this->parameter;
    }

    /**
     * Set parameterValue
     * @param Model\ParameterValuesInterface $parameterValue
     * @return string
     */
    public function setParameterValue(Model\ParameterValuesInterface $parameterValue)
    {
        $this->parameterValue = $parameterValue;
        return $this;
    }

    /**
     * Get parameterValue
     * @return Model\ParameterValuesInterface
     */
    public function getParameterValue()
    {
        return $this->parameterValue;
    }
}