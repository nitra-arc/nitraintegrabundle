<?php

namespace Nitra\IntegraBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\Common\Collections as Collections;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\ExecutionContext;

/**
 * @ODM\Document
 * @Assert\Callback(methods={"isFormulaCorrect"})
 */
class OutRules implements Model\OutRulesInterface
{
    /**
     * @var string
     * @ODM\Id
     */
    protected $id;

    /**
     * @var integer sortOrder - порядковый номер правила
     * @ODM\Int
     * @Assert\Range(min = 0)
     */
    protected $sortOrder = 0;

    /**
     * @var string description - описание правила
     * @ODM\String
     */
    protected $description;

    /**
     * @var Model\CategoryInterface
     * @ODM\ReferenceOne(targetDocument="Nitra\IntegraBundle\Document\Model\CategoryInterface")
     */
    protected $category;

    /**
     * @var Model\BrandInterface
     * @ODM\ReferenceOne(targetDocument="Nitra\IntegraBundle\Document\Model\BrandInterface")
     */
    protected $brand;

    /**
     * @var Model\ParameterAndValueInterface[]
     * @ODM\EmbedMany(targetDocument="Nitra\IntegraBundle\Document\Model\ParameterAndValueInterface", strategy="setArray")
     */
    private $parameterValues;

    /**
     * @var integer Id поставщика
     * @ODM\Int
     */
    protected $supplierId;

    /**
     * @var float minMargin - Минимальная наценка по правилу
     * @ODM\Float
     * @Assert\Range(min = "0")
     */
    protected $minMargin;

    /**
     * @var float maxMargin - Максимальная наценка по правилу
     * @ODM\Float
     * @Assert\Range(min = "0")
     */
    protected $maxMargin;

    /**
     * @var Model\StoreInterface
     * @ODM\ReferenceOne(targetDocument="Nitra\IntegraBundle\Document\Model\StoreInterface")
     */
    protected $store;

    /**
     * @var string formula - формула
     * @ODM\String
     */
    protected $formula;

    /**
     * @var string condition - формула для расчета
     * @ODM\String
     */
    protected $condition;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->parameterValues = new ArrayCollection();
    }

    /**
     * Ошибка!
     *
     * @param \Symfony\Component\Validator\ExecutionContext $context
     */
    public function isFormulaCorrect(ExecutionContext $context)
    {
        if (!preg_match('/[A-Za-z]/', $this->getFormula())) {
            $context->addViolationAt('formula', 'Выберите хотя бы одну цену для формулы!', array(), null);
        }
        if (preg_match('/}[\d.]+|[\d.]+{/', $this->getFormula())) {
            $context->addViolationAt('formula', 'Выберите хотя бы одну из математических операций!', array(), null);
        }
        if (preg_match('/[+-\/*]{2,}/', $this->getFormula())) {
            $context->addViolationAt('formula', 'Нельзя выбирать 2 математические операции подряд!', array(), null);
        }
        if (preg_match('/^[-+*\/]+|[-+*\/]+$/', $this->getFormula())) {
            $context->addViolationAt('formula', 'Формула не может начинаться или заканчиваться на математическую операцию!', array(), null);
        }
    }

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sortOrder
     * @param int $sortOrder
     * @return $this
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * Get sortOrder
     * @return int $sortOrder
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set category
     * @param Model\CategoryInterface $category
     * @return $this
     */
    public function setCategory($category = null)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * Get category
     * @return Model\CategoryInterface $category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Clear category
     */
    public function clearCategory()
    {
        $this->category = null;
    }

    /**
     * Set brand
     * @param Model\BrandInterface $brand
     * @return $this
     */
    public function setBrand($brand = null)
    {
        $this->brand = $brand;
        return $this;
    }

    /**
     * Get brand
     * @return Model\BrandInterface $brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Clear brand
     */
    public function clearBrand()
    {
        $this->brand = null;
    }

    /**
     * Set store
     * @param Model\StoreInterface $store
     * @return $this
     */
    public function setStore($store=null)
    {
        $this->store = $store;
        return $this;
    }

    /**
     * Get store
     * @return Model\StoreInterface $store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set description
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set minMargin
     * @param float $minMargin
     * @return self
     */
    public function setMinMargin($minMargin)
    {
        $this->minMargin = $minMargin;
        return $this;
    }

    /**
     * Get minMargin
     * @return float $minMargin
     */
    public function getMinMargin()
    {
        return $this->minMargin;
    }

    /**
     * Set maxMargin
     * @param float $maxMargin
     * @return self
     */
    public function setMaxMargin($maxMargin)
    {
        $this->maxMargin = $maxMargin;
        return $this;
    }

    /**
     * Get maxMargin
     * @return float $maxMargin
     */
    public function getMaxMargin()
    {
        return $this->maxMargin;
    }

    /**
     * Set formula
     * @param string $formula
     * @return self
     */
    public function setFormula($formula)
    {
        $this->formula = $formula;
        return $this;
    }

    /**
     * Get formula
     * @return string $formula
     */
    public function getFormula()
    {
        return $this->formula;
    }

    /**
     * Set condition
     * @param string $condition
     * @return self
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
        return $this;
    }

    /**
     * Get condition
     * @return string $condition
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * Set supplierId
     * @param int $supplierId
     * @return self
     */
    public function setSupplierId($supplierId)
    {
        $this->supplierId = $supplierId;
        return $this;
    }

    /**
     * Get supplierId
     * @return int $supplierId
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * Add parameterValue
     * @param Model\ParameterAndValueInterface $parameterValue
     * @return $this
     */
    public function addParameterValue(Model\ParameterAndValueInterface $parameterValue)
    {
        $this->parameterValues[] = $parameterValue;
        return $this;
    }

    /**
     * Remove parameterValue
     * @param Model\ParameterAndValueInterface $parameterValue
     * @return $this
     */
    public function removeParameterValue(Model\ParameterAndValueInterface $parameterValue)
    {
        $this->parameterValues->removeElement($parameterValue);
        return $this;
    }

    /**
     * Get parameterValues
     * @return Model\ParameterAndValueInterface[] $parameterValues
     */
    public function getParameterValues()
    {
        return $this->parameterValues;
    }
}